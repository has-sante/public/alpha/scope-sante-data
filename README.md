# BQSS

Base sur la qualité et la sécurité des soins.

[![pipeline status](https://gitlab.com/has-sante/public/bqss/badges/develop/pipeline.svg)](https://gitlab.com/has-sante/public/bqss/-/commits/main)
[![coverage report](https://gitlab.com/has-sante/public/bqss/badges/develop/coverage.svg)](https://gitlab.com/has-sante/public/bqss/-/commits/main)

**Documentation**: [https://has-sante.gitlab.io/public/bqss](https://has-sante.gitlab.io/public/bqss)

**Code source**: [https://gitlab.com/has-sante/public/bqss](https://gitlab.com/has-sante/public/bqss)

## Description du projet

Lancé en novembre 2013, le site Scope Santé a eu pour but d'informer le public sur le niveau de qualité et de sécurité des soins des établissements de santé de France et a été remplacé en juin 2022 par le site QualiScope aux mêmes fins.

Le projet BQSS quant à lui, a pour but d'agréger les données concernées par le périmètre du site QualiScope, historique compris.

Ce projet n'utilise que des sources de données publiées en Open Data de sorte que le processus de reconstitution de la base BQSS ne nécessite pas d'accès ou d'autorisations particulières pour être rejoué ou modifié.

Les domaines de données traités sont les suivants :

- [FINESS](https://www.data.gouv.fr/en/datasets/finess-extraction-du-fichier-des-etablissements/) : Fichier National des Établissements Sanitaires et Sociaux listant tous les établissements
- [IQSS](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2020/) : Indicateurs de Qualité et de Sécurité des Soins
- [e-Satis](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2020/) : Indicateurs de mesure de la satisfaction patient
- [SAE](https://data.drees.solidarites-sante.gouv.fr/explore/dataset/708_bases-statistiques-sae/information/) : Statistique Annuelle des Établissements de santé rendant compte des résultats des enquêtes
- [Certification](https://www.data.gouv.fr/fr/datasets/certification-des-etablissements-de-sante-2020/) : résultats du processus de certification des établissements de santé par la HAS

---

## Utiliser les données produites

Ce projet produit une base fournie sous deux [formes différentes](docs/source/donnees/general-description.md): relationnelle ou orientée document.

Si vous souhaitez utiliser la donnée produite grâce à ce projet, voilà quelques ressources utiles :

- [Tutoriels d'utilisation des données](docs/source/donnees/tutoriels/index.md)
- [Description générale des données](docs/source/donnees/general-description.md)
- [La page du projet sur le portail d'Open Data data.gouv.fr](https://www.data.gouv.fr/fr/datasets/bqss/)
- [Le modèle de données de la table clef-valeur](docs/source/schemas/bqss/valeurs.md)
- [Le modèle de données de la table metadata](docs/source/schemas/bqss/metadata.md)
- [Les instructions pour mettre à jour la base de données](docs/source/developpement/maintenance.md).

---

## Comprendre le processus de création de la base

Pour comprendre comment la base est créée, voir [la page dédiée](docs/source/creation/data-processing-description.md).

---

## Contribuer au projet

Pour contribuer au projet, voir [la page dédiée](docs/source/developpement/development.md).
