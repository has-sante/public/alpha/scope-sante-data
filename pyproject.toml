[tool.poetry]
name = "bqss"
version = "1.0.0"
description = "Base sur la Qualité et la Sécurité des Soins"
authors = ["Timothée Chehab <t.chehab@has-sante.fr>"]
license = "Apache-2.0"
readme = "README.md"
repository = "https://gitlab.com/has-sante/public/bqss"
homepage = "https://gitlab.com/has-sante/public/bqss"
include = ["bin"]
keywords = []
classifiers = [
    "Intended Audience :: Developers",
    "License :: OSI Approved :: Apache Software License",
    "Natural Language :: English",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: Implementation :: CPython",
    "Programming Language :: Python",
    "Topic :: Software Development :: Libraries :: Python Modules",
]

# [tool.poetry.urls]
# Changelog = "https://gitlab.com/has-sante/public/bqss/releases"

[tool.poetry.dependencies]
python = "^3.8"

# Project-Specific
python-dotenv = "^0.20.0"
click = "8.1.2"
pandas = "^1.4.0"
frictionless = "^4.28.2"
tqdm = "^4.61.2"
py7zr = "^0.18.3"
pyproj = "^3.2.0"
requests = "^2.26.0"

# Notebooks
jupyter = "^1.0.0"
jupytext = "^1.11.4"
matplotlib = "^3.4.3"
seaborn = "^0.11.2"
keplergl = "^0.3.0"
matplotlib-venn = "^0.11.6"

# Documentation
importlib-metadata = { version = "^4.11.3", optional = true }
myst-parser = { version = "^0.18.0", optional = true }
myst-nb = {version = "^0.16.0", optional = true}
pygments = { version = "^2.8.1", optional = true }
sphinx = { version = "^4.5.0", optional = true }
sphinx-autodoc-typehints = { version = "^1.17.0", optional = true }
sphinxcontrib-apidoc = { version = "^0.3.0", optional = true }
sphinx-click = { version = "^4.0.0", optional = true }
table-schema-to-markdown = { version = "^0.4.6", optional = true }
sphinxcontrib-mermaid = {version = "^0.7", optional = true}
pydata-sphinx-theme = {version = "^0.9.0", optional = true}
autodoc-pydantic = {version = "^1.6.1", optional = true}
openpyxl = "^3.0.7"

# see: https://github.com/sphinx-doc/sphinx/issues/10291
jinja2 = { version = "<3.1", optional = true}

pandera = {extras = ["io"], version = "^0.10.1"}


[tool.poetry.dev-dependencies]

# Type Checking and Data Validation
mypy = "^0.942" # Static type checker
# stub files for some packages
types-requests = "^2.27.16"
types-PyYAML = "^6.0.5"

# Testing
pytest = "^7.1.1"
pytest-cov = "^3.0.0"
pytest-dotenv = "^0.5.2"
pytest-mock = "^3.6.0"
pytest-ordering = "^0.6"
pytest-sugar = "^0.9.4"
pytest-xdist = "^2.2.1"

# Linting
## Code formatting
black = "^22.3.0"
## Code quality
isort = "^5.10.1"
pylint = "^2.8.1"
## Automation and management
pre-commit = "^2.12.1"
pydantic = "^1.8.2"
ipdb = "^0.13.9"


[tool.poetry.extras]
docs = [
    "importlib-metadata",
    "myst-parser",
    "myst-nb",
    "pygments",
    "sphinx",
    "sphinx-autodoc-typehints",
    "sphinxcontrib-apidoc",
    "sphinx-click",
    "table-schema-to-markdown",
    "sphinxcontrib-mermaid",
    "pydata-sphinx-theme",
    "autodoc-pydantic",
    "jinja2",
]


[tool.poetry.scripts]
cli = "bin.cli:cli"

#################################################################################
# Tooling configs                                                               #
#################################################################################
[tool.black]
line-length = 79

[tool.coverage.run]
branch = true
concurrency = ["multiprocessing"]
parallel = true
source = ["bqss"]

[tool.coverage.report]
exclude_lines = [
    "pragma: no cover",
    "raise AssertionError",
    "raise NotImplementedError",
    "if __name__ == .__main__.:",
]
fail_under = 70
show_missing = true
skip_covered = true

[tool.cruft]
skip = [
    ".git",
    "README.md",
]

[tool.isort]
profile = "black"
atomic = "true"
combine_as_imports = "true"
line_length = 79

[tool.mypy]
disallow_untyped_defs = false
files = ["bqss/*.py","bin/*.py"]
exclude = "notebooks/config.py"
ignore_missing_imports = true
pretty = true
show_column_numbers = true
show_error_context = true
show_error_codes = true
strict_optional = false
plugins = "pydantic.mypy"


[tool.pylint.basic]
good-names-rgxs = ["^Test_.*$", "logger"]

[tool.pylint.messages_control]
disable = [
  # Explicitly document only as needed
  "missing-module-docstring",
  "missing-class-docstring",
  "missing-function-docstring",
  # Black & Flake8 purview
  "line-too-long",
  "bad-continuation",
  "c-extension-no-member",
]

[tool.pylint.typecheck]
generated-members = "pandas.*"

[tool.pylint.similarities]
# Ignore imports when computing similarities.
min-similarity-lines = 10
ignore-imports = "yes"

[tool.pytest.ini_options]
log_cli = 1
addopts = ["-rfsxX", "-l", "--tb=short", "--strict-markers"]
xfail_strict = "true"
testpaths = ["tests",]
norecursedirs = [".*", "*.egg", "build", "dist",]
env_override_existing_values = 1
env_files = [".env", ".test.env"]

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
