#! /usr/bin/env python
import logging

import click

from bqss.bqss.main import main as bqss_main
from bqss.certification_14_20 import constants as certification_14_20_constants
from bqss.certification_14_20.main import main as certification_14_20_main
from bqss.certification_21_25 import constants as certification_21_25_constants
from bqss.certification_21_25.main import main as certification_21_25_main
from bqss.constants import LOG_LEVEL
from bqss.esatis import constants as esatis_constants
from bqss.esatis.main import main as esatis_main
from bqss.finess import constants as finess_constants
from bqss.finess.main import main as finess_main
from bqss.iqss import constants as iqss_constants
from bqss.iqss.main import main as iqss_main
from bqss.sae import constants as sae_constants
from bqss.sae.main import main as sae_main

logging.basicConfig(
    level=logging.getLevelName(LOG_LEVEL),
    format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
)


@click.group()
def cli():
    pass


@cli.command()
@click.option(
    "--skip-data-dl",
    default=False,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
def finess(skip_data_dl=False, data_validation=False) -> None:
    """Lance le pipeline du domaine de données FINESS"""
    if (
        not skip_data_dl
        and finess_constants.DATA_DOMAIN_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {finess_constants.DATA_DOMAIN_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        skip_data_dl = True
    finess_main(
        skip_data_download=skip_data_dl, data_validation=data_validation
    )


@cli.command()
@click.option(
    "--skip-data-dl",
    default=False,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
def certification_14_20(skip_data_dl=False, data_validation=False) -> None:
    """
    Lance le pipeline du domaine de données Certification du référentiel 2014-2020
    """
    if (
        not skip_data_dl
        and certification_14_20_constants.DATA_DOMAIN_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {certification_14_20_constants.DATA_DOMAIN_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        skip_data_dl = True

    certification_14_20_main(
        skip_data_download=skip_data_dl,
        data_validation=data_validation,
    )


@cli.command()
@click.option(
    "--skip-data-dl",
    default=False,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
def certification_21_25(skip_data_dl=False, data_validation=False) -> None:
    """
    Lance le pipeline du domaine de données Certification du référentiel 2021-2025
    """
    if (
        not skip_data_dl
        and certification_21_25_constants.DATA_DOMAIN_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {certification_21_25_constants.DATA_DOMAIN_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        skip_data_dl = True

    certification_21_25_main(
        skip_data_download=skip_data_dl,
        data_validation=data_validation,
    )


@cli.command()
@click.option(
    "--skip-data-dl",
    default=False,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
def sae(skip_data_dl=False, data_validation=False) -> None:
    """Lance le pipeline du domaine de données SAE"""
    if (
        not skip_data_dl
        and sae_constants.DATA_DOMAIN_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {sae_constants.DATA_DOMAIN_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        skip_data_dl = True

    sae_main(skip_data_download=skip_data_dl, data_validation=data_validation)


@cli.command()
@click.option(
    "--skip-data-dl",
    default=False,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
def iqss(skip_data_dl=False, data_validation=False) -> None:
    """Lance le pipeline du domaine de données IQSS"""
    if (
        not skip_data_dl
        and iqss_constants.DATA_IQSS_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {iqss_constants.DATA_IQSS_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        skip_data_dl = True

    iqss_main(skip_data_download=skip_data_dl, data_validation=data_validation)


@cli.command()
@click.option(
    "--skip-data-dl",
    default=False,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
def esatis(skip_data_dl=False, data_validation=False) -> None:
    """Lance le pipeline du domaine de données e-Satis"""
    if (
        not skip_data_dl
        and esatis_constants.DATA_ESATIS_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {esatis_constants.DATA_ESATIS_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        skip_data_dl = True

    esatis_main(
        skip_data_download=skip_data_dl, data_validation=data_validation
    )


@cli.command()
@click.option(
    "--force-full-run",
    default=False,
    is_flag=True,
    help="Force le run de tous les pipelines nécessaires à la constitution de la base",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données pour chaque domaine de données intermédiaire",
)
@click.option(
    "--open-data",
    default=False,
    is_flag=True,
    help="Active l'update des données en open data (data.gouv.fr)",
)
def bqss(
    force_full_run=False,
    data_validation=False,
    open_data=False,
) -> None:
    """Lance la constitution de la base BQSS"""
    bqss_main(
        force_full_run=force_full_run,
        data_validation=data_validation,
        open_data=open_data,
    )


if __name__ == "__main__":
    cli()
