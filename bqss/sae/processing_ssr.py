from typing import Final

import pandas as pd

from bqss.constants import ISO_8859_1
from bqss.sae.constants import RAW_DOMAIN_PATH

DTYPES_DICT: Final[dict] = {
    "AN": "int64",
    "FI": "string",
    "GDE": "string",
    "LIT": "Int64",
    "PLA": "Int64",
    "JOUHC": "Int64",
    "JOUHP": "Int64",
}


def process_ssr_data(sae_df) -> pd.DataFrame:
    """
    Traite les données du bordereau SSR :
        - table SSR : description des capacités et activités
    """
    filenames = RAW_DOMAIN_PATH.glob("**/SSR_2*.csv")
    dfs = []
    for filename in filenames:
        ssr_df = pd.read_csv(
            filename,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=DTYPES_DICT,
            usecols=DTYPES_DICT.keys(),
        )
        dfs.append(ssr_df)
    ssr_df = pd.concat(dfs)
    # le champ GDE permet de filtrer par affections : système nerveux,
    # respiratoire, cardio-vasculaires, etc ainsi que d'avoir le total
    ssr_df = ssr_df[ssr_df["GDE"] == "SSR_TOT"]
    ssr_df.rename(
        columns={col: f"ssr_{col.lower()}" for col in ssr_df.columns},
        inplace=True,
    )
    sae_df = pd.merge(
        left=sae_df,
        right=ssr_df,
        how="outer",
        left_on=["id_an", "id_fi"],
        right_on=["ssr_an", "ssr_fi"],
        validate="one_to_one",
    )
    sae_df.drop(columns=["ssr_an", "ssr_fi", "ssr_gde"], inplace=True)
    return sae_df
