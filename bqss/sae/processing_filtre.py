from typing import Final

import pandas as pd

from bqss.constants import ISO_8859_1
from bqss.sae.constants import RAW_DOMAIN_PATH

DTYPES_DICT: Final[dict] = {
    "AN": "int64",
    "FI": "string",
    "STATUT": "string",
}

BOOLEAN_COLUMNS: Final[list] = [
    "HEB_MED",
    "HEB_CHIR",
    "HEB_PERINAT",
    "HEB_PSY",
    "HEB_SSR",
    "HEB_SLD",
    "MED",
    "CHIRAMBU",
    "PSY",
    "RTH",
    "CHIMIO",
    "IRC",
    "IVGAMP",
    "CPP",
    "HAD",
    "SSR",
    "URG",
    "SMURSAMU",
    "BLOC",
    "IMAG",
    "BIO",
    "ANEST",
    "MEDIC",
    "STERIL",
    "DIM",
    "TELEMED",
    "EMSP",
    "DOULEUR",
    "PALIA",
    "MSOCIAL",
    "CARDIO_INT",
    "REA",
    "CHIRCANCER",
    "NEUROCHIR",
    "NEURO_INT",
    "GREFFE",
    "BRULE",
    "CHIRCAR",
]


def process_filtre_data(sae_df) -> pd.DataFrame:
    """
    Traite les données du bordereau FILTRE : bordereau caractérisant précisément
    l'offre de soins disponible
    """
    filenames = RAW_DOMAIN_PATH.glob("**/FILTRE_2*.csv")
    dfs = []
    for filename in filenames:
        filtre_df = pd.read_csv(
            filename,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=DTYPES_DICT,
            usecols=[*DTYPES_DICT.keys(), *BOOLEAN_COLUMNS],
            converters={
                col: lambda val: bool(int(val)) if val else pd.NA
                for col in BOOLEAN_COLUMNS
            },
        )
        dfs.append(filtre_df)
    filtre_df = pd.concat(dfs)
    # le champ STATUT peut prendre 2 valeurs :
    #   - INIT : valeur intiale préremplie via les autorisations AS
    #   - DECLA : valeur déclarée par l'établissement
    filtre_df = filtre_df[filtre_df["STATUT"] == "DECLA"]
    filtre_df.rename(
        columns={col: f"filtre_{col.lower()}" for col in filtre_df.columns},
        inplace=True,
    )
    sae_df = pd.merge(
        left=sae_df,
        right=filtre_df,
        how="outer",
        left_on=["id_an", "id_fi"],
        right_on=["filtre_an", "filtre_fi"],
        validate="one_to_one",
    )
    sae_df.drop(
        columns=["filtre_an", "filtre_fi", "filtre_statut"], inplace=True
    )
    return sae_df
