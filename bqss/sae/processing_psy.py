from typing import Final

import pandas as pd

from bqss.constants import ISO_8859_1
from bqss.sae.constants import RAW_DOMAIN_PATH

DTYPES_DICT: Final[dict] = {
    "AN": "int64",
    "FI": "string",
    "DIS": "string",
    "CAP_HTP": "Int64",
    "JOU_HTP": "Int64",
    "JOU_ACP": "Int64",
    "CAP_HDJ": "Int64",
    "CAP_HDN": "Int64",
    "VEN_HDJ": "Int64",
    "VEN_HDN": "Int64",
}


def process_psy_data(sae_df) -> pd.DataFrame:
    """
    Traite les données du bordereau PSY :
        - table PSY : description des capacités et activités, prise en charge
        ambulatoire, file active, personnels concourant à l'activité
    """
    filenames = RAW_DOMAIN_PATH.glob("**/PSY_2*.csv")
    dfs = []
    for filename in filenames:
        psy_df = pd.read_csv(
            filename,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=DTYPES_DICT,
            usecols=DTYPES_DICT.keys(),
        )
        dfs.append(psy_df)
    psy_df = pd.concat(dfs)
    # le champ DIS permet de filtrer par discipline de psychiatrie :
    #   - GEN : psychiatrie générale (adulte)
    #   - INF : psychiatrie infanto-juvénile (enfants)
    #   - PEN : psychiatrie en milieu pénitenciaire
    #   - TOT : total psychiatrie (somme des 3 autres disciplines)
    psy_df = psy_df[psy_df["DIS"].isin(["GEN", "INF"])]
    # calcul des volumétries (capacités et activités) des hospitalisations de
    # jour (aussi dites partielles)
    psy_df.fillna(
        {"CAP_HDJ": 0, "CAP_HDN": 0, "VEN_HDJ": 0, "VEN_HDN": 0}, inplace=True
    )
    psy_df["CAP_HDJN"] = psy_df["CAP_HDJ"] + psy_df["CAP_HDN"]
    psy_df["VEN_HDJN"] = psy_df["VEN_HDJ"] + psy_df["VEN_HDN"]
    psy_df.drop(
        columns=["CAP_HDJ", "CAP_HDN", "VEN_HDJ", "VEN_HDN"], inplace=True
    )
    psy_df = psy_df.pivot(
        index=["AN", "FI"],
        columns="DIS",
        values=["CAP_HTP", "JOU_HTP", "JOU_ACP", "CAP_HDJN", "VEN_HDJN"],
    )
    # renaming (we cant' use psy_df.rename after a pivot)
    psy_df.columns = psy_df.columns.map(
        lambda col: f"psy_{'_'.join(col).lower()}"
    )
    sae_df = pd.merge(
        left=sae_df,
        right=psy_df,
        how="outer",
        left_on=["id_an", "id_fi"],
        right_index=True,
        validate="one_to_one",
    )
    return sae_df
