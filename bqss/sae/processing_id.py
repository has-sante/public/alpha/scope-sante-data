from typing import Final

import pandas as pd

from bqss.constants import ISO_8859_1
from bqss.sae.constants import RAW_DOMAIN_PATH

DTYPES_DICT: Final[dict] = {
    "an": "int64",
    "fi": "string",
    "rs": "string",
    "fi_ej": "string",
}


def process_id_data() -> pd.DataFrame:
    """
    Traite les données du bordereau ID : bordereau sur l'identification de
    l'entité interrogée.
    Ce bordereau nous sert principalement à répertorier les établissements
    interrogés pour nous en servir de base d'agrégation pour les autres
    bordereaux.
    """
    filenames = RAW_DOMAIN_PATH.glob("**/ID_2*.csv")
    dfs = []
    for filename in filenames:
        id_df = pd.read_csv(
            filename,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=DTYPES_DICT,
            usecols=DTYPES_DICT.keys(),
        )
        id_df.rename(
            columns={col: f"id_{col.lower()}" for col in id_df.columns},
            inplace=True,
        )
        dfs.append(id_df)
    return pd.concat(dfs)
