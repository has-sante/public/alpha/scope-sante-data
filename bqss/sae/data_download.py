import logging
import os
import re
import shutil
from typing import Final

import requests
from py7zr import py7zr

from bqss.sae.constants import RAW_DOMAIN_PATH

logger: Final[logging.Logger] = logging.getLogger(__name__)


def download_data(data_sources):
    """
    Télécharge les données du domaine de données SAE
    """
    logger.info("Downloading data...")

    # clean directory structure
    shutil.rmtree(RAW_DOMAIN_PATH, ignore_errors=True)
    for year in range(
        data_sources["sources"][-1]["year"],
        data_sources["sources"][0]["year"] + 1,
    ):
        os.makedirs(RAW_DOMAIN_PATH / str(year))

    for source in data_sources["sources"]:
        download_data_from_source(source)

    logger.info("Downloading data OK")


def download_data_from_source(source):
    """
    Télécharge et décompresse la source de données en paramètre
    """
    dir_path = RAW_DOMAIN_PATH / str(source["year"])
    file_path = dir_path / "data.7z"
    with requests.get(
        source["compressed_data"], stream=True
    ) as response, open(file_path, "wb") as file:
        shutil.copyfileobj(response.raw, file)
    filter_pattern = re.compile(
        r"SAE \d{4} Bases statistiques - formats SAS-CSV/Bases statistiques/Bases CSV/.*"
    )
    with py7zr.SevenZipFile(file_path, mode="r") as archive:
        all_files = archive.getnames()
        target_files = [f for f in all_files if filter_pattern.match(f)]
        archive.extract(path=dir_path, targets=target_files)
