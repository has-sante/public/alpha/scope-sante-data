from typing import Final

import pandas as pd

from bqss.constants import ISO_8859_1
from bqss.sae.constants import RAW_DOMAIN_PATH

DTYPES_DICT: Final[dict] = {
    "AN": "int64",
    "FI": "string",
    "SALEND": "Int64",
    "BLOCS_A14": "Int64",
    "BLOCS_B14": "Int64",
}


def process_blocs_data(sae_df: pd.DataFrame) -> pd.DataFrame:
    """
    Traite les données du bordereau BLOCS : sites opératoires et salles
    d'intervention
    """
    filenames = RAW_DOMAIN_PATH.glob("**/BLOCS_2*.csv")
    dfs = []
    for filename in filenames:
        blocs_df = pd.read_csv(
            filename,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=DTYPES_DICT,
            usecols=DTYPES_DICT.keys(),
        )
        dfs.append(blocs_df)
    blocs_df = pd.concat(dfs)
    blocs_df.rename(
        columns={col: f"blocs_{col.lower()}" for col in blocs_df.columns},
        inplace=True,
    )
    sae_df = pd.merge(
        left=sae_df,
        right=blocs_df,
        how="outer",
        left_on=["id_an", "id_fi"],
        right_on=["blocs_an", "blocs_fi"],
        validate="one_to_one",
    )
    sae_df.drop(columns=["blocs_an", "blocs_fi"], inplace=True)
    return sae_df
