from typing import Final

import pandas as pd

from bqss.constants import ISO_8859_1
from bqss.sae.constants import RAW_DOMAIN_PATH

DTYPES_DICT: Final[dict] = {
    "AN": "int64",
    "FI": "string",
    "PLATOT": "Int64",
    "JOU_HAD": "Int64",
}


def process_had_data(sae_df) -> pd.DataFrame:
    """
    Traite les données du bordereau HAD :
        - table HAD : description des capacités et activités
    """
    filenames = RAW_DOMAIN_PATH.glob("**/HAD_2*.csv")
    dfs = []
    for filename in filenames:
        had_df = pd.read_csv(
            filename,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=DTYPES_DICT,
            usecols=DTYPES_DICT.keys(),
        )
        dfs.append(had_df)
    had_df = pd.concat(dfs)
    had_df.rename(
        columns={col: f"had_{col.lower()}" for col in had_df.columns},
        inplace=True,
    )
    sae_df = pd.merge(
        left=sae_df,
        right=had_df,
        how="outer",
        left_on=["id_an", "id_fi"],
        right_on=["had_an", "had_fi"],
        validate="one_to_one",
    )
    sae_df.drop(columns=["had_an", "had_fi"], inplace=True)
    return sae_df
