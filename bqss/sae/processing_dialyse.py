from typing import Final

import pandas as pd

from bqss.constants import ISO_8859_1
from bqss.sae.constants import RAW_DOMAIN_PATH

DTYPES_DICT: Final[dict] = {
    "AN": "int64",
    "FI": "string",
    "DIALY": "string",
    "CAPA": "Int64",
    "SEAN": "Int64",
    "SEM": "Int64",
}


def process_dialyse_data(sae_df) -> pd.DataFrame:
    """
    Traite les données du bordereau DIALYSE :
        - table DIALYSE : traitement de l'insuffisance rénale chronique par
        épuration extra-rénale
    """
    filenames = RAW_DOMAIN_PATH.glob("**/DIALYSE_2*.csv")
    # il existe 2 types de dialyses : hémodialyse et dialyse péritonéale
    hemo_dfs = []  # hémodialyse dfs
    peri_dfs = []  # péritonéale dfs
    for filename in filenames:
        dialyse_df = pd.read_csv(
            filename,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=DTYPES_DICT,
            usecols=DTYPES_DICT.keys(),
        )
        hemo_df = dialyse_df.drop(columns="SEM")
        peri_df = dialyse_df.drop(columns="SEAN")
        hemo_dfs.append(hemo_df)
        peri_dfs.append(peri_df)
    hemo_df = pd.concat(hemo_dfs)
    peri_df = pd.concat(peri_dfs)
    # le champ DIALY permet de filtrer par code de dialyse, il peut prendre une
    # vingtaine de valeurs différentes dont le détail se trouve dans la
    # documentation de la SAE (dans dictionnaires des variables par bordereaux)
    hemo_df = hemo_df[hemo_df["DIALY"].str.startswith("11")]
    peri_df = peri_df[peri_df["DIALY"].str.startswith("12")]
    hemo_df = hemo_df.pivot(
        index=["AN", "FI"],
        columns="DIALY",
        values=["CAPA", "SEAN"],
    )
    peri_df = peri_df.pivot(
        index=["AN", "FI"],
        columns="DIALY",
        values=["CAPA", "SEM"],
    )
    dialyse_df = pd.concat([hemo_df, peri_df], axis=1)
    # renaming (we cant' use dialyse_df.rename after a pivot)
    dialyse_df.columns = dialyse_df.columns.map(
        lambda col: f"dialyse_{'_'.join(col).lower()}"
    )
    sae_df = pd.merge(
        left=sae_df,
        right=dialyse_df,
        how="outer",
        left_on=["id_an", "id_fi"],
        right_index=True,
        validate="one_to_one",
    )
    return sae_df
