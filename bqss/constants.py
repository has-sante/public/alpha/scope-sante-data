import os
from pathlib import Path
from typing import Final

from dotenv import load_dotenv

# see `.env.template` for requisite environment variables
load_dotenv()
LOG_LEVEL = os.getenv("LOG_LEVEL", "INFO")
RESOURCES_DIR = os.getenv("RESOURCES_DIR", "resources")
DATA_DIR = os.getenv("DATA_DIR", "data")
DATA_GOUV_API_KEY = os.getenv("DATA_GOUV_API_KEY")

# Files downloaded from open data are usually encoded in iso-8859-1
ISO_8859_1: Final[str] = "iso-8859-1"
# utf-8 is the chosen encoding for our output
UTF_8: Final[str] = "utf-8"

ROOT_PATH: Final[Path] = Path(__file__).parent.parent.absolute()
RESOURCES_PATH: Final[Path] = ROOT_PATH / RESOURCES_DIR
DATA_PATH: Final[Path] = ROOT_PATH / DATA_DIR
SCHEMAS_PATH: Final[Path] = ROOT_PATH / "schemas"

FINESS_TYPE_GEO = "geo"
FINESS_TYPE_JUR = "jur"
FINESS_TYPE_MIXTE = "mixte"
FINESS_TYPE_UNKNOWN = "na"

TESTS_SKIP_DATA_DOWNLOAD = bool(os.getenv("TESTS_SKIP_DATA_DOWNLOAD", "False"))
