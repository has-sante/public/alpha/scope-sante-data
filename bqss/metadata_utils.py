import pandas as pd
from frictionless import Schema

from bqss.constants import UTF_8


def table_schema_to_csv(schema_path, csv_path):
    """
    Converti un table schema au format JSON en un fichier de meta-données au
    format CSV.
    """
    fields_df = table_schema_to_df(schema_path)
    fields_df.to_csv(
        csv_path,
        sep=",",
        encoding=UTF_8,
        index=False,
    )


def table_schema_to_df(schema_path) -> pd.DataFrame:
    """
    Converti un table schema au format JSON en un dataframe (qui pourra être
    sauvegardé au format CSV)
    """
    schema = Schema(schema_path)
    fields = [
        [field[k] for k in field.keys() if k not in ["constraints", "enum"]]
        for field in list(schema.fields)
    ]
    fields_df = pd.DataFrame(
        fields,
        columns=[
            k
            for k in schema.fields[0].keys()
            if k not in ["constraints", "enum"]
        ],
    )
    return fields_df
