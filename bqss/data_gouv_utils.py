import logging
from pathlib import Path
from typing import Final

import requests

from bqss.constants import DATA_GOUV_API_KEY

logger: Final[logging.Logger] = logging.getLogger(__name__)

API_BASE_URL = "https://www.data.gouv.fr/api/1"


def update_dataset_metadata(
    dataset_id,
    metadata_file_path,
):
    """
    Met à jour sur data.gouv.fr la description du dataset passé en paramètre
    avec le contenu du fichier au chemin passé en paramètre
    """
    url = API_BASE_URL + f"/datasets/{dataset_id}/"
    headers = {
        "X-API-KEY": DATA_GOUV_API_KEY,
    }
    with open(metadata_file_path, encoding="utf-8") as metadata_file:
        metadata = metadata_file.read()
    response = requests.put(
        url,
        headers=headers,
        json={
            "description": metadata,
        },
        timeout=30,
    )
    response.raise_for_status()


def update_resource_data(
    dataset_id,
    resource_id,
    data_file_path: Path,
):
    """
    Met à jour sur data.gouv.fr la ressource passée en paramètre avec les données
    présentes sur disque au chemin passé en paramètre. À noter que la ressource
    doit avoir été préalablement créée, cette fonction ne réalise qu'un update.
    """
    url = (
        API_BASE_URL
        + f"/datasets/{dataset_id}/resources/{resource_id}/upload/"
    )
    headers = {
        "X-API-KEY": DATA_GOUV_API_KEY,
    }
    with open(data_file_path, "rb") as data_file:
        files = {
            "file": data_file,
        }
        response = requests.post(
            url,
            headers=headers,
            files=files,
            timeout=120,
        )
    response.raise_for_status()
