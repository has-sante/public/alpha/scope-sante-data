import logging
from typing import Final

from bqss import data_acquisition_utils
from bqss.certification_14_20.constants import (
    FINAL_DOMAIN_PATH,
    RESOURCES_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.certification_14_20.data_download import download_data
from bqss.certification_14_20.processing import process_data
from bqss.validation_utils import is_data_valid

logger: Final[logging.Logger] = logging.getLogger(__name__)

# pylint: disable=duplicate-code
def main(skip_data_download: bool = False, data_validation: bool = False):
    """
    Point d'entrée du pipeline de données de certifications
    """
    # download data
    if skip_data_download:
        logger.info("Skipped data download")
    else:
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)

    process_data()

    # data validation
    if data_validation:
        logger.info("Validating data...")
        for filename in ["certifications", "thematiques"]:
            valid = is_data_valid(
                FINAL_DOMAIN_PATH / f"{filename}.csv",
                SCHEMAS_DOMAIN_PATH / f"{filename}.json",
            )
            logger.info(
                "Validating %s data : %s",
                filename,
                "VALID" if valid else "INVALID",
            )
    else:
        logger.info("Skipped data validation")
