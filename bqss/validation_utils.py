import json
import logging
from typing import Final

import pandera
from frictionless import Resource, validate, validate_schema
from pandera.io import from_frictionless_schema

logger: Final[logging.Logger] = logging.getLogger(__name__)


def is_metadata_valid(schema) -> bool:
    """
    Validate the given metadata file, typically a table schema.
    Return True if the metadata are valid, False otherwise
    """
    report = validate_schema(schema)
    return report.valid


def is_data_valid(data_path, metadata_path) -> bool:
    """
    Validate the data at the given path against the metadata (usually a table
    schema) at the given path.
    Return True if the data are valid regarding the metadata, False otherwise
    """
    resource = Resource(data_path, schema=metadata_path)
    report = validate(resource, type="resource", limit_memory=None)
    for error in report.task.errors:
        logger.warning(error.message)
    return report.valid


def tableschema_to_pandera(
    schema_path, nullable_bool=False, nullable_int=False, coerce=True
):
    with schema_path.open() as schema_fp:
        schema = from_frictionless_schema(json.load(schema_fp))

    schema.coerce = coerce
    schema.ordered = True
    for col in schema.columns.values():
        col.coerce = coerce
        if nullable_bool and isinstance(col.dtype, pandera.dtypes.Bool):
            col.dtype = pandera.engines.pandas_engine.BOOL
        if nullable_int and isinstance(col.dtype, pandera.dtypes.Int64):
            col.dtype = pandera.engines.pandas_engine.INT64

    return schema
