import logging
from typing import Final

import requests
import tqdm

from bqss.data_acquisition_utils import clean_directory
from bqss.iqss.constants import RAW_IQSS_PATH

logger: Final[logging.Logger] = logging.getLogger(__name__)


def download_data(data_sources):
    """
    Télécharge l'extraction du recueil en paramètre
    """
    # clean directory structure
    clean_directory(RAW_IQSS_PATH)
    for data_source in tqdm.tqdm(
        data_sources["sources"], desc="Downloading iqss data"
    ):
        year_path = RAW_IQSS_PATH / f"{data_source['year']}"
        year_path.mkdir(exist_ok=True)
        for file_d in tqdm.tqdm(
            data_source["files"], desc=f"Year: {data_source['year']}"
        ):
            resp = requests.get(file_d["data_url"], allow_redirects=True)
            file_type = resp.url.rsplit(".")[-1]
            file_path = (
                year_path
                / f"{file_d['theme']}_{file_d['secteur']}_data.{file_type}"
            )

            with file_path.open("wb") as fio:
                fio.write(resp.content)
