from pathlib import Path
from typing import Final

from pandas.api.types import CategoricalDtype

from bqss.constants import DATA_PATH, RESOURCES_PATH, SCHEMAS_PATH

RESOURCES_IQSS_PATH: Final[Path] = RESOURCES_PATH / "iqss"
DATA_IQSS_PATH: Final[Path] = DATA_PATH / "iqss"
SCHEMAS_IQSS_PATH: Final[Path] = SCHEMAS_PATH / "iqss"
RAW_IQSS_PATH: Final[Path] = DATA_IQSS_PATH / "raw"
FINAL_IQSS_PATH: Final[Path] = DATA_IQSS_PATH / "final"
CLEAN_IQSS_PATH: Final[Path] = DATA_IQSS_PATH / "cleaned"

COL_2_DROP = [
    "type_etablissement_2016",
    "type_etablissement_2017",
    "repondant",
    "libelle_region",
    "Type Etab",
    "Chirurgie",
    "statut_2017",
    "statut_2016",
    "cat",
    "region",
    "Libelle_region",
    "lib_reg",
    "Participation",
    "type",
    "Enquete_obligatoire",
    "finess_pmsi",
    "lib_cat",
    "Lib_reg",
    "region_2016",
    "region_2017",
    "Sources",
    "code_reg",
    "Region",
    # Pas géré par la HAS
    "icatb2_score",
    "icatb2_classe",
    "classe_icatb2_2017",
    "controle_icatb2_2017",
    "evolution_icatb2_2017",
    "score_icatb2_2017",
    "bn_sarm_principal",
    "bn_sarm_classe",
    "bn_sarm_complementaire",
]

SECTEUR = ["MCO", "HAD", "PSY", "SSR", "nan", "MHS"]
THEME = [
    "DIA",
    "IDM",
    "DPA",
    "AVC",
    "HPP",
    "ETEORTHO",
    "IAS",
    "ISOORTHO",
    "CA",
    "RCP",
    "DAN",
]
ACRONYME = [
    "AAT",
    "AQD",
    "ASE",
    "BASI",
    "COORD",
    "CPA",
    "DEC",
    "DEL1",
    "DHS",
    "DOC",
    "DTD",
    "DTN",
    "DTN1",
    "DTN2",
    "EAT",
    "ENV",
    "EPR1",
    "ETEORTHO",
    "HYG",
    "ICATB.2",
    "ICSHA.2",
    "ICSHA",
    "ICSHA.3",
    "ISOORTHO",
    "MDD",
    "NUT",
    "PCD",
    "PECIHPPI",
    "PHO",
    "PSH",
    "PSPV",
    "QLS",
    "RCP2",
    "SER",
    "SPH",
    "SURMIN",
    "TDA",
    "TDP",
    "TRD",
    "TRE",
    "ICA.BMR",
    "BN.SARM",
    "ICA.LISO",
    "ICALIN.2",
    "nan",
]

KEY_VALUE_DTYPES_DICT: Final[dict] = {
    "annee": "int64",
    "finess": "string",
    "finess_type": CategoricalDtype(categories=["geo", "jur"], ordered=False),
    "raison_sociale": "string",
    "key": "string",
    "value_boolean": "boolean",
    "value_string": "string",
    "value_integer": "Int64",
    "value_float": "float64",
    "missing_value": "string",
    "value_date": "string",
}
