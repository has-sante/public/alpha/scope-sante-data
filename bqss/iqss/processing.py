import logging
from typing import Final

import pandas as pd

from bqss.data_acquisition_utils import clean_directory
from bqss.iqss.constants import (
    CLEAN_IQSS_PATH,
    FINAL_IQSS_PATH,
    RESOURCES_IQSS_PATH,
)
from bqss.iqss.processing_clean import clean_data
from bqss.iqss.processing_finess import set_finess_type
from bqss.iqss.processing_merge import merged_iqss_data
from bqss.iqss.processing_reformate import reformate_historized_iqss

logger: Final[logging.Logger] = logging.getLogger(__name__)


def process_data():
    """
    Traite les données du domaine IQSS
    """
    logger.info("Début du traitement des données IQSS")
    for directory in [CLEAN_IQSS_PATH, FINAL_IQSS_PATH]:
        clean_directory(directory)

    metadata_df = pd.read_csv(RESOURCES_IQSS_PATH / "metadata.csv", sep=",")

    clean_data(metadata_df)
    logger.info("Nettoyage des données terminé")

    merged_iqss_data()
    logger.info("Fusion des données terminée")

    set_finess_type()
    logger.info("Ajout du type de finess terminé")

    reformate_df = reformate_historized_iqss()
    logger.info("Formattage en clef-valeur terminé")

    reformate_df.to_csv(
        FINAL_IQSS_PATH / "iqss_key_value.csv",
        index=False,
        sep=",",
        encoding="UTF-8",
    )
    logger.info("Fin du traitement des données IQSS")
