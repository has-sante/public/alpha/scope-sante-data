import logging
from typing import Final

import pandas as pd

from bqss.constants import (
    FINESS_TYPE_GEO,
    FINESS_TYPE_JUR,
    FINESS_TYPE_UNKNOWN,
)
from bqss.finess.constants import FINAL_DOMAIN_PATH as FINAL_FINESS_PATH
from bqss.finess.processing_finess import FINAL_FINESS_DTYPES_DICT
from bqss.iqss.constants import FINAL_IQSS_PATH

logger: Final[logging.Logger] = logging.getLogger(__name__)


def set_finess_type():
    finess_df = pd.read_csv(
        FINAL_FINESS_PATH / "finess.csv",
        dtype=FINAL_FINESS_DTYPES_DICT,
    )
    iqss_df = pd.read_csv(
        FINAL_IQSS_PATH / "iqss.csv",
        dtype={
            "finess": "string",
        },
    )

    finess_ej = list(finess_df["num_finess_ej"].astype(str))
    finess_et = list(finess_df["num_finess_et"].astype(str))
    iqss_df["finess"] = iqss_df["finess"].astype(str)
    iqss_df["finess_type"] = FINESS_TYPE_UNKNOWN

    iqss_df.loc[
        iqss_df.finess.isin(finess_ej), "finess_type"
    ] = FINESS_TYPE_JUR
    iqss_df.loc[
        iqss_df.finess.isin(finess_et), "finess_type"
    ] = FINESS_TYPE_GEO

    if not iqss_df.loc[iqss_df.finess_type == FINESS_TYPE_UNKNOWN].empty:
        logger.error(
            "Cette liste de finess est inconnue: %s.\nOn ne la retrouve pas dans le fichier compilé des finess.",
            list(
                iqss_df.loc[iqss_df.finess_type == FINESS_TYPE_UNKNOWN][
                    "finess"
                ]
            ),
        )

    iqss_df.to_csv(
        FINAL_IQSS_PATH / "iqss.csv", index=False, encoding="UTF-8", sep=","
    )
