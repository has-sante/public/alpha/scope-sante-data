import logging
from typing import Final

import pandas as pd

from bqss.data_acquisition_utils import read_file
from bqss.iqss.constants import (
    ACRONYME,
    COL_2_DROP,
    RAW_IQSS_PATH,
    RESOURCES_IQSS_PATH,
    SECTEUR,
    THEME,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)


def validate_metadata_file():
    """
    Valide le fichier de métadonnées du domaine IQSS
    """
    logger.info("Validation du fichier de metadata")
    metadata_df = pd.read_csv(RESOURCES_IQSS_PATH / "metadata.csv")

    # check que les variables du fichier métadata sont bien les variables des fichiers en open data
    for year_data_path in RAW_IQSS_PATH.iterdir():
        files = [
            file_path
            for file_path in year_data_path.iterdir()
            if (year_data_path / file_path).is_file()
        ]
        file_year = int(year_data_path.name) - 1
        iqss_df_columns = [
            list(read_file(file_path).columns) for file_path in files
        ]
        iqss_df_columns = {
            item for sublist in iqss_df_columns for item in sublist
        }

        metadata_file_year = set(
            metadata_df.loc[
                metadata_df["annee"]  # pylint: disable=unsubscriptable-object
                == file_year
            ]["ancienne_variable"]
        )
        diff = metadata_file_year - iqss_df_columns

        if diff and [i for i in diff if i not in COL_2_DROP]:
            logger.error(
                "Ces variables ne sont pas reconnus: %s",
                [i for i in diff if i not in COL_2_DROP],
            )

    # code + annee dans le fichier de metadonnée
    if metadata_df.duplicated(subset=["code", "annee"]).any():
        logger.error(
            "Le couple (code, annee) n'est pas unique pour: %s",
            metadata_df[  # pylint: disable=unsubscriptable-object
                metadata_df.duplicated(subset=["code", "annee"])
            ][["code", "annee"]].to_dict(orient="records"),
        )

    # check secteur theme accronyme
    if not (
        metadata_df.secteur.isin(SECTEUR).any()
        & metadata_df.theme.isin(THEME).any()
        & metadata_df.acronyme.isin(ACRONYME).any()
    ):
        logger.error("Un code un secteur ou un accronyme n'est pas connu")

    # Check types identique pour un code donné
    types_count_df = metadata_df.groupby("code")["type"].nunique()
    wrong_codes_df = types_count_df[types_count_df > 1].index
    if not wrong_codes_df.empty:
        logger.error(
            "Les codes suivants ont des types hétérogènes: %s",
            wrong_codes_df.tolist(),
        )

    logger.info("Validation du fichier de meta-data terminée")
