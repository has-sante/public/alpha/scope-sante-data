import logging
from typing import Final, List

import numpy as np
import pandas as pd

from bqss.iqss.constants import FINAL_IQSS_PATH

logger: Final[logging.Logger] = logging.getLogger(__name__)


NON_APPLICABLE_MISSING_VALUE = "Non applicable"
NON_REPONDANT_MISSING_VALUE = "Non répondant"
NON_CONCERNE_MISSING_VALUE = "Non concerné"
NON_CONTROLE_MISSING_VALUE = "Non contrôlé"
NON_VALIDE_MISSING_VALUE = "Non validé"
NON_CALCULABLE_MISSING_VALUE = "Non calculable"
DONNEES_INSUFFISANTES_MISSING_VALUE = "Données insuffisantes"
MISSING_VALUES_DICT: Final[dict] = {
    r"NA": NON_APPLICABLE_MISSING_VALUE,
    r"Non Applicable": NON_APPLICABLE_MISSING_VALUE,
    r"NR": NON_REPONDANT_MISSING_VALUE,
    r"Non répondant": NON_REPONDANT_MISSING_VALUE,
    r"NC": NON_CONCERNE_MISSING_VALUE,
    r"Non concerné.*": NON_CONCERNE_MISSING_VALUE,
    r"Non validé": NON_VALIDE_MISSING_VALUE,
    r"Recueil Non Validé": NON_VALIDE_MISSING_VALUE,
    r"Non Contrôlable": NON_CONTROLE_MISSING_VALUE,
    r"ES non contrôlé": NON_CONTROLE_MISSING_VALUE,
    r"Pas de contrôle": NON_CONTROLE_MISSING_VALUE,
    r"Non Calculable": NON_CALCULABLE_MISSING_VALUE,
    r"DI": DONNEES_INSUFFISANTES_MISSING_VALUE,
    r"Effectif <.*": DONNEES_INSUFFISANTES_MISSING_VALUE,
    r"Eff$": DONNEES_INSUFFISANTES_MISSING_VALUE,
}

SUPPORTED_DTYPES: Final[list] = [
    ("object", "value_string"),
    ("Int64", "value_integer"),
    # float64 is nullable but int64 isn't
    ("float64", "value_float"),
]

FINAL_ORDERED_COLUMNS = [
    "finess",
    "finess_type",
    "raison_sociale",
    "annee",
    "key",
    "value_string",
    "value_integer",
    "value_float",
    "missing_value",
]


def reformate_historized_iqss():
    """
    Reformate les critères iqss en ligne
    """

    merged_df = pd.read_csv(
        FINAL_IQSS_PATH / "iqss.csv",
        dtype={
            "finess": "string",
        },
    )

    id_columns = ["finess", "finess_type", "raison_sociale", "annee"]

    found_dtypes = [
        (dtype, dtype in [dtype[0] for dtype in SUPPORTED_DTYPES])
        for dtype in merged_df[
            merged_df.columns.difference(id_columns)
        ].dtypes.unique()
    ]
    if not all(found_dtype for _, found_dtype in found_dtypes):
        logger.error(
            "Type de donnée non supporté: %s, "
            "ils ne sont pas pris en compte lors de la conversion en clef-valeur",
            [dtype for dtype, found_dtype in found_dtypes if not found_dtype],
        )

    typed_dfs = []
    for dtype, column_name in SUPPORTED_DTYPES:
        typed_columns = merged_df.select_dtypes(dtype).columns
        typed_columns = typed_columns.difference(id_columns).tolist()
        if not typed_columns:
            continue
        typed_df = merged_df.loc[:, id_columns + typed_columns]
        typed_df = create_key_value_typed_df(
            typed_df, dtype, id_columns, typed_columns, column_name
        )
        typed_dfs.append(typed_df)
    reformate_df = pd.concat(typed_dfs)
    reformate_df = extract_missing_values(reformate_df)
    reformate_df["value_float"] = reformate_df["value_float"].astype("Float64")
    reformate_df["value_integer"] = reformate_df["value_integer"].astype(
        "Int64"
    )

    return reformate_df[FINAL_ORDERED_COLUMNS]


def create_key_value_typed_df(
    typed_df: pd.DataFrame,
    dtype: str,
    id_columns: List[str],
    typed_columns: List[str],
    column_name: str,
) -> pd.DataFrame:
    """
    Create and return a key-value typed dataframe (a dataframe with all values
    of a common type in the same column) according to the given parameters.
    """
    if dtype == "object":
        typed_df = _split_values_by_real_type(
            typed_df, id_columns, typed_columns
        )
    else:
        typed_df = typed_df.melt(
            id_vars=id_columns,
            value_vars=typed_columns,
            var_name="key",
            value_name=column_name,
        ).dropna(subset=[column_name])
    return typed_df


# pylint: disable=too-many-locals
def _split_values_by_real_type(typed_df, id_columns, typed_columns):
    """
    Certaines colonnes contiennent un mix de valeurs manquantes (de type string)
    et de valeures mesurées (integer ou float). Cette fonction a pour but
    d'affecter les bonnes valeurs aux bonnes colonnes du format clef valeur.

    Par exemple:
            finess          iqss_1
    1           12             0.1
    2           13             0.3
    3           14   Non répondant

    dtype   string          object

    Devient:
            finess      clef    value_float     missing_value
    1           12    iqss_1            0.1               NaN
    1           13    iqss_1            0.3               NaN
    1           14    iqss_1            NaN     Non répondant

    dtype   string    string        float64            string

    Pour dés raisons techniques, on est obligé de faire aussi d'autres opération
    de nettoyage à ce moment:
    - gestion des valeurs en pourcentages `"10%"` devient `.1`
    - remplacement des valeurs ne contenant que des `"."` par NaN
    """
    typed_df.loc[:, typed_columns] = typed_df.loc[:, typed_columns].replace(
        ".", np.nan, regex=False
    )
    only_numeric_df = typed_df[typed_columns].replace(
        # anything containing alphabetical characters
        r"[a-zA-Z]",
        value=np.nan,
        regex=True,
    )
    all_strings_mask = only_numeric_df.isna().all(axis="rows")
    pct_mask = (
        only_numeric_df.loc[:, ~all_strings_mask]
        .apply(lambda x: x.str.endswith("%"))
        .any()
    )
    cleaned_pct_df = only_numeric_df.loc[
        :, pct_mask & ~all_strings_mask
    ].apply(lambda col: pd.to_numeric(col.str.rstrip("%")))

    cleaned_numeric_df = only_numeric_df.loc[
        :, ~pct_mask & ~all_strings_mask
    ].apply(
        lambda col: pd.to_numeric(col).astype("Float64")
        if col.str.contains(".", regex=False).fillna(False).any()
        else pd.to_numeric(col).astype("Int64")
    )
    cleaned_int_df = cleaned_numeric_df.select_dtypes("Int64")
    cleaned_float_df = cleaned_numeric_df.select_dtypes("Float64")

    missing_values_df = (
        typed_df.loc[:, typed_columns]
        .loc[:, ~all_strings_mask]
        .mask(only_numeric_df.notna(), np.nan)
    )
    cleaned_all_str_df = typed_df.loc[:, typed_columns].loc[
        :, all_strings_mask
    ]
    dfs = [
        (cleaned_pct_df, "value_float"),
        (cleaned_float_df, "value_float"),
        (cleaned_int_df, "value_integer"),
        (missing_values_df, "missing_value"),
        (cleaned_all_str_df, "value_string"),
    ]

    cleaned_kv_dfs = []
    for clean_df, value_type in dfs:
        cleaned_kv_dfs.append(
            pd.concat([typed_df[id_columns], clean_df], axis=1)
            .melt(
                id_vars=id_columns,
                value_vars=clean_df.columns,
                var_name="key",
                value_name=value_type,
            )
            .dropna(subset=[value_type])
        )
    typed_df = pd.concat(cleaned_kv_dfs, axis="index")
    return typed_df


def extract_missing_values(iqss_df: pd.DataFrame) -> pd.DataFrame:
    """
    Extrait et harmonise les valeurs non-nulles identifiées comme valeurs
    manquantes dans une colonne dédiée nommée "missing_value"
    """
    iqss_missing_values = iqss_df["value_string"].str.match(
        "|".join(MISSING_VALUES_DICT.keys()), na=False
    )
    iqss_df.loc[iqss_missing_values, "missing_value"] = iqss_df.loc[
        iqss_missing_values, "value_string"
    ]
    iqss_df.loc[:, "missing_value"] = iqss_df.loc[:, "missing_value"].replace(
        regex=MISSING_VALUES_DICT
    )
    iqss_df.loc[iqss_missing_values, "value_string"] = np.NaN
    return iqss_df
