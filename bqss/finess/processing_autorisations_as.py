import logging
from typing import Final

import pandas as pd
import tqdm

from bqss.constants import ISO_8859_1
from bqss.finess.constants import (
    DATE_EXPORT_PATTERN,
    PREPROCESSED_AUTORISATIONS_AS_PATH,
    RAW_AUTORISATIONS_AS_PATH,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)

STRUCTUREET_HEADER: Final[str] = "structureet;nofinesset;nofinessej\n"
AUTORISATIONS_AS_HEADER: Final[str] = (
    "activiteoffresoin;nofinessej;rsej;activite;libactivite;modalite;"
    "libmodalite;forme;libforme;noautor;dateautor;nofinesset;rset;noligautor;"
    "noligautoranc;noautorarhgos;noimplarhgos;sectpsy;libsectpsy;datemeo;"
    "datefin\n"
)
COLUMNS_RENAMING_DICT: Final[dict] = {
    "datefinmeo": "datefin",
}

AS_DTYPES_DICT: Final[dict] = {
    "nofinessej": "string",
    "rsej": "string",
    "activite": "int64",
    "libactivite": "string",
    "modalite": "string",
    "libmodalite": "string",
    "forme": pd.Int64Dtype(),
    "libforme": "string",
    "noautor": "int64",
    "dateautor": "string",
    "nofinesset": "string",
    "rset": "string",
    "noligautor": pd.Int64Dtype(),
    "noligautoranc": pd.Int64Dtype(),
    "sectpsy": "string",
    "libsectpsy": "string",
    "datemeo": "string",
    "datefin": "string",
}

FINAL_COLUMNS_RENAMING_DICT: Final[dict] = {
    "nofinessej": "num_finess_ej",
    "rsej": "raison_sociale_ej",
    "libactivite": "libelle_activite",
    "libmodalite": "libelle_modalite",
    "libforme": "libelle_forme",
    "noautor": "num_autorisation",
    "dateautor": "date_autorisation",
    "nofinesset": "num_finess_et",
    "rset": "raison_sociale_et",
    "noligautor": "num_ligne_autorisee",
    "noligautoranc": "ancien_num_ligne_autorisee",
    "noautorarhgos": "num_autorisation_arhgos",
    "noimplarhgos": "num_implantation_arhgos",
    "sectpsy": "code_secteur_psy",
    "libsectpsy": "libelle_secteur_psy",
    "datemeo": "date_mise_en_oeuvre",
    "datefin": "date_fin",
    "noautoranc": "ancien_num_autorisation",
    "datemajautor": "date_maj_autorisation",
    "liborgane": "libelle_organe",
    "nomeo": "num_mise_en_oeuvre",
    "datemajmeo": "date_maj_mise_en_oeuvre",
    "appartenancecartesan": "appartenance_cartesan",
    "capaut": "capacite_autorisee",
}

FINAL_AS_DTYPES_DICT = {
    **{
        FINAL_COLUMNS_RENAMING_DICT.get(k, k): v
        for k, v in AS_DTYPES_DICT.items()
    },
    "num_autorisation_arhgos": "string",
    "num_implantation_arhgos": "string",
    "libelle_organe": "string",
    "annee_derniere_activite": "string",
    "annee_premiere_activite": "string",
}


def build_historized_autorisations_as_data() -> pd.DataFrame:
    """
    Construit et retourne un référentiel historisé des autorisations AS
    """
    logger.debug("Loading latest AS data")
    autorisations_as_df = build_latest_data()
    logger.debug("Loading historic AS data")
    autorisations_as_df = add_historic_data(autorisations_as_df)

    # remove date outliers (while keeping NA)
    autorisations_as_df.drop(
        autorisations_as_df[
            autorisations_as_df["datefin"] > "2099-01-01"
        ].index,
        inplace=True,
    )

    autorisations_as_df.rename(
        columns=FINAL_COLUMNS_RENAMING_DICT, inplace=True
    )

    return autorisations_as_df


def build_latest_data() -> pd.DataFrame:
    latest_file_path = RAW_AUTORISATIONS_AS_PATH / "latest.csv"
    latest_structureet_file_path = (
        PREPROCESSED_AUTORISATIONS_AS_PATH / "latest_structureet.csv"
    )
    latest_autorisations_as_file_path = (
        PREPROCESSED_AUTORISATIONS_AS_PATH / "latest_autorisations_as.csv"
    )

    date_export = split_latest_data(
        latest_file_path,
        latest_structureet_file_path,
        latest_autorisations_as_file_path,
    )

    latest_autorisations_as_df = pd.read_csv(
        latest_autorisations_as_file_path,
        delimiter=";",
        encoding=ISO_8859_1,
        dtype=AS_DTYPES_DICT,
    )

    # remove XML section columns
    latest_autorisations_as_df.drop(columns="activiteoffresoin", inplace=True)

    # add date_export column in first position (loc=0) since column order is
    # enforced by data validation
    latest_autorisations_as_df.insert(
        loc=0, column="date_export", value=date_export
    )

    return latest_autorisations_as_df


def split_latest_data(
    latest_file_path,
    latest_structureet_file_path,
    latest_autorisations_as_file_path,
) -> str:
    with open(latest_file_path, encoding=ISO_8859_1) as latest_file, open(
        latest_structureet_file_path, "w", encoding=ISO_8859_1
    ) as structureet_file, open(
        latest_autorisations_as_file_path, "w", encoding=ISO_8859_1
    ) as autorisations_as_file:
        structureet_file.write(STRUCTUREET_HEADER)
        autorisations_as_file.write(AUTORISATIONS_AS_HEADER)
        for line in latest_file:
            if line.startswith("structureet"):
                structureet_file.write(line)
            elif line.startswith("activiteoffresoin"):
                autorisations_as_file.write(line)
            elif line.startswith("finess"):
                # should only appear once in the whole file
                date_export = line.split(";")[-1].strip()
            else:
                logger.warning("Unknown CSV line : %s", line)
        return date_export


def add_historic_data(autorisations_as_df):
    historized_autorisations_as_df = autorisations_as_df.copy(deep=True)
    historic_path = RAW_AUTORISATIONS_AS_PATH / "historic"
    dfs = []
    for filepath in tqdm.tqdm(
        historic_path.glob("*.csv"),
        desc="Loading historic autorisations AS data",
    ):
        historic_df = pd.read_csv(
            filepath,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=AS_DTYPES_DICT,
        )
        date_export = "-".join(
            DATE_EXPORT_PATTERN.match(filepath.stem).groups()
        )
        preprocess_historic_data(historic_df, date_export)
        dfs.append(historic_df)
    return pd.concat([historized_autorisations_as_df] + dfs)


def preprocess_historic_data(historic_df, date_export):
    """
    Prétraite les données historiques d'Autorisations AS avant de pouvoir les
    merger avec la dernière extraction
    """
    historic_df["date_export"] = date_export

    historic_df.rename(columns=COLUMNS_RENAMING_DICT, inplace=True)
