import logging
from typing import Final

import numpy as np
import pandas as pd
import tqdm
from pyproj import CRS, Transformer

from bqss.constants import ISO_8859_1
from bqss.finess.constants import (
    DATE_EXPORT_PATTERN,
    FINAL_COLUMNS_RENAMING_DICT,
    PREPROCESSED_FINESS_ET_PATH,
    RAW_FINESS_ET_PATH,
    TYPE_VOIE_MAPPING,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)

STRUCTUREET_HEADER: Final[str] = (
    "structureet;nofinesset;nofinessej;rs;rslongue;complrs;compldistrib;"
    "numvoie;typvoie;voie;compvoie;lieuditbp;commune;departement;"
    "libdepartement;ligneacheminement;telephone;telecopie;categetab;"
    "libcategetab;categagretab;libcategagretab;siret;codeape;codemft;libmft;"
    "codesph;libsph;dateouv;dateautor;datemaj;numuai\n"
)
GEOLOCALISATION_HEADER: Final[
    str
] = "geolocalisation;nofinesset;coordxet;coordyet;sourcecoordet;datemaj\n"

FINESS_DTYPES_DICT: Final[dict] = {
    # "string" to pandas string type
    # https://pandas.pydata.org/pandas-docs/stable/user_guide/text.html
    "nofinesset": "string",
    "nofinessej": "string",
    "rs": "string",
    "rslongue": "string",
    "complrs": "string",
    "compldistrib": "string",
    # some street numbers aren't numeric
    "numvoie": "string",
    "typvoie": "string",
    "voie": "string",
    "compvoie": "string",
    "lieuditbp": "string",
    "commune": pd.Int64Dtype(),
    "region": pd.Int64Dtype(),
    "libregion": "string",
    "departement": "string",
    "libdepartement": "string",
    "cog": "string",
    "codepostal": "string",
    "libelle_routage": "string",
    "ligneacheminement": "string",
    "telephone": "string",
    "telecopie": "string",
    "categetab": pd.Int64Dtype(),
    "libcategetab": "string",
    "liblongcategetab": "string",
    "categretab": pd.Int64Dtype(),
    "libcategretab": "string",
    "siret": "string",
    "codeape": "string",
    "libcodeape": "string",
    "mft": "string",
    "libmft": "string",
    "liblongmft": "string",
    "codesph": pd.Int64Dtype(),
    "sph": pd.Int64Dtype(),
    "libsph": "string",
    "numen": "string",
    "sourcegeocod": "string",
    "dategeocod": "string",
    "dateautor": "string",
    "dateouvert": "string",
    "datemaj": "string",
    # columns below are needed at the beginning of the pipeline but do not
    # appear in the final result (they will be renamed/deleted along the way)
    "coordxet": "float64",
    "coordyet": "float64",
    "sourcecoordet": "string",
}

COLUMNS_DELETION_LIST: Final[list] = [
    "libcategetab",
    "libmft",
]

HISTORIC_COLUMNS_RENAMING_DICT: Final[dict] = {
    "categretab": "categagretab",
    "coordx": "coordxet",
    "coordy": "coordyet",
    "dateouvert": "dateouv",
    "libcategretab": "libcategagretab",
    "liblongcategetab": "libcategetab",
    "liblongmft": "libmft",
    "mft": "codemft",
    "numen": "numuai",
    "sourcegeocod": "sourcecoordet",
    "sph": "codesph",
}

FINAL_FINESS_DTYPES_DICT = {
    **{
        FINAL_COLUMNS_RENAMING_DICT.get(
            HISTORIC_COLUMNS_RENAMING_DICT.get(k, k), k
        ): v
        for k, v in FINESS_DTYPES_DICT.items()
    },
    **{
        "statut_juridique_ej": pd.Int64Dtype(),
        "libelle_commune": "string",
        "adresse_postale_voie": "string",
        "adresse_postale_commune": "string",
    },
}

CLEANING_SIRET_DICT: Final[dict] = {
    "242600666²²²²²": "24260066600083",
}

WGS84_EPSG_CODE: Final[int] = 4326
FINESS_GEODETIC_CRS: Final[dict] = {
    "LAMBERT_93": 2154,  # RGF93 : France métropolitaine
    "UTM_N20": 5490,  # RGAF09 : Guadeloupe, Martinique
    "UTM_N21": 4467,  # RGSPM06 : Saint Pierre et Miquelon
    "UTM_N22": 2972,  # RGFG95 : Guyane
    "UTM_S38": 4471,  # RGM04 : Mayotte
    "UTM_S40": 2975,  # RGR92 : Réunion
}


def build_historized_finess_data() -> pd.DataFrame:
    """
    Construit et retourne un référentiel historisé des FINESS
    """
    logger.debug("Chargement des données FINESS les plus récentes")
    finess_df = build_latest_data()
    logger.debug("loading historic finess data")
    finess_df = add_historic_data(finess_df)

    # ajout du champ "fermé cette année"
    finess_df["ferme_cette_annee"] = False
    dates = np.sort(finess_df["date_export"].unique())
    for i, _ in enumerate(dates):
        if i == len(dates) - 1:
            break
        current_finess = finess_df[finess_df["date_export"] == dates[i]]
        next_finess = finess_df[finess_df["date_export"] == dates[i + 1]]
        closed_this_year = current_finess[
            ~current_finess["nofinesset"].isin(next_finess["nofinesset"])
        ]
        finess_df.loc[
            (finess_df["nofinesset"].isin(closed_this_year["nofinesset"]))
            & (finess_df["date_export"] == dates[i]),
            "ferme_cette_annee",
        ] = True

    finess_df = convert_coordinates_to_wgs84(finess_df)

    finess_df.rename(columns=FINAL_COLUMNS_RENAMING_DICT, inplace=True)

    finess_df = add_adresse_postale(finess_df)

    logger.info(
        "%d FINESS ET ont étés historisés sur %d enregistrements",
        len(pd.unique(finess_df["num_finess_et"])),
        len(finess_df),
    )

    return finess_df


def build_latest_data() -> pd.DataFrame:
    latest_file_path = RAW_FINESS_ET_PATH / "latest.csv"
    latest_structureet_file_path = (
        PREPROCESSED_FINESS_ET_PATH / "latest_structureet.csv"
    )
    latest_geolocalisation_file_path = (
        PREPROCESSED_FINESS_ET_PATH / "latest_geolocalisation.csv"
    )

    date_export = split_latest_data(
        latest_file_path,
        latest_structureet_file_path,
        latest_geolocalisation_file_path,
    )
    merged_df = merge_latest_data(
        latest_structureet_file_path, latest_geolocalisation_file_path
    )

    # add date_export column in first position (loc=0) since column order is
    # enforced by data validation
    merged_df.insert(loc=0, column="date_export", value=date_export)

    merged_df = filter_out_unwanted_categories_et(
        merged_df,
        categorie_agregat_et_col_name="categagretab",
    )

    return merged_df


def split_latest_data(
    latest_file_path,
    latest_structureet_file_path,
    latest_geolocalisation_file_path,
) -> str:
    with open(latest_file_path, encoding=ISO_8859_1) as latest_file, open(
        latest_structureet_file_path, "w", encoding=ISO_8859_1
    ) as structureet_file, open(
        latest_geolocalisation_file_path, "w", encoding=ISO_8859_1
    ) as geolocalisation_file:
        structureet_file.write(STRUCTUREET_HEADER)
        geolocalisation_file.write(GEOLOCALISATION_HEADER)
        for line in latest_file:
            if line.startswith("structureet"):
                structureet_file.write(line)
            elif line.startswith("geolocalisation"):
                geolocalisation_file.write(line)
            elif line.startswith("finess"):
                # should only appear once in the whole file
                date_export = line.split(";")[-1].strip()
            else:
                logger.warning("Unknown CSV line : %s", line)
        return date_export


def merge_latest_data(
    latest_structureet_file_path, latest_geolocalisation_file_path
):
    latest_finess_df = pd.read_csv(
        latest_structureet_file_path,
        delimiter=";",
        encoding=ISO_8859_1,
        dtype=FINESS_DTYPES_DICT,
    )
    latest_geolocalisation_df = pd.read_csv(
        latest_geolocalisation_file_path,
        delimiter=";",
        encoding=ISO_8859_1,
        dtype=FINESS_DTYPES_DICT,
    )
    # remove XML section columns
    latest_finess_df.drop(columns="structureet", inplace=True)
    latest_geolocalisation_df.drop(columns="geolocalisation", inplace=True)
    # renaming
    latest_geolocalisation_df.rename(
        columns={"datemaj": "dategeocod"}, inplace=True
    )
    # merge df
    merged_df = pd.merge(
        left=latest_finess_df,
        right=latest_geolocalisation_df,
        how="left",
        left_on="nofinesset",
        right_on="nofinesset",
    )
    return merged_df


def add_historic_data(finess_df):
    historized_finess_df = finess_df.copy(deep=True)
    historic_path = RAW_FINESS_ET_PATH / "historic"
    dfs = []
    for filepath in tqdm.tqdm(
        historic_path.glob("*.csv"),
        desc="Loading historic finess ET data",
    ):
        historic_df = pd.read_csv(
            filepath,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=FINESS_DTYPES_DICT,
            # redressement des coordonnées x,y Lambert 93 encodées en
            # French_France.1252 sur data gouv (virgules tous les 10^3). Ex :
            # pour l'ES 750034308, on a en entrée coordx = 655,097.8 et
            # coordy = 6,864,227.1 => on veut en sortie 655097.8 et 6864227.1
            # voir https://gitlab.com/has-sante/public/bqss/-/merge_requests/17#note_630165123
            converters={
                col: lambda val: float(val.replace(",", "")) if val else pd.NA
                for col in ["coordx", "coordy"]
            },
        )
        historic_df = filter_out_unwanted_categories_et(
            historic_df,
            categorie_agregat_et_col_name="categretab",
        )
        # preprocessing before merging
        date_export = "-".join(
            DATE_EXPORT_PATTERN.match(filepath.stem).groups()
        )
        preprocess_historic_data(historic_df, date_export)
        dfs.append(historic_df)
    return pd.concat([historized_finess_df] + dfs)


def preprocess_historic_data(historic_df, date_export):
    """
    Prétraite les données historiques FINESS avant de pouvoir les merger avec la
    dernière extraction
    """
    historic_df["date_export"] = date_export

    historic_df.drop(columns=COLUMNS_DELETION_LIST, inplace=True)
    historic_df.rename(columns=HISTORIC_COLUMNS_RENAMING_DICT, inplace=True)

    # clean data
    historic_df["siret"].replace(CLEANING_SIRET_DICT, inplace=True)


def convert_coordinates_to_wgs84(
    finess_df: pd.DataFrame,
) -> pd.DataFrame:
    """
    Converti les coordonnées des différents systèmes géodésiques utilisés dans
    les données finess en open data en coordonnées WGS84
    """
    finess_df["geodetic_crs"] = (
        finess_df["sourcecoordet"].str.split(",|;").str[-1]
    )
    transformed_finess = []
    for crs, epsg_code in FINESS_GEODETIC_CRS.items():
        transformer = Transformer.from_crs(
            crs_from=CRS.from_epsg(epsg_code),
            crs_to=CRS.from_epsg(WGS84_EPSG_CODE),
        )
        finess_to_transform = finess_df[finess_df["geodetic_crs"] == crs]
        finess_to_transform = finess_to_transform.dropna(
            subset=["coordxet", "coordyet"]
        )
        # pylint: disable=unpacking-non-sequence
        lat, lon = transformer.transform(
            finess_to_transform["coordxet"].to_numpy(dtype="float64"),
            finess_to_transform["coordyet"].to_numpy(dtype="float64"),
        )
        finess_to_transform["latitude"] = lat
        finess_to_transform["longitude"] = lon
        transformed_finess.append(
            finess_to_transform[
                ["nofinesset", "date_export", "latitude", "longitude"]
            ]
        )
    transformed_finess = pd.concat(transformed_finess)
    finess_df = pd.merge(
        left=finess_df,
        right=transformed_finess,
        how="left",
        on=["nofinesset", "date_export"],
        validate="one_to_one",
    )
    finess_df.drop(columns="geodetic_crs", inplace=True)
    return finess_df


def add_adresse_postale(
    finess_df: pd.DataFrame,
):
    """
    Cette fonction ajoute les champs "adresse_postale_ligne_1" et
    "adresse_postale_ligne_2" (en plus de quelques champs intermédiaires)
    pour faciliter l'affichage de l'adresse d'un établissement sur le site web.
    Exemple :
    adresse_postale_ligne_1 : 17 RUE DES FAUVETTES
    adresse_postale_ligne_2 : 92321 CHATILLON
    """
    missing_cp_mask = finess_df["code_postal"].isna()
    temp = finess_df.loc[missing_cp_mask, "ligne_acheminement"].str.extract(
        #  code postal                   commune   cedex ignoré
        r"([0-9]{1}[a-zA-Z0-9]{1}\d{3})\s([\w ]+?)(?:\sCEDEX$)?$",
        expand=True,
    )
    finess_df.loc[missing_cp_mask, "code_postal"] = temp[0]
    finess_df.loc[missing_cp_mask, "libelle_commune"] = temp[1]

    # pour les finess qui disposent déjà d'un CP, libelle_routage semble contenir le nom de la commune
    finess_df["libelle_commune"].fillna(
        finess_df["libelle_routage"], inplace=True
    )
    finess_df["type_voie"] = finess_df["type_voie"].map(
        TYPE_VOIE_MAPPING, na_action="ignore"
    )  #  Quand on ne sait pas faire le mapping, on met une string vide
    finess_df["adresse_postale_ligne_1"] = (
        finess_df["num_voie"]
        + " "
        + finess_df["type_voie"]
        + " "
        + finess_df["libelle_voie"]
    ).str.title()
    finess_df["adresse_postale_ligne_2"] = (
        finess_df["code_postal"] + " " + finess_df["libelle_commune"]
    )
    return finess_df


def filter_out_unwanted_categories_et(
    finess_df: pd.DataFrame,
    *,
    categorie_agregat_et_col_name: str = "categagretab",
):
    """
    Cette fonction filtre les établissements appartenant à des catégories
    d'établissement jugées non pertinentes pour cette base de données
    """
    return finess_df[
        (
            (finess_df[categorie_agregat_et_col_name] >= 1000)
            & (finess_df[categorie_agregat_et_col_name] < 2000)
        )
        | (finess_df[categorie_agregat_et_col_name] == 2204)
        | (finess_df[categorie_agregat_et_col_name] == 2205)
    ]
