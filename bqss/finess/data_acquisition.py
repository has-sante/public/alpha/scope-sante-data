import glob
import logging
import os
import shutil
import urllib.error
import urllib.request
import zipfile
from typing import Final

from bqss.finess.constants import (
    RAW_AUTORISATIONS_AS_PATH,
    RAW_DOMAIN_PATH,
    RAW_FINESS_EJ_PATH,
    RAW_FINESS_ET_PATH,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)


def download_data(data_sources):
    """
    Télécharge toutes les données du domaine de données FINESS
    """
    logger.info("Downloading data...")

    # clean directory structure
    shutil.rmtree(RAW_DOMAIN_PATH, ignore_errors=True)
    os.makedirs(RAW_FINESS_ET_PATH, exist_ok=True)
    os.makedirs(RAW_FINESS_EJ_PATH, exist_ok=True)
    os.makedirs(RAW_AUTORISATIONS_AS_PATH, exist_ok=True)

    for source_name in ["finesset", "finessej", "autorisations_as"]:
        try:
            download_latest(
                source_name,
                data_sources["sources"][source_name]["files"]["latest"],
            )
            download_historic(
                source_name,
                data_sources["sources"][source_name]["files"]["historic"],
            )
        except urllib.error.HTTPError as exception:
            raise ValueError(
                "Impossible de télécharger les données FINESS, "
                "elles ont probablement été mises à jour. "
                "Essayez de mettre à jour le lien dans resources/finess/data_sources.yml"
            ) from exception

    logger.info("Downloading data OK")


def download_latest(source_name, latest_data_source):
    """
    Télécharge la dernière extraction de la source en paramètre
    """
    urllib.request.urlretrieve(  # nosec
        latest_data_source, RAW_DOMAIN_PATH / source_name / "latest.csv"
    )


def download_historic(source_name, historic_data_source):
    """
    Télécharge l'historique des données de la source en paramètre
    """
    source_path = RAW_DOMAIN_PATH / source_name
    file_path = source_path / "historic.zip"
    urllib.request.urlretrieve(historic_data_source, file_path)  # nosec
    with zipfile.ZipFile(file_path, "r") as zip_f:
        zip_f.extractall(source_path)
    # rename etalab directory since it contains hard-coded years
    path_names = glob.glob(os.path.join(source_path, "etalab*"))
    os.rename(path_names[0], os.path.join(source_path, "historic"))
