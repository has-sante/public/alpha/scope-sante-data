import re
from pathlib import Path
from typing import Final

from bqss.constants import DATA_PATH, RESOURCES_PATH, SCHEMAS_PATH

RESOURCES_DOMAIN_PATH: Final[Path] = RESOURCES_PATH / "finess"
SCHEMAS_DOMAIN_PATH: Final[Path] = SCHEMAS_PATH / "finess"
DATA_DOMAIN_PATH: Final[Path] = DATA_PATH / "finess"
RAW_DOMAIN_PATH: Final[Path] = DATA_DOMAIN_PATH / "raw"
PREPROCESSED_DOMAIN_PATH: Final[Path] = DATA_DOMAIN_PATH / "preprocessed"
FINAL_DOMAIN_PATH: Final[Path] = DATA_DOMAIN_PATH / "final"
# FINESS ET paths
RAW_FINESS_ET_PATH: Final[Path] = RAW_DOMAIN_PATH / "finesset"
PREPROCESSED_FINESS_ET_PATH: Final[Path] = (
    PREPROCESSED_DOMAIN_PATH / "finesset"
)
# FINESS EJ paths
RAW_FINESS_EJ_PATH: Final[Path] = RAW_DOMAIN_PATH / "finessej"
# Autorisations d'activités de soins (AS) paths
RAW_AUTORISATIONS_AS_PATH: Final[Path] = RAW_DOMAIN_PATH / "autorisations_as"
PREPROCESSED_AUTORISATIONS_AS_PATH: Final[Path] = (
    PREPROCESSED_DOMAIN_PATH / "autorisations_as"
)
# Activités ET path
PREPROCESSED_ACTIVITES_ET_PATH: Final[Path] = (
    PREPROCESSED_DOMAIN_PATH / "activites_et"
)

DATE_EXPORT_PATTERN = re.compile(r".*([0-9]{4})([0-9]{2})([0-9]{2})")

FINAL_COLUMNS_RENAMING_DICT: Final[dict] = {
    "nofinesset": "num_finess_et",
    "nofinessej": "num_finess_ej",
    "rs": "raison_sociale",
    "rslongue": "raison_sociale_longue",
    "complrs": "complement_raison_sociale",
    "compldistrib": "complement_distribution",
    "numvoie": "num_voie",
    "typvoie": "type_voie",
    "voie": "libelle_voie",
    "compvoie": "complement_voie",
    "lieuditbp": "lieu_dit_bp",
    "libdepartement": "libelle_departement",
    "ligneacheminement": "ligne_acheminement",
    "categetab": "categorie_et",
    "libcategetab": "libelle_categorie_et",
    "categagretab": "categorie_agregat_et",
    "libcategagretab": "libelle_categorie_agregat_et",
    "codeape": "code_ape",
    "codemft": "code_mft",
    "libmft": "libelle_mft",
    "codesph": "code_sph",
    "libsph": "libelle_sph",
    "dateouv": "date_ouverture",
    "dateautor": "date_autorisation",
    "datemaj": "date_maj",
    "numuai": "num_uai",
    "coordxet": "coord_x_et",
    "coordyet": "coord_y_et",
    "sourcecoordet": "source_coord_et",
    "dategeocod": "date_geocodage",
    "libregion": "libelle_region",
    "cog": "code_officiel_geo",
    "codepostal": "code_postal",
    "libcodeape": "libelle_code_ape",
    # EJ specific
    "statutjuridique": "statut_juridique",
    "libstatutjuridique": "libelle_statut_juridique",
    "categej": "categorie_ej",
    "libcategej": "libelle_categorie_ej",
    "datecrea": "date_creation",
}


class DefaultKeyDict(dict):
    def __missing__(self, key):
        return key


# source https://gist.github.com/384400/bf3c83a4e7d1aa66a87e#file-voies-html-L251
TYPE_VOIE_MAPPING = DefaultKeyDict(
    {
        "ABE": "Abbaye",
        "ACH": "Ancien Chemin",
        "AIRE": "Aire",
        "ALL": "Allée",
        "ANSE": "Anse",
        "ARC": "Arc",
        "ART": "Ancienne Route",
        "AUT": "Avenue",  #  who knows..
        "AV": "Avenue",
        "BCLE": "Boucle",
        "BD": "Boulevard",
        "BOIS": "Bois",
        "BRG": "Bourg",
        "BUT": "Butte",
        "CALE": "Cale",
        "CAMP": "Camp",
        "CAR": "Carrefour",
        "CARE": "Carrière",
        "CARR": "Carré",
        "CAV": "Cavée",
        "CCAL": "Centre Commercial",
        "CGNE": "Campagne",
        "CHE": "Chemin",
        "CHEM": "Chemin",
        "CHEZ": "Chez",
        "CHI": "Centre Hospitalier Intercommunal",  # Probablement une typo en amont...
        "CHL": "Centre Hospitalier Local",  # Au flaire, pas sûr du tout
        "CHS": "Chaussée",
        "CHT": "Château",
        "CHV": "Chemin Vicinal",
        "CITE": "Cité",
        "CLOS": "Clos",
        "COL": "Col",
        "COLI": "Colline",
        "COR": "Corniche",
        "COTE": "Cote",
        "COUR": "Cour",
        "CRS": "Cours",
        "CTR": "Centre",
        "CTRE": "Centre",
        "DIG": "Digue",
        "DOM": "Domaine",
        "DSC": "Descente",
        "ECA": "Ecart",
        "EN": "EN",  # pour entreprise mais EN semble être la bonne valeur
        "ENC": "Enclos",
        "ESP": "Esplanade",
        "ESPA": "Espace",
        "FG": "Faubourg",
        "FON": "Fontaine",
        "FORM": "Forum",
        "FORT": "Fort",
        "FOS": "Fosse",
        "FOYR": "Foyer",
        "FRM": "Ferme",
        "GAL": "Galerie",
        "GARE": "Gare",
        "GPE": "Groupe",  # probablement une mauvaise donnée...
        "GR": "Grande Rue",
        "HAM": "Hameau",
        "HLE": "Halle",
        "HLM": "HLM",
        "ILE": "Île",
        "IMM": "Immeuble",
        "IMP": "Impasse",
        "JARD": "jardin",
        "LD": "Lieu-dit",
        "LEVE": "Levée",
        "LOT": "Lotissement",
        "MAIL": "Mail",
        "MAN": "Manoir",
        "MAR": "Marché",
        "MAS": "Mas",
        "MLN": "Moulin",
        "MTE": "Montée",
        "PAE": "Petite Avenue",  # Ça sent l'ego...
        "PARC": "Parc",
        "PAS": "Passage",
        "PASS": "Passage",
        "PAT": "Patio",
        "PAV": "Pavillon",
        "PCH": "Porche",
        "PERI": "Périphérique",
        "PKG": "Parking",
        "PL": "Place",
        "PLAG": "Plage",
        "PLAN": "Plam",
        "PLE": "Place",
        "PLN": "Plaine",
        "PLT": "Plateau",
        "PNT": "Pointe",
        "PONT": "Pont",
        "PORT": "Port",
        "PRE": "Pré",
        "PRO": "Promenade",
        "PROM": "Promenage",
        "PRT": "Petite Route",  # Mauvaise donnée
        "PRV": "Parvis",
        "PTE": "Porte",
        "QU": "Quai",
        "QUA": "Quartier",
        "QUAI": "Quai",
        "R": "Rue",
        "REM": "Rempart",
        "RES": "Résidence",
        "RLE": "Ruelle",
        "ROC": "Rocade",
        "RPE": "Rampe",
        "RPT": "Rond-point",
        "RTD": "Rotonde",
        "RTE": "Route",
        "RUE": "Rue",
        "SEN": "Sente - Sentier",
        "SQ": "Square",
        "STA": "Stade",
        "TOUR": "Tour",
        "TPL": "Terre-plein",
        "TRA": "Traverse",
        "TRN": "Terrain",
        "TSSE": "Terrasse",
        "VAL": "Val",
        "VCHE": "Vieux Chemin",
        "VEN": "Venelle",
        "VGE": "Village",
        "VIA": "Via",
        "VLA": "Villa",
        "VLGE": "Village",
        "VOI": "Voie",
        "VTE": "Vielle Route",
        "ZA": "Zone d'activité",
        "ZAC": "Zone d'activité",
        "ZAD": "Zone d’aménagement différé",
        "ZI": "Zone industrielle",
        "ZONE": "Zone",
        "ZUP": "Zone à urbaniser en priorité",
    }
)

HISTORIZED_AUTORISATIONS_AS_COLUMNS: Final[list] = [
    "date_mise_en_oeuvre",
    "date_export",
    "date_fin",
    "num_finess_et",
    "libelle_activite",
    "libelle_modalite",
    "activite",
    "modalite",
    "forme",
    "annee_premiere_activite",
    "annee_derniere_activite",
]
