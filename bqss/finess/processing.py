import logging
from typing import Final

import pandas as pd

from bqss.constants import UTF_8
from bqss.data_acquisition_utils import clean_directory
from bqss.finess.constants import (
    FINAL_DOMAIN_PATH,
    HISTORIZED_AUTORISATIONS_AS_COLUMNS,
    PREPROCESSED_ACTIVITES_ET_PATH,
    PREPROCESSED_AUTORISATIONS_AS_PATH,
    PREPROCESSED_FINESS_ET_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.finess.processing_activites_et import (
    build_activites_et_data,
    finalize_activites_et_data,
)
from bqss.finess.processing_autorisations_as import (
    build_historized_autorisations_as_data,
)
from bqss.finess.processing_finess import build_historized_finess_data
from bqss.finess.processing_finess_ej import build_historized_finess_ej_data
from bqss.metadata_utils import table_schema_to_csv

logger: Final[logging.Logger] = logging.getLogger(__name__)


def process_data():
    """
    Traite les données du domaine de données FINESS
    """
    logger.info("Début des traitements FINESS")

    clean_directory_tree()

    finess_et = build_historized_finess_data()
    finess_ej = build_historized_finess_ej_data()
    finess_et_ej = add_fields_from_ej(finess_et, finess_ej)

    raw_activites_et = process_autorisations_as_data()

    final_activite_et = process_activites_et_data(raw_activites_et)

    logger.info("Fin des traitements FINESS")

    return (
        finess_et,
        finess_ej,
        finess_et_ej,
        raw_activites_et,
        final_activite_et,
    )


def clean_directory_tree():
    """
    Efface et reconstruit l'arborescence des dossiers
    """
    for path in [
        PREPROCESSED_FINESS_ET_PATH,
        PREPROCESSED_AUTORISATIONS_AS_PATH,
        PREPROCESSED_ACTIVITES_ET_PATH,
        FINAL_DOMAIN_PATH,
    ]:
        clean_directory(path)


def process_autorisations_as_data() -> pd.DataFrame:
    """
    Traite la source de données Autorisations AS (actes de soins) du domaine de
    données FINESS et retourne les données d'activités de soins des ET
    """
    historized_autorisations_as_data = build_historized_autorisations_as_data()
    logger.info(
        "%d autorisations AS ont étés historisées sur %d enregistrements",
        len(
            pd.unique(
                historized_autorisations_as_data[
                    [
                        "num_finess_et",
                        "num_autorisation",
                        "num_ligne_autorisee",
                        "num_mise_en_oeuvre",
                    ]
                ].values.ravel("K")
            )
        ),
        len(historized_autorisations_as_data),
    )

    historized_autorisations_as_data = add_actual_autorisation_end(
        historized_autorisations_as_data
    )

    historized_autorisations_as_data.to_csv(
        FINAL_DOMAIN_PATH / "autorisations_as.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    logger.debug("Données FINESS AS historisée écrite sur disque")
    return historized_autorisations_as_data[
        HISTORIZED_AUTORISATIONS_AS_COLUMNS
    ]


def add_actual_autorisation_end(autorisations_as: pd.DataFrame):
    autorisations_df = autorisations_as.copy()
    # Création de la variable "key_activite" déterminant uniquement le type d'activité
    autorisations_df["key_activite"] = (
        autorisations_df["activite"].astype(str)
        + "_"
        + autorisations_df["modalite"].astype(str)
        + "_"
        + autorisations_df["forme"].astype(str)
    )

    # Typage des dates
    date_cols = ["date_mise_en_oeuvre", "date_export", "date_fin"]
    new_cols = [f"{c}_y" for c in date_cols]
    autorisations_df[new_cols] = (
        autorisations_df[date_cols]
        .apply(pd.to_datetime)
        .apply(lambda x: x.dt.year)
    )

    # Création de la variable "Date de dernière activité"
    dates_max_df = (
        autorisations_df.groupby(by=["key_activite", "num_finess_et"])
        .agg({"date_export_y": "max", "date_fin_y": "max"})
        .reset_index()
        .rename(
            columns={
                "date_export_y": "date_export_max",
                "date_fin_y": "date_fin_max",
            }
        )
    )
    autorisations_df = autorisations_df.merge(
        dates_max_df, on=["key_activite", "num_finess_et"]
    )
    autorisations_df["annee_derniere_activite"] = autorisations_df[
        ["date_export_max", "date_fin_max"]
    ].min(axis=1)
    autorisations_df.drop(
        ["date_export_max", "date_fin_max"], axis=1, inplace=True
    )

    # Création de la variable "Date de première donnée d'activité"
    dates_min_df = (
        autorisations_df.groupby(by=["key_activite", "num_finess_et"])
        .agg({"date_export_y": "min", "date_mise_en_oeuvre_y": "min"})
        .reset_index()
        .rename(
            columns={
                "date_export_y": "date_export_min",
                "date_mise_en_oeuvre_y": "date_mise_en_oeuvre_min",
            }
        )
    )
    autorisations_df = autorisations_df.merge(
        dates_min_df, on=["key_activite", "num_finess_et"]
    )
    autorisations_df["annee_premiere_activite"] = autorisations_df[
        ["date_export_min", "date_mise_en_oeuvre_min"]
    ].max(axis=1)
    autorisations_df.drop(
        columns=[
            "date_export_min",
            "date_mise_en_oeuvre_min",
            "date_export_y",
            "date_mise_en_oeuvre_y",
            "date_fin_y",
            "key_activite",
        ],
        inplace=True,
    )
    autorisations_df["annee_derniere_activite"] = autorisations_df[
        "annee_derniere_activite"
    ].astype("int")

    autorisations_df["annee_premiere_activite"] = autorisations_df[
        "annee_premiere_activite"
    ].astype("int")

    return autorisations_df


def process_activites_et_data(raw_activites_et: pd.DataFrame) -> pd.DataFrame:
    """
    Construit le référentiel des activités de soins basé sur les données
    d'autorisations
    """
    logger.debug(
        "Traitement des données d'autorisation pour produire activites_et"
    )
    processed_activites_et = build_activites_et_data(raw_activites_et)

    logger.debug("Formattage d'activite_et en clef-valeur")
    final_activites_et = finalize_activites_et_data(processed_activites_et)
    logger.info(
        "Les activités de %d établissements ont étées consolidées sur %d enregistrements",
        final_activites_et["finess"].nunique(),
        len(final_activites_et),
    )

    final_activites_et.to_csv(
        FINAL_DOMAIN_PATH / "activites_et_key_value.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    logger.debug("Activités des établissements écrites sur disque")

    # genere un fichier metadata à partir du table schema
    table_schema_to_csv(
        SCHEMAS_DOMAIN_PATH / "activites_et.json",
        FINAL_DOMAIN_PATH / "activites_et_metadata.csv",
    )
    logger.info("Metadatas pour les activités des établissements crées")

    return final_activites_et


def add_fields_from_ej(
    finess_et: pd.DataFrame,
    finess_ej: pd.DataFrame,
) -> pd.DataFrame:
    """
    Cette fonction ajoute au référentiel des finess ET (géographiques) certains
    champs relatifs à l'EJ liée (exemple : statut juridique, raison sociale,...)
    """
    finess_et_ej = pd.merge(
        left=finess_et,
        right=finess_ej[
            [
                "date_export",
                "num_finess_ej",
                "raison_sociale",
                "raison_sociale_longue",
                "statut_juridique",
                "libelle_statut_juridique",
            ]
        ],
        how="left",
        on=["num_finess_ej", "date_export"],
        suffixes=("_et", "_ej"),
    )

    missing_ej_mask = finess_et_ej["statut_juridique"].isna()

    if missing_ej_mask.any():
        last_known_ej_df = (
            finess_ej.loc[
                finess_ej["num_finess_ej"].isin(
                    finess_et.loc[missing_ej_mask, "num_finess_ej"]
                )
            ]
            .sort_values(["num_finess_ej", "date_export"])
            .drop_duplicates(subset=["num_finess_ej"], keep="last")
        )
        missing_ej = pd.merge(
            left=finess_et.loc[missing_ej_mask],
            right=last_known_ej_df[
                [
                    "num_finess_ej",
                    "raison_sociale",
                    "raison_sociale_longue",
                    "statut_juridique",
                    "libelle_statut_juridique",
                ]
            ],
            how="left",
            on=["num_finess_ej"],
            suffixes=("_et", "_ej"),
        )
        finess_et_ej = finess_et_ej.loc[
            finess_et_ej["statut_juridique"].notna()
        ]
        finess_et_ej = pd.concat([finess_et_ej, missing_ej]).reset_index(
            drop=True
        )
        logger.warning(
            "Les finess géographiques suivants n'ont pas "
            "de finess juridique associés pour l'année donnée: %s. "
            "Nous avons utilisé les derniers finess juridiques connus "
            "pour les compléter.",
            finess_et.loc[missing_ej_mask, "num_finess_et"].tolist(),
        )
    # nous ne pouvons pas caster en "int64" car certains ET sont liés à des EJ
    # qui n'existent pas dans notre référentiel finess EJ !
    finess_et_ej["statut_juridique"] = finess_et_ej["statut_juridique"].astype(
        "Int64"
    )
    finess_et_ej.rename(
        columns={
            "statut_juridique": "statut_juridique_ej",
            "libelle_statut_juridique": "libelle_statut_juridique_ej",
        },
        inplace=True,
    )
    # ajout de la valeur du statut juridique spécifique à QualiScope
    # document de référence : http://finess.sante.gouv.fr/fininter/jsp/pdf.do?xsl=StatutJuridique.xsl
    finess_et_ej.loc[
        finess_et_ej["statut_juridique_ej"] <= 30, "statut_juridique"
    ] = "Public"
    finess_et_ej.loc[
        (
            (finess_et_ej["statut_juridique_ej"] >= 40)
            & (finess_et_ej["statut_juridique_ej"] <= 66)
        )
        | (finess_et_ej["statut_juridique_ej"] == 89),
        "statut_juridique",
    ] = "Privé à but non lucratif"
    finess_et_ej.loc[
        (
            (finess_et_ej["statut_juridique_ej"] >= 70)
            & (finess_et_ej["statut_juridique_ej"] <= 88)
        )
        | (
            (finess_et_ej["statut_juridique_ej"] >= 91)
            & (finess_et_ej["statut_juridique_ej"] <= 95)
        ),
        "statut_juridique",
    ] = "Privé"

    # Champ type d'établissement
    # document de référence : http://finess.sante.gouv.fr/fininter/jsp/pdf.do?xsl=CategEta.xsl
    finess_et_ej["type_etablissement"] = finess_et_ej["statut_juridique"]
    finess_et_ej.loc[
        finess_et_ej["categorie_et"].isin([355, 106]), "type_etablissement"
    ] = "CH"
    finess_et_ej.loc[
        finess_et_ej["categorie_et"] == 131, "type_etablissement"
    ] = "CLCC"
    finess_et_ej.loc[
        finess_et_ej["raison_sociale_et"].str.contains("CHU "),
        "type_etablissement",
    ] = "CHU"

    mask = (finess_et_ej["categorie_et"] == 101) & (
        finess_et_ej["raison_sociale_et"].str.contains(
            "HU|C H U|CHRU|C.H.U.|GHU|HUS|CHR", regex=True
        )
    )
    finess_et_ej.loc[
        mask,
        "type_etablissement",
    ] = "CHU"

    finess_et_ej.to_csv(
        FINAL_DOMAIN_PATH / "finess.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )

    logger.debug("Données FINESS historisées écrites sur le disque")

    return finess_et_ej
