import logging
from typing import Final

import numpy as np
import pandas as pd

from bqss.constants import UTF_8
from bqss.finess.constants import PREPROCESSED_ACTIVITES_ET_PATH

logger: Final[logging.Logger] = logging.getLogger(__name__)

ACTIVITES_MAPPING_DICT: Final[dict] = {
    "Réadaptation fonct.": "ssr",
    "Chirurgie": "chirurgie",
    "Médecine": "medecine",
    "Psychiatrie": "psychiatrie",
    "Obstétrique": "obstetrique",
    "Gynécologie, obstétrique, néonatologie, réanimation néonatale": "obstetrique",
    "Soins de suite": "ssr",
    "Soins de longue durée": "sld",
    "Rééducation et réadaptation fonctionnelle": "ssr",
    "Activités interventionnelles sous imagerie médicale, par voie endovasculaire, en cardiologie": "chirurgie",
    "Traitement du cancer": "cancerologie",
    "Médecine d'urgence": "medecine",
    "AMP DPN": "amp_dpn",
    "Traitement de l'insuffisance rénale chronique par épuration extrarénale": "nephrologie",
    "Réanimation": "reanimation",
    "Transplantations d'organes et greffes de moelle osseuse": "chirurgie",
    "Chirurgie cardiaque": "chirurgie",
    "Neurochirurgie": "chirurgie",
    "Activités interventionnelles sous imagerie médicale, par voie endovasculaire, en neuroradiologie": "chirurgie",
    "Traitement des grands brûlés": "reanimation",
    "Soins de suite et de réadaptation spécialisés - Affections de l'appareil locomoteur": "ssr",
    "Soins de suite et de réadaptation spécialisés - Affections des systèmes digestif, métabolique et endocrinien": "ssr",
    "Soins de suite et de réadaptation non spécialisés": "ssr",
    "Soins de suite et de réadaptation spécialisés - Affections de la personne âgée polypathologique, dépendante ou à risque de dépendance": "ssr",
    "Greffe de rein": "chirurgie",
    "Soins de suite et de réadaptation spécialisés - Affections du système nerveux": "ssr",
    "Soins de suite et de réadaptation spécialisés - Affections liées aux conduites addictives": "ssr",
    "Soins de suite et de réadaptation spécialisés - Affections cardio-vasculaires": "ssr",
    "Soins de suite et de réadaptation spécialisés - Affections respiratoires": "ssr",
    "Greffe de cellules hématopoïétiques allogreffe": "chirurgie",
    "Soins de suite et de réadaptation spécialisés - Affections onco-hématologiques": "ssr",
    "Examen des caractéristiques génétiques d'une personne ou identification d'une personne par empreintes génétiques à des fins médicales": "diagnostic_genetique",
    "Soins de suite et de réadaptation spécialisés - Affections des brûlés": "ssr",
    "Greffe coeur-poumons": "chirurgie",
    "Greffe rein-pancréas": "chirurgie",
    "Greffe d'intestin": "chirurgie",
    "Greffe de foie": "chirurgie",
    "Greffe de pancréas": "chirurgie",
    "Greffe de coeur": "chirurgie",
    "Greffe de poumon": "chirurgie",
    "DPN marq. Sériques": "amp_dpn",
    "Trait. insuf. rénale": "nephrologie",
    "AMP spermatozoïdes": "amp_dpn",
    "AMP rec.sperme intra": "amp_dpn",
    "AMP FIV sans manip.": "amp_dpn",
    "AMP trsfert embryon": "amp_dpn",
    "AMP ponct.ovo. intra": "amp_dpn",
    "POSU cardio": "medecine",
    "DPN cytognétique": "amp_dpn",
    "SMUR": "medecine",
    "Néonatalogie": "obstetrique",
    "AMP trait.ovo. intra": "amp_dpn",
    "AMP FIV par manip.": "amp_dpn",
    "UPATOU": "medecine",
    "antenne SMUR": "medecine",
    "AMP rec. sperme dons": "amp_dpn",
    "AMP cons. gmtes dons": "amp_dpn",
    "SAU": "medecine",
    "AMP trait. ovo. dons": "amp_dpn",
    "AMP cons. Embryons": "amp_dpn",
    "DPN biochimie": "amp_dpn",
    "AMP cons.gmtes intra": "amp_dpn",
    "Act. biologiques AMP": "amp_dpn",
    "DPN génét. molécul.": "amp_dpn",
    "intensifs neonat": "obstetrique",
    "Réanimation néonat.": "reanimation",
    "DPN maladies infect.": "amp_dpn",
    "POSU pédiatrie": "medecine",
    "Transplantation": "chirurgie",
    "AMP ponct. ovo. dons": "amp_dpn",
    "SAMU": "medecine",
    "IRC avant 2002": "nephrologie",
    "Diagnostic prénatal": "obstetrique",
    "Réanimation médicale": "reanimation",
    "Rayons ionisants": "imagerie_medicale",
    "ctre périnatal proxi": "obstetrique",
    "Accueil trt urgences": "medecine",
    "Réanimation chirurg.": "reanimation",
    "Act. cliniques AMP": "amp_dpn",
    "IRC unité médicalisé": "nephrologie",
    "Réanimation polyval.": "reanimation",
    "IRC centre Enfants": "nephrologie",
    "IRC centre Adulte": "nephrologie",
    "IRC dom hemodial": "nephrologie",
    "IRC dom peritoneal": "nephrologie",
    "autodialyse simple": "nephrologie",
    "autodialyse assistée": "nephrologie",
    "dialyse saisonnière": "nephrologie",
    "POSU traumato": "medecine",
    "POSU NDA": "medecine",
    "Util. Radioéléments": "imagerie_medicale",
    "DPN hématologie": "amp_dpn",
    "DPN immunologie": "amp_dpn",
    "POSU SOS mains": "medecine",
    "Trait. grands brûlés": "reanimation",
    "Greffe coeur poumon": "chirurgie",
    "Greffe poumon": "chirurgie",
    "Greffe pancréas": "chirurgie",
    "Greffe Rein-pancréas": "chirurgie",
    "Greffe coeur": "chirurgie",
    "Greffe intestin": "chirurgie",
    "Greffe foie": "chirurgie",
}


def build_activites_et_data(
    raw_activites_et: pd.DataFrame,
) -> pd.DataFrame:
    """
    Applique le mapping convertissant les données d'autorisations de soins en
    données d'activité
    """
    processed_activites_et = raw_activites_et.drop_duplicates(
        subset=["num_finess_et", "activite", "modalite", "forme"]
    ).copy()
    # Replication des valeurs d'activité pour toutes les années entre la
    # première et la dernière date d'activité
    expanded_dfs = []
    for row in processed_activites_et.dropna().itertuples():
        dates = pd.date_range(
            str(int(row.annee_premiere_activite)),
            str(int(row.annee_derniere_activite) + 1),
            freq="Y",
        ).strftime("%Y")
        expanded = pd.DataFrame(
            {
                "annee": dates,
                "num_finess_et": row.num_finess_et,
                "libelle_activite": row.libelle_activite,
                "libelle_modalite": row.libelle_modalite,
            }
        )
        expanded_dfs.append(expanded)
    processed_activites_et = pd.concat(expanded_dfs)

    # Application du mapping des activités sur la base des autorisations
    processed_activites_et.replace(
        {
            "libelle_activite": ACTIVITES_MAPPING_DICT,
        },
        inplace=True,
    )
    processed_activites_et = pd.concat(
        [
            processed_activites_et[
                ["annee", "num_finess_et", "libelle_activite"]
            ].rename(columns={"libelle_activite": "key"}),
        ]
    )
    processed_activites_et.drop_duplicates(inplace=True)

    processed_activites_et.to_csv(
        PREPROCESSED_ACTIVITES_ET_PATH / "activites_et.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )

    return processed_activites_et


def finalize_activites_et_data(
    processed_activites_et: pd.DataFrame, year_gt_threshold=2014
) -> pd.DataFrame:
    """
    Formatage des données d'activités de soins au format clé-valeur et sélection
    des activités postérieures à 2015
    """
    final_activites_et = processed_activites_et.assign(
        finess_type="geo",
        value_boolean=True,
        value_string=np.nan,
        value_integer=np.nan,
        value_float=np.nan,
        missing_value=np.nan,
        value_date=np.nan,
    )
    final_activites_et.rename(
        columns={
            "num_finess_et": "finess",
        },
        inplace=True,
    )

    # Certaines activitées remontent au début du XX ème siècle apparement
    final_activites_et = final_activites_et[
        final_activites_et["annee"].astype(int) > year_gt_threshold
    ]
    return final_activites_et
