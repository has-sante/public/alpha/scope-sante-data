import logging
from typing import Final

from bqss import data_acquisition_utils
from bqss.finess.constants import (
    FINAL_DOMAIN_PATH,
    RESOURCES_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.finess.data_acquisition import download_data
from bqss.finess.processing import process_data
from bqss.validation_utils import is_data_valid

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main(skip_data_download: bool = False, data_validation: bool = False):
    """
    Point d'entrée du pipeline de données FINESS
    """
    # download data
    if skip_data_download:
        logger.info("Skipped data download")
    else:
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)

    process_data()

    # data validation
    if data_validation:
        logger.info("Validating data...")
        valid = is_data_valid(
            FINAL_DOMAIN_PATH / "finess.csv",
            SCHEMAS_DOMAIN_PATH / "finess.json",
        )
        logger.info("Validating data : %s", "VALID" if valid else "INVALID")
    else:
        logger.info("Skipped data validation")
