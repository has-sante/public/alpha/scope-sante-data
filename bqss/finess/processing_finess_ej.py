import logging
from typing import Final

import pandas as pd
import tqdm

from bqss.constants import ISO_8859_1
from bqss.finess.constants import (
    DATE_EXPORT_PATTERN,
    FINAL_COLUMNS_RENAMING_DICT,
    RAW_FINESS_EJ_PATH,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)

FINESS_EJ_DTYPES_DICT: Final[dict] = {
    "structureej": "string",
    "nofinessej": "string",
    "rs": "string",
    "rslongue": "string",
    "complrs": "string",
    # some street numbers aren't numeric
    "numvoie": "string",
    "typvoie": "string",
    "voie": "string",
    "compvoie": "string",
    "compldistrib": "string",
    "lieuditbp": "string",
    "commune": pd.Int64Dtype(),
    "ligneacheminement": "string",
    "departement": "string",
    "libdepartement": "string",
    "telephone": "string",
    "statutjuridique": "int64",
    "libstatutjuridique": "string",
    "categej": pd.Int64Dtype(),
    "libcategej": "string",
    "siren": "string",
    "codeape": "string",
    "datecrea": "string",
    # below are fields that are historic specific
    "region": pd.Int64Dtype(),
    "libregion": "string",
    "cog": "string",
    "codepostal": pd.Int64Dtype(),
    "libelle_routage": "string",
    "telecopie": "string",
    "liblongcategej": "string",
    "libcodeape": "string",
    "datemaj": "string",
}

LATEST_FINESS_EJ_COL_NAMES: Final[list] = [
    "structureej",
    "nofinessej",
    "rs",
    "rslongue",
    "complrs",
    "numvoie",
    "typvoie",
    "voie",
    "compvoie",
    "compldistrib",
    "lieuditbp",
    "commune",
    "ligneacheminement",
    "departement",
    "libdepartement",
    "telephone",
    "statutjuridique",
    "libstatutjuridique",
    "categej",
    "libcategej",
    "siren",
    "codeape",
    "datecrea",
]


def build_historized_finess_ej_data():
    """
    Construit et retourne un référentiel historisé des FINESS EJ
    """
    latest_data = build_latest_data()
    historized_data = add_historic_data(latest_data)
    historized_data.rename(columns=FINAL_COLUMNS_RENAMING_DICT, inplace=True)

    logger.info(
        "%d FINESS EJ ont étés historisés sur %d enregistrements",
        len(pd.unique(historized_data["num_finess_ej"])),
        len(historized_data),
    )
    return historized_data


def build_latest_data() -> pd.DataFrame:
    latest_data = pd.read_csv(
        RAW_FINESS_EJ_PATH / "latest.csv",
        header=0,
        names=LATEST_FINESS_EJ_COL_NAMES,
        dtype=FINESS_EJ_DTYPES_DICT,
        delimiter=";",
        encoding=ISO_8859_1,
    )
    latest_data.drop(columns="structureej", inplace=True)

    with open(
        RAW_FINESS_EJ_PATH / "latest.csv", encoding=ISO_8859_1
    ) as latest_file:
        header = latest_file.readline()
        date_export = header.split(";")[-1].strip()
    latest_data.insert(loc=0, column="date_export", value=date_export)

    return latest_data


def add_historic_data(latest_data) -> pd.DataFrame:
    historic_path = RAW_FINESS_EJ_PATH / "historic"
    historic_dfs = []
    for filepath in tqdm.tqdm(
        historic_path.glob("*.csv"),
        desc="Loading historic finess EJ data",
    ):
        historic_df = pd.read_csv(
            filepath,
            dtype=FINESS_EJ_DTYPES_DICT,
            delimiter=";",
            encoding=ISO_8859_1,
        )
        date_export = "-".join(
            DATE_EXPORT_PATTERN.match(filepath.stem).groups()
        )
        preprocess_historic_data(historic_df, date_export)
        historic_dfs.append(historic_df)
    return pd.concat([latest_data] + historic_dfs)


def preprocess_historic_data(historic_df, date_export):
    """
    Prétraite les données historiques FINESS avant de pouvoir les merger avec la
    dernière extraction
    """
    historic_df["date_export"] = date_export

    historic_df.drop(columns=["libcategej"], inplace=True)
    historic_df.rename(
        columns={
            "liblongcategej": "libcategej",
        },
        inplace=True,
    )
