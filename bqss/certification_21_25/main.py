import logging
from typing import Final, Tuple

import pandas as pd

from bqss import constants, data_acquisition_utils
from bqss.certification_21_25.constants import (
    DECISION_MAPPING,
    FINAL_DOMAIN_PATH,
    KEY_COLUMN_PREFIX,
    RAW_DOMAIN_PATH,
    RESOURCES_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.certification_21_25.data_download import download_data
from bqss.data_acquisition_utils import clean_directory
from bqss.validation_utils import is_data_valid

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main(skip_data_download: bool = False, data_validation: bool = False):
    """
    Point d'entrée du pipeline de données de certifications
    """
    # download data
    if skip_data_download:
        logger.info("Skipped data download")
    else:
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)

    res_cert_df, res_chap_df, res_obj_df = load_clean_data()
    kv_cert_df, kv_chap_df, kv_obj_df = format_data(
        res_cert_df, res_chap_df, res_obj_df
    )

    write_data(kv_cert_df, kv_chap_df, kv_obj_df)

    meta_df = build_metadata(res_chap_df, res_obj_df)
    meta_df.to_csv(
        FINAL_DOMAIN_PATH / "certification_metadata.csv",
        index=False,
        sep=",",
        encoding=constants.UTF_8,
    )

    # data validation
    if data_validation:
        logger.info("Validation des données")
        for filename in ["certifications", "chapitres", "objectifs"]:
            valid = is_data_valid(
                FINAL_DOMAIN_PATH / f"{filename}.csv",
                SCHEMAS_DOMAIN_PATH / f"{filename}.json",
            )
            if not valid:
                raise ValueError(f"{filename} non valide")
    else:
        logger.info("Skipped data validation")


# pylint: disable=unsubscriptable-object
def load_clean_data() -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    logger.info("Chargement des fichiers et nettoyage")
    clean_directory(FINAL_DOMAIN_PATH)
    demarche_df = pd.read_csv(
        RAW_DOMAIN_PATH / "demarches.csv", parse_dates=["date_de_decision"]
    )
    # On drop les certifications à venir
    demarche_df.dropna(subset=["Decision_de_la_CCES"], inplace=True)
    demarche_df["Decision_de_la_CCES"] = (
        demarche_df["Decision_de_la_CCES"].map(DECISION_MAPPING).astype("int")
    )
    if demarche_df.duplicated(subset="code_demarche").any():
        raise ValueError("Il y'a des doublons dans les démarches")
    et_geo_df = pd.read_csv(RAW_DOMAIN_PATH / "etablissements_geo.csv")
    # On ne charge pas les données juridiques pour le moment

    # denormalization brute du coup on a des lignes sans données => `.`
    res_chap_df = pd.read_csv(RAW_DOMAIN_PATH / "resultats_chapitres.csv")
    res_chap_df = res_chap_df.loc[res_chap_df["moy_chapitre"] != "."]
    res_chap_df["moy_chapitre"] = res_chap_df["moy_chapitre"].astype("float64")
    res_obj_df = pd.read_csv(RAW_DOMAIN_PATH / "resultats_objectifs.csv")
    res_obj_df = res_obj_df.loc[res_obj_df["moy_objectif"] != "."]
    res_obj_df["moy_objectif"] = res_obj_df["moy_objectif"].astype("float64")
    res_obj_df["id_objectif"] = res_obj_df["objectif"].str.extract(
        r"([1-9]+\.[1-9]+)"
    )
    # Les établissements non certifiés ou certifiés avec réserve n'ont pas le détail
    # de leur résultats publiés.
    res_obj_df.dropna(subset=["moy_objectif"], inplace=True)

    res_cert_df = et_geo_df.merge(demarche_df, on="code_demarche")
    res_chap_df = et_geo_df.merge(res_chap_df, on="code_demarche")
    res_chap_df = res_chap_df.merge(demarche_df, on="code_demarche")
    res_obj_df = et_geo_df.merge(res_obj_df, on="code_demarche")
    res_obj_df = res_obj_df.merge(demarche_df, on="code_demarche")

    return res_cert_df, res_chap_df, res_obj_df


def format_data(
    res_cert_df: pd.DataFrame,
    res_chap_df: pd.DataFrame,
    res_obj_df: pd.DataFrame,
) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:

    logger.info("Formattage en clef-valeur des données")

    kv_cert_df = res_cert_df[
        [
            "annee_visite",
            "FINESS_EG",
            "Decision_de_la_CCES",
            "code_demarche",
            "date_de_decision",
        ]
    ]
    kv_cert_df = kv_cert_df.rename(
        columns={
            "annee_visite": "annee",
            "FINESS_EG": "finess",
            "Decision_de_la_CCES": "decision",
            "code_demarche": "id_demarche",
            "date_de_decision": "date_decision",
        },
    )
    kv_cert_df = kv_cert_df.melt(
        id_vars=["annee", "finess"], var_name="key", value_name="value_integer"
    )
    date_decision_mask = kv_cert_df["key"] == "date_decision"
    kv_cert_df["value_date"] = (
        kv_cert_df["value_integer"]
        .mask(~date_decision_mask)
        .astype("datetime64[ns]")
    )
    kv_cert_df["value_integer"] = (
        kv_cert_df["value_integer"].mask(date_decision_mask).astype("Int64")
    )
    kv_cert_df["finess_type"] = "geo"
    kv_cert_df["key"] = KEY_COLUMN_PREFIX + "_" + kv_cert_df["key"]

    kv_chap_df = res_chap_df[
        ["annee_visite", "FINESS_EG", "id_chapitre", "moy_chapitre"]
    ]
    kv_chap_df = kv_chap_df.rename(
        columns={
            "annee_visite": "annee",
            "FINESS_EG": "finess",
            "id_chapitre": "key",
            "moy_chapitre": "value_float",
        },
    )
    kv_chap_df["finess_type"] = "geo"
    kv_chap_df["key"] = f"{KEY_COLUMN_PREFIX}_score_chapitre_" + kv_chap_df[
        "key"
    ].astype("str")

    kv_obj_df = res_obj_df[
        ["annee_visite", "FINESS_EG", "id_objectif", "moy_objectif"]
    ]
    kv_obj_df = kv_obj_df.rename(
        columns={
            "annee_visite": "annee",
            "FINESS_EG": "finess",
            "id_objectif": "key",
            "moy_objectif": "value_float",
        }
    )
    kv_obj_df["finess_type"] = "geo"
    kv_obj_df["key"] = (
        f"{KEY_COLUMN_PREFIX}_score_objectif_" + kv_obj_df["key"]
    )

    kv_cert_df = kv_cert_df[
        [
            "annee",
            "finess",
            "finess_type",
            "key",
            "value_integer",
            "value_date",
        ]
    ]
    kv_chap_df = kv_chap_df[
        ["annee", "finess", "finess_type", "key", "value_float"]
    ]
    kv_obj_df = kv_obj_df[
        ["annee", "finess", "finess_type", "key", "value_float"]
    ]

    return kv_cert_df, kv_chap_df, kv_obj_df


def write_data(
    kv_cert_df: pd.DataFrame, kv_chap_df: pd.DataFrame, kv_obj_df: pd.DataFrame
):
    logger.info("Écriture des fichiers finaux")

    kv_cert_df.to_csv(
        FINAL_DOMAIN_PATH / "certifications.csv",
        index=False,
        sep=",",
        encoding=constants.UTF_8,
    )
    kv_chap_df.to_csv(
        FINAL_DOMAIN_PATH / "chapitres.csv",
        index=False,
        sep=",",
        encoding=constants.UTF_8,
    )
    kv_obj_df.to_csv(
        FINAL_DOMAIN_PATH / "objectifs.csv",
        index=False,
        sep=",",
        encoding=constants.UTF_8,
    )

    kv_df = pd.concat([kv_cert_df, kv_chap_df, kv_obj_df])
    kv_df = kv_df[
        [
            "annee",
            "finess",
            "finess_type",
            "key",
            "value_integer",
            "value_float",
            "value_date",
        ]
    ]
    kv_df.to_csv(
        FINAL_DOMAIN_PATH / "certifications_key_value.csv",
        index=False,
        sep=",",
        encoding=constants.UTF_8,
    )


def build_metadata(
    res_chap_df: pd.DataFrame,
    res_obj_df: pd.DataFrame,
) -> pd.DataFrame:
    logger.info("Écriture du fichier de metadata")
    chap_meta_df = res_chap_df[["id_chapitre", "chapitre"]].drop_duplicates()
    chap_meta_df["type"] = "float"
    chap_meta_df[
        "name"
    ] = f"{KEY_COLUMN_PREFIX}_score_chapitre_" + chap_meta_df[
        "id_chapitre"
    ].astype(
        "str"
    )
    chap_meta_df.rename(columns={"chapitre": "title"}, inplace=True)
    chap_meta_df["description"] = ""
    chap_meta_df.drop(columns=["id_chapitre"], inplace=True)

    obj_meta_df = res_obj_df[["id_objectif", "objectif"]].drop_duplicates()
    obj_meta_df["type"] = "float"
    obj_meta_df["name"] = f"{KEY_COLUMN_PREFIX}_score_objectif_" + obj_meta_df[
        "id_objectif"
    ].astype("str")
    obj_meta_df.rename(columns={"objectif": "title"}, inplace=True)
    obj_meta_df["description"] = ""
    obj_meta_df.drop(columns=["id_objectif"], inplace=True)

    cert_meta_df = pd.DataFrame.from_records(
        [
            {
                "name": f"{KEY_COLUMN_PREFIX}_decision",
                "title": f"Décision de certification obtenu par l'établissement. ({DECISION_MAPPING})",
                "description": "",
                "type": "integer",
            },
            {
                "name": f"{KEY_COLUMN_PREFIX}_id_demarche",
                "title": "Identifiant de la démarche de certification",
                "description": "",
                "type": "integer",
            },
            {
                "name": f"{KEY_COLUMN_PREFIX}_date_decision",
                "title": "Date de la décision de certification",
                "description": "",
                "type": "date",
            },
        ]
    )
    meta_df = pd.concat([cert_meta_df, chap_meta_df, obj_meta_df])

    return meta_df[["name", "title", "description", "type"]]
