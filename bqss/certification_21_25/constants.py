from pathlib import Path
from typing import Final

from bqss.constants import DATA_PATH, RESOURCES_PATH, SCHEMAS_PATH

RESOURCES_DOMAIN_PATH: Final[Path] = RESOURCES_PATH / "certification_21_25"
SCHEMAS_DOMAIN_PATH: Final[Path] = SCHEMAS_PATH / "certification_21_25"
DATA_DOMAIN_PATH: Final[Path] = DATA_PATH / "certification_21_25"
RAW_DOMAIN_PATH: Final[Path] = DATA_DOMAIN_PATH / "raw"
FINAL_DOMAIN_PATH: Final[Path] = DATA_DOMAIN_PATH / "final"

KEY_COLUMN_PREFIX = "certification_ref_2021"

DECISION_MAPPING = {
    "Certifié avec mention": 1,
    "Certifié": 2,
    "Certifié sous conditions": 3,
    "Non certifié": 4,
}

REVERSE_DECISION_MAPPING = {v: k for k, v in DECISION_MAPPING.items()}
