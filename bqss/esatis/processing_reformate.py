import logging
from typing import Final

import numpy as np
import pandas as pd

from bqss.bqss.reformate_utils import create_float_df, create_object_df
from bqss.esatis.constants import FINAL_ESATIS_PATH

logger: Final[logging.Logger] = logging.getLogger(__name__)

NON_REPONDANT_MISSING_VALUE = "Non répondant"
NON_VALIDE_MISSING_VALUE = "Non validé"
DONNEES_INSUFFISANTES_MISSING_VALUE = "Données insuffisantes"
MISSING_VALUES_DICT: Final[dict] = {
    r"NR": NON_REPONDANT_MISSING_VALUE,
    r"NV": NON_VALIDE_MISSING_VALUE,
    r"DI": DONNEES_INSUFFISANTES_MISSING_VALUE,
}

ORDERED_COLUMNS = [
    "finess",
    "finess_type",
    "annee",
    "key",
    "value_float",
    "value_string",
    "missing_value",
]


def reformate_historized():
    """
    Merge les fichiers et sauvegarde l'historisation d'e-satis
    """

    historized_esatis = pd.read_csv(FINAL_ESATIS_PATH / "esatis.csv")

    historized_esatis[["finess_geo", "finess_ej", "finess_pmsi"]].astype(
        dtype=str
    )
    if not all(
        isinstance(dtype, (float, object))
        for dtype in historized_esatis.dtypes.unique()
    ):
        logger.warning(
            "reformate_historized_esatis() support only object and float type"
        )

    list_idx = ["finess_geo", "finess_ej", "finess_pmsi", "annee"]
    df_str = create_object_df(historized_esatis, list_idx)
    df_float = create_float_df(historized_esatis, list_idx)
    reformate_df = pd.merge(
        df_float,
        df_str,
        how="outer",
        on=["finess_geo", "finess_ej", "finess_pmsi", "annee", "key"],
        suffixes=("", "_y"),
    )
    reformate_df.drop_duplicates(inplace=True)
    if not reformate_df["finess_geo"].isnull().any():
        reformate_df["finess_type"] = "geo"
        reformate_df.rename(columns={"finess_geo": "finess"}, inplace=True)
    else:
        logger.error("Il existe des finess géographique à null")
    reformate_df.drop(
        columns=["finess_ej", "finess_pmsi"], inplace=True, errors="ignore"
    )

    reformate_df["missing_value"] = reformate_df["value_string"]
    esatis_missing_values = reformate_df["missing_value"].str.match(
        "|".join(MISSING_VALUES_DICT.keys()), na=False
    )
    reformate_df.loc[esatis_missing_values, "value_string"] = np.NaN
    reformate_df.loc[~esatis_missing_values, "missing_value"] = np.NaN
    reformate_df["missing_value"].replace(
        regex=MISSING_VALUES_DICT, inplace=True
    )
    reformate_df = reformate_df[ORDERED_COLUMNS]
    return reformate_df
