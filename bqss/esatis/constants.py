from pathlib import Path
from typing import Final

from bqss.constants import DATA_PATH, RESOURCES_PATH, SCHEMAS_PATH

RESOURCES_ESATIS_PATH: Final[Path] = RESOURCES_PATH / "esatis"
DATA_ESATIS_PATH: Final[Path] = DATA_PATH / "esatis"
SCHEMAS_ESATIS_PATH: Final[Path] = SCHEMAS_PATH / "esatis"
RAW_ESATIS_PATH: Final[Path] = DATA_ESATIS_PATH / "raw"
FINAL_ESATIS_PATH: Final[Path] = DATA_ESATIS_PATH / "final"
CLEAN_ESATIS_PATH: Final[Path] = DATA_ESATIS_PATH / "cleaned"
