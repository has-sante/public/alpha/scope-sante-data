import logging
from typing import Final

from bqss import data_acquisition_utils
from bqss.esatis.constants import (
    FINAL_ESATIS_PATH,
    RESOURCES_ESATIS_PATH,
    SCHEMAS_ESATIS_PATH,
)
from bqss.esatis.data_acquisition import download_data
from bqss.esatis.processing import process_data
from bqss.validation_utils import is_data_valid

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main(skip_data_download: bool = False, data_validation: bool = False):
    """
    Point d'entrée de tout le pipeline esatis
    """
    # acquisition des données
    if skip_data_download:
        logger.info("Skipped data download")
    else:
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_ESATIS_PATH
        )
        download_data(data_sources)

    # process des données
    process_data()

    # validation de la donnée
    if data_validation:
        logger.info("Validating key value data...")
        valid = is_data_valid(
            FINAL_ESATIS_PATH / "esatis_key_value.csv",
            SCHEMAS_ESATIS_PATH / "esatis_key_value.json",
        )
        logger.info(
            "Validating key value data : %s", "VALID" if valid else "INVALID"
        )
        logger.info("Validating data...")
        valid = is_data_valid(
            FINAL_ESATIS_PATH / "esatis.csv",
            SCHEMAS_ESATIS_PATH / "esatis.json",
        )
        logger.info("Validating data : %s", "VALID" if valid else "INVALID")
    else:
        logger.info("Skipped data validation")
