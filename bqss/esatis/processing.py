import logging
from typing import Final

from bqss.data_acquisition_utils import clean_directory
from bqss.esatis.constants import (
    CLEAN_ESATIS_PATH,
    FINAL_ESATIS_PATH,
    SCHEMAS_ESATIS_PATH,
)
from bqss.esatis.processing_clean import clean_data
from bqss.esatis.processing_merge import merged_data
from bqss.esatis.processing_reformate import reformate_historized
from bqss.metadata_utils import table_schema_to_csv

logger: Final[logging.Logger] = logging.getLogger(__name__)


def process_data():
    """
    Traite les données du domaine E-SATIS
    """

    logger.info("Processing data...")
    for directory in [CLEAN_ESATIS_PATH, FINAL_ESATIS_PATH]:
        clean_directory(directory)

    clean_data()
    logger.info("cleaned directory")

    merged_data()
    logger.info("merged directory")

    # genere un fichier metadata à partir du table schema
    table_schema_to_csv(
        SCHEMAS_ESATIS_PATH / "esatis.json",
        FINAL_ESATIS_PATH / "esatis_metadata.csv",
    )
    logger.info("created metadata for esatis")

    # reformate le merge pour avoir un fichier en clé valeur
    historized_esatis_key_value = reformate_historized()
    historized_esatis_key_value.to_csv(
        FINAL_ESATIS_PATH / "esatis_key_value.csv",
        sep=",",
        index=False,
        encoding="UTF-8",
    )
    logger.info("data processing done")
