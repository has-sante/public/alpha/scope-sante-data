import pandas as pd

from bqss.esatis.constants import CLEAN_ESATIS_PATH, FINAL_ESATIS_PATH


def merged_data():
    """
    Merge les fichiers et sauvegarde l'historisation d'e-satis
    """

    list_common_columns = [
        "finess_ej",
        "finess_geo",
        "annee",
    ]

    merged_df = pd.DataFrame()
    for year_data_path in sorted(CLEAN_ESATIS_PATH.iterdir()):
        files = [
            file_path
            for file_path in sorted(year_data_path.iterdir())
            if (year_data_path / file_path).is_file()
        ]
        df_from_each_file = [pd.read_csv(file_path) for file_path in files]

        if len(df_from_each_file) == 2:
            year_merged_df = df_from_each_file[0].merge(
                df_from_each_file[1], on=list_common_columns, how="outer"
            )
        else:
            year_merged_df = df_from_each_file[0]

        merged_df = pd.concat([year_merged_df, merged_df], ignore_index=True)

    merged_df.to_csv(
        FINAL_ESATIS_PATH / "esatis.csv",
        sep=",",
        index=False,
        encoding="UTF-8",
    )
