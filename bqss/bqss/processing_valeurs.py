import datetime

import pandas as pd

from bqss.bqss import data_reader
from bqss.bqss.constants import (
    CERTIF_14_20_THEMATIQUE_BIOLOGIE,
    CERTIF_14_20_THEMATIQUE_BLOC,
    CERTIF_14_20_THEMATIQUE_DON_ORGANE,
    CERTIF_14_20_THEMATIQUE_ENDOSCOPIE,
    CERTIF_14_20_THEMATIQUE_FIN_DE_VIE,
    CERTIF_14_20_THEMATIQUE_ID_PATIENT,
    CERTIF_14_20_THEMATIQUE_IMAG_INTERV,
    CERTIF_14_20_THEMATIQUE_IMAGERIE,
    CERTIF_14_20_THEMATIQUE_NAISSANCE,
    CERTIF_14_20_THEMATIQUE_RADIOTHERAPIE,
    CERTIF_14_20_THEMATIQUE_URGENCES,
)
from bqss.certification_14_20.constants import (
    CERTIF_V2014_DECISION_THEMATIQUE_KEY_PREFIX,
)
from bqss.constants import (
    FINESS_TYPE_GEO,
    FINESS_TYPE_JUR,
    FINESS_TYPE_UNKNOWN,
)


def build_key_value_table() -> pd.DataFrame:
    """
    Construit et retourne la table valeurs agrégeant les tables clé-valeurs des
    domaines de données suivants : SAE, IQSS, e-Satis, certification
    """
    sae_df = data_reader.get_sae()
    sae_df = sae_df.drop(columns=["raison_sociale", "finess_ej"])

    iqss_df = data_reader.get_iqss()
    iqss_df = iqss_df.drop(columns=["raison_sociale"])

    esatis_df = data_reader.get_esatis()

    certification_14_20_df = get_certification_14_20_key_value()
    certification_21_25_df = data_reader.get_certifications_21_25_kv()

    activites_et = data_reader.get_activites_et()

    valeurs_df = pd.concat(
        [
            sae_df,
            iqss_df,
            esatis_df,
            certification_14_20_df,
            certification_21_25_df,
            activites_et,
        ]
    )
    return valeurs_df


def get_certification_14_20_key_value() -> pd.DataFrame:
    """
    Retourne la version finale des données de certification en clé-valeur, i.e
    la version enrichie de la redescente des décisions de certification par
    thématiques.
    """
    certifications_df = data_reader.get_certifications_14_20()
    certifications_df = certifications_df[["finess", "demarche"]]
    thematiques_df = data_reader.get_thematiques()
    thematiques_df = thematiques_df[["demarche", "thematique", "decision"]]
    # Ce merge distribue toutes les décisions de certification par thématiques
    # sur tous les établissements géo associés à la démarche de certification
    # => il faut donc filtrer les lignes pour lesquelles cette redescente brute
    # n'a pas de sens
    certification_kv_df = pd.merge(
        left=certifications_df,
        right=thematiques_df,
        left_on="demarche",
        right_on="demarche",
        validate="many_to_many",
    )

    sae_df = data_reader.get_sae()
    sae_df = sae_df[sae_df["annee"] == max(sae_df["annee"].unique())]

    # thématique : urgences
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_urg") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_URGENCES,
        rightly_certified_finess,
    )
    # thématique : bloc opératoire
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_bloc") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_BLOC,
        rightly_certified_finess,
    )
    # thématique : radiothérapie
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_rth") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_RADIOTHERAPIE,
        rightly_certified_finess,
    )
    # thématique : imagerie interventionnelle
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_neuro_int") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_IMAG_INTERV,
        rightly_certified_finess,
    )
    # thématique : biologie
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_bio") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_BIOLOGIE,
        rightly_certified_finess,
    )
    # thématique : imagerie
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_imag") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_IMAGERIE,
        rightly_certified_finess,
    )
    # thématique : endoscopie
    rightly_certified_finess = sae_df[
        (
            sae_df["key"].isin(
                ["blocs_salend", "blocs_blocs_a14", "blocs_blocs_b14"]
            )
        )
        & (sae_df["value_integer"] > 0)
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_ENDOSCOPIE,
        rightly_certified_finess,
    )
    # thématique : salle de naissance
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "perinat_jouacc") & (sae_df["value_integer"] > 0)
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_NAISSANCE,
        rightly_certified_finess,
    )
    # thématique : fin de vie
    rightly_certified_finess = sae_df[
        (
            sae_df["key"].isin(
                [
                    # MCO
                    "filtre_heb_med",
                    "filtre_heb_chir",
                    "filtre_heb_perinat",
                    "filtre_med",
                    "filtre_chirambu",
                    "filtre_ivgamp",
                    # SSR
                    "filtre_heb_ssr",
                    "filtre_ssr",
                    # SLD
                    "filtre_heb_sld",
                    # HAD
                    "filtre_had",
                ]
            )
        )
        & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_FIN_DE_VIE,
        rightly_certified_finess,
    )
    # thématique : identification du patient
    rightly_certified_finess = sae_df[
        (
            sae_df["key"].isin(
                [
                    # MCO
                    "filtre_heb_med",
                    "filtre_heb_chir",
                    "filtre_heb_perinat",
                    "filtre_med",
                    "filtre_chirambu",
                    "filtre_ivgamp",
                    # SSR
                    "filtre_heb_ssr",
                    "filtre_ssr",
                    # PSY
                    "filtre_heb_psy",
                    "filtre_psy",
                    # SLD
                    "filtre_heb_sld",
                ]
            )
        )
        & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_ID_PATIENT,
        rightly_certified_finess,
    )
    # thématique : don d'organes et de tissus
    rightly_certified_finess = sae_df[
        (
            sae_df["key"].isin(
                [
                    # MCO
                    "filtre_heb_med",
                    "filtre_heb_chir",
                    "filtre_heb_perinat",
                    "filtre_med",
                    "filtre_chirambu",
                    "filtre_ivgamp",
                ]
            )
        )
        & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_DON_ORGANE,
        rightly_certified_finess,
    )

    # we enforce a year in our key-value file but this information is not
    # meaningful, please refer to the date of certification (key certif_date)
    # see https://gitlab.com/has-sante/public/bqss/-/issues/32
    certification_kv_df["annee"] = datetime.datetime.now().year
    certification_kv_df["finess_type"] = FINESS_TYPE_GEO
    certification_kv_df[
        "key"
    ] = CERTIF_V2014_DECISION_THEMATIQUE_KEY_PREFIX + certification_kv_df[
        "thematique"
    ].astype(
        str
    )
    certification_kv_df.rename(
        columns={"decision": "value_string"}, inplace=True
    )
    certification_kv_df.drop(columns=["demarche", "thematique"], inplace=True)

    niveaux_certif_df = data_reader.get_certifications_14_20_kv()
    certification_kv_df = pd.concat([niveaux_certif_df, certification_kv_df])
    return certification_kv_df


def filter_wrongly_certified_finess(
    certification_kv_df: pd.DataFrame,
    thematiques: list,
    rightly_certified_finess: pd.DataFrame,
) -> pd.DataFrame:
    """
    Cette fonction filtre les décisions de certification par thématiques pour
    les finess qui se sont incorrectement vu attribuer une décision pour une
    thématique qui ne les concerne pas
    """
    all_certified_finess = certification_kv_df[
        (certification_kv_df["thematique"].isin(thematiques))
    ]["finess"]
    wrongly_certified_finess = all_certified_finess[
        ~all_certified_finess.isin(rightly_certified_finess)
    ]
    certification_kv_df = certification_kv_df.drop(
        certification_kv_df[
            (certification_kv_df["thematique"].isin(thematiques))
            & (certification_kv_df["finess"].isin(wrongly_certified_finess))
        ].index
    )
    return certification_kv_df


def set_finess_type(
    valeurs_df: pd.DataFrame,
    finess_df: pd.DataFrame,
) -> pd.DataFrame:
    """
    Spécifie le type de finess des lignes clé-valeur en accord avec le
    référentiel finess passé en paramètre. Tous les finess non référencés
    sont considérés comme inconnus dans les données en clé-valeur.
    """
    valeurs_df.loc[
        valeurs_df["finess"].isin(finess_df["num_finess_ej"]), "finess_type"
    ] = FINESS_TYPE_JUR
    valeurs_df.loc[
        valeurs_df["finess"].isin(finess_df["num_finess_et"]), "finess_type"
    ] = FINESS_TYPE_GEO
    valeurs_df.loc[
        ~valeurs_df["finess"].isin(finess_df["num_finess_et"])
        & ~valeurs_df["finess"].isin(finess_df["num_finess_ej"]),
        "finess_type",
    ] = FINESS_TYPE_UNKNOWN
    return valeurs_df
