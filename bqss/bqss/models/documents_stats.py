import typing
from enum import Enum

# pylint: disable=no-name-in-module
from pydantic import BaseModel


class ClasseEnum(str, Enum):
    """
    Valeurs possibles pour la classe d'un indicateur
    """

    A = "A"
    B = "B"
    C = "C"
    D = "D"


class BinaryClasseEnum(int, Enum):
    """
    Valeurs possibles pour les classes binaires
    """

    ZERO = 0
    ONE = 1


class MissingClasseEnum(str, Enum):
    """
    Valeurs manquantes possibles
    """

    NC = "Non concerné"
    NR = "Non répondant"
    NV = "Non validé"
    DI = "Données insuffisantes"


# pylint: disable=too-few-public-methods
class DistributionModel(BaseModel):
    """
    Distribution des valeurs recueillies
    """

    classe: typing.Union[ClasseEnum, BinaryClasseEnum]
    nb_etablissements: int
    part_etablissements: float


class MissingDistributionModel(BaseModel):
    """
    Distribution des valeurs manquantes
    """

    classe: MissingClasseEnum


class IndicateurYearStatsModel(BaseModel):
    """
    Statistiques d'un indicateur donné pour une année de recueil donnée
    """

    annee_recueil: int
    distribution: typing.List[DistributionModel]
    missing_distribution: typing.List[MissingDistributionModel]


class IndicateurStatsModel(BaseModel):
    """
    Statistiques nationales d'un indicateur donné
    """

    indicateur: str
    statistiques_annuelles: typing.List[IndicateurYearStatsModel]


class StatsModel(BaseModel):
    """
    Statistiques sur les établissements de santé
    """

    distributions_nationales_indicateurs: typing.List[IndicateurStatsModel]
