import pandas as pd

from bqss.bqss.constants import NOMENCLATURES_DTYPES_DICT
from bqss.certification_14_20 import constants as certification_14_20_constants
from bqss.certification_21_25 import constants as certification_21_25_constants
from bqss.constants import UTF_8
from bqss.esatis import constants as esatis_constants
from bqss.finess import constants as finess_constants
from bqss.iqss import constants as iqss_constants
from bqss.sae import constants as sae_constants


def get_sae_metadata() -> pd.DataFrame:
    sae_metadata = pd.read_csv(
        sae_constants.FINAL_DOMAIN_PATH / "sae_metadata.csv",
        sep=",",
        encoding=UTF_8,
    )
    sae_metadata["source"] = "SAE"
    return sae_metadata


def get_certification_14_20_metadata() -> pd.DataFrame:
    certification_metadata = pd.read_csv(
        certification_14_20_constants.FINAL_DOMAIN_PATH
        / "certification_metadata.csv",
        sep=",",
        encoding=UTF_8,
    )
    certification_metadata["source"] = "Certification v2014"
    return certification_metadata


def get_certification_21_25_metadata() -> pd.DataFrame:
    certification_metadata = pd.read_csv(
        certification_21_25_constants.FINAL_DOMAIN_PATH
        / "certification_metadata.csv",
        sep=",",
        encoding=UTF_8,
    )
    certification_metadata["source"] = "Certification v2021"
    return certification_metadata


def get_certification_nomenclatures() -> pd.DataFrame:
    nomenclatures = pd.read_csv(
        certification_14_20_constants.RESOURCES_DOMAIN_PATH
        / "nomenclatures.csv",
        sep=",",
        encoding=UTF_8,
        dtype=NOMENCLATURES_DTYPES_DICT,
    )
    nomenclatures["source"] = "Certification v2014"
    return nomenclatures


def get_iqss_metadata() -> pd.DataFrame:
    iqss_metadata = pd.read_csv(
        iqss_constants.RESOURCES_IQSS_PATH / "metadata.csv",
        sep=",",
        encoding=UTF_8,
        dtype={
            "version": "Int64",
            "annee": "Int64",
        },
    )
    iqss_metadata["code"] = iqss_metadata["code"].str.lower()
    iqss_metadata.rename(
        columns={
            "code": "name",
            "libelle": "title",
            "commentaire": "description",
        },
        inplace=True,
    )
    return iqss_metadata


def get_iqss_nomenclatures() -> pd.DataFrame:
    nomenclatures = pd.read_csv(
        iqss_constants.RESOURCES_IQSS_PATH / "nomenclatures.csv",
        sep=",",
        encoding=UTF_8,
        dtype=NOMENCLATURES_DTYPES_DICT,
    )
    return nomenclatures


def get_esatis_metadata() -> pd.DataFrame:
    metadata = pd.read_csv(
        esatis_constants.FINAL_ESATIS_PATH / "esatis_metadata.csv",
        sep=",",
        encoding=UTF_8,
    )
    metadata["source"] = "IQSS questionnaire patient"
    return metadata


def get_esatis_nomenclatures() -> pd.DataFrame:
    nomenclatures = pd.read_csv(
        esatis_constants.RESOURCES_ESATIS_PATH / "nomenclatures.csv",
        sep=",",
        encoding=UTF_8,
        dtype=NOMENCLATURES_DTYPES_DICT,
    )
    nomenclatures["source"] = "IQSS questionnaire patient"
    return nomenclatures


def get_activites_et_metadata() -> pd.DataFrame:
    metadata = pd.read_csv(
        finess_constants.FINAL_DOMAIN_PATH / "activites_et_metadata.csv",
        sep=",",
        encoding=UTF_8,
    )
    metadata["source"] = "Activités ET"
    return metadata


def get_all_metadata() -> pd.DataFrame:
    metadata_df = pd.concat(
        [
            get_certification_14_20_metadata(),
            get_certification_21_25_metadata(),
            get_iqss_metadata(),
            get_esatis_metadata(),
            get_sae_metadata(),
            get_activites_et_metadata(),
        ]
    )

    # cleaning
    metadata_df.drop_duplicates(inplace=True)
    return metadata_df
