import logging
import re
import typing
from typing import Final, Optional

import numpy as np
import pandas as pd
import tqdm

from bqss.certification_21_25.constants import REVERSE_DECISION_MAPPING

logger: Final[logging.Logger] = logging.getLogger(__name__)


CERTIF_2021_TYPE_PAT = re.compile(r"score_(.*)_.*|_(decision)")


def _get_finess_variable_value(finess_iqss_d):
    if pd.notna(finess_iqss_d["value_boolean"]):
        return finess_iqss_d["value_boolean"]

    if pd.notna(finess_iqss_d["value_date"]):
        return finess_iqss_d["value_date"]

    if pd.notna(finess_iqss_d["value_float"]):
        return finess_iqss_d["value_float"]

    if pd.notna(finess_iqss_d["value_integer"]):
        return finess_iqss_d["value_integer"]

    if pd.notna(finess_iqss_d["value_string"]):
        return finess_iqss_d["value_string"]

    raise ValueError(f"No valid value found for {finess_iqss_d}")


def build_finess_metadata_d(finess_df: pd.DataFrame) -> dict:

    identification_cols = [
        "num_finess_ej",
        "raison_sociale_et",
        "raison_sociale_longue_et",
        "raison_sociale_ej",
        "raison_sociale_longue_ej",
        "date_ouverture",
        "statut_juridique",
        "statut_juridique_ej",
        "libelle_statut_juridique_ej",
        "type_etablissement",
    ]
    coordonnees_cols = [
        "adresse_postale_ligne_1",
        "adresse_postale_ligne_2",
        "libelle_commune",
        "code_postal",
        "departement",
        "libelle_departement",
        "telephone",
        "latitude",
        "longitude",
    ]
    finess_df = finess_df[
        ["ferme_cette_annee", "num_finess_et"]
        + identification_cols
        + coordonnees_cols
    ]
    finesss_data_l = finess_df.replace({np.nan: None}).to_dict(
        orient="records"
    )
    result_finess_d = {}
    for finess_meta_d in tqdm.tqdm(
        finesss_data_l,
        desc="Formattage des metadonnées par finess",
    ):
        num_finess = finess_meta_d["num_finess_et"]
        result_finess_d[num_finess] = {
            "finess_geo": num_finess,
            # Certains établissements ne sont pas dans les parties de la SAE que
            # l'on prend, on les mets à petit par défaut
            "taille": "petit",
            "ferme_cette_annee": finess_meta_d["ferme_cette_annee"],
            "identification": {
                c: finess_meta_d[c] for c in identification_cols
            },
            # Les valeurs nan ne sont pas compatible avec le format JSON
            # Certaines variables sont parfois des nan (latitude, longitude)
            "coordonnees": {c: finess_meta_d[c] for c in coordonnees_cols},
            "autorisations": [],
            "activites": [],
            "IQSS": [],
            "satisfaction_patient": [],
            "capacite": [],
            "volumetrie": [],
            "certification_v2014": [],
        }
    return result_finess_d


# pylint: disable=too-many-locals
def build_finess_iqss_d(
    iqss_df: pd.DataFrame,
    indicateur_meta_d: typing.Dict,
    iqss_name_key: str,
    finess_d: Optional[typing.Dict] = None,
) -> dict:
    result_finess_iqss_d = finess_d or {}
    # Warning, sort order matters
    finesss_data_l: typing.List[typing.Dict] = iqss_df.sort_values(
        [
            "finess",
            "indicateur",
            "annee",
        ]
    ).to_dict(orient="records")

    prev_num_finess = None
    prev_indicateur = None
    prev_year = None

    for finess_iqss_d in tqdm.tqdm(
        finesss_data_l,
        desc=f"Formattage des données {iqss_name_key} par finess",
    ):
        num_finess = finess_iqss_d["finess"]
        year = finess_iqss_d["annee"]
        indicateur = finess_iqss_d["indicateur"]
        finess_data_d = result_finess_iqss_d.setdefault(num_finess, {})
        finess_data_l = finess_data_d.setdefault(iqss_name_key, [])
        # On se base sur le fait que la donnée est triée
        if prev_indicateur != indicateur or prev_num_finess != num_finess:
            indicateur_d = {
                "indicateur": indicateur,
                "valeurs": [],
                "secteur": indicateur_meta_d[indicateur]["secteur"],
                "theme": indicateur_meta_d[indicateur]["theme"],
                "source": finess_iqss_d["source"],
            }
            prev_year = None
            finess_data_l.append(indicateur_d)

        if prev_year != year:
            valeurs_d = {"annee_recueil": year}
            indicateur_d["valeurs"].append(valeurs_d)

        iqss_data = {"title": finess_iqss_d["title"]}
        missing_value = finess_iqss_d["missing_value"]
        if pd.notna(missing_value):
            iqss_data["missing_value"] = missing_value
        else:
            iqss_data["value"] = _get_finess_variable_value(finess_iqss_d)

        valeurs_d[finess_iqss_d["type_metier"]] = iqss_data

        prev_num_finess = num_finess
        prev_indicateur = indicateur
        prev_year = year
    return result_finess_iqss_d


# pylint: disable=too-many-locals
def build_finess_sae_d(
    valeurs_df: pd.DataFrame,
    metadata_df: pd.DataFrame,
    type_metier: str,
    sae_name_key: str,
    finess_d: Optional[dict] = None,
) -> dict:
    sae_meta_df = metadata_df.loc[metadata_df["type_metier"] == type_metier]

    sae_meta_d = sae_meta_df.set_index("name").to_dict(orient="index")

    sae_df = valeurs_df.loc[valeurs_df["key"].isin(sae_meta_df["name"])].copy()

    finesss_data_l = sae_df.sort_values(
        [
            "finess",
            "annee",
            "key",
        ]
    ).to_dict(orient="records")

    result_finess_sae_d = finess_d or {}

    for finess_sae_d in tqdm.tqdm(
        finesss_data_l,
        desc=f"Formattage des données de {type_metier} par finess",
    ):
        key = finess_sae_d["key"]
        num_finess = finess_sae_d["finess"]
        year = finess_sae_d["annee"]
        finess_data_d = result_finess_sae_d.setdefault(num_finess, {})
        finess_data_l = finess_data_d.setdefault(sae_name_key, [])
        finess_data_l.append(
            {
                "annee": year,
                "key": key,
                "title": sae_meta_d[key]["title"],
                "description": sae_meta_d[key]["description"],
                "value": _get_finess_variable_value(finess_sae_d),
            }
        )
    return result_finess_sae_d


def build_finess_certif_2014_d(
    valeurs_df: pd.DataFrame,
    metadata_df: pd.DataFrame,
    finess_d: Optional[dict] = None,
) -> dict:
    certif_df = valeurs_df.loc[valeurs_df["source"] == "Certification v2014"]

    certif_meta_df = metadata_df.loc[
        metadata_df["source"] == "Certification v2014"
    ]

    certif_meta_d = certif_meta_df.set_index("name").to_dict(orient="index")

    finesss_data_l = certif_df.sort_values(["finess", "key"]).to_dict(
        orient="records"
    )
    result_finess_certif_d = finess_d or {}

    for finess_certif_d in tqdm.tqdm(
        finesss_data_l,
        desc="Formattage des données de certification ref 2014-2020 par finess",
    ):
        key = finess_certif_d["key"]
        num_finess = finess_certif_d["finess"]
        finess_data_d = result_finess_certif_d.setdefault(num_finess, {})
        finess_data_l = finess_data_d.setdefault("certification_v2014", [])

        if key == "certif_V2014_id_niveau":
            thematique = "Niveau global"
        elif key == "certif_date":
            thematique = "Date de certification"
        else:
            # Construit ainsi dans bqss_data/certification/processing.py:265
            thematique = (
                certif_meta_d[key]["description"].split(":", 1)[-1].strip()
            )

        finess_data_l.append(
            {
                "key": key,
                "title": certif_meta_d[key]["title"],
                "value": _get_finess_variable_value(finess_certif_d),
                "libelle_thematique": thematique,
            }
        )
    return result_finess_certif_d


def build_finess_certif_2021_d(
    valeurs_df: pd.DataFrame,
    metadata_df: pd.DataFrame,
    finess_d: Optional[dict] = None,
) -> dict:
    certif_df = valeurs_df.loc[valeurs_df["source"] == "Certification v2021"]

    certif_meta_df = metadata_df.loc[
        metadata_df["source"] == "Certification v2021"
    ]

    certif_meta_d = certif_meta_df.set_index("name").to_dict(orient="index")

    finesss_data_l = certif_df.sort_values(
        [
            "finess",
            "key",
        ]
    ).to_dict(orient="records")
    result_finess_certif_d = finess_d or {}

    for finess_certif_d in tqdm.tqdm(
        finesss_data_l,
        desc="Formattage des données de certification ref 2021-2025 par finess",
    ):
        key = finess_certif_d["key"]
        num_finess = finess_certif_d["finess"]
        finess_data_d = result_finess_certif_d.setdefault(num_finess, {})
        finess_data_l = finess_data_d.setdefault("certification_v2021", [])
        title = certif_meta_d[key]["title"].split(":", 1)[-1].strip()
        value = _get_finess_variable_value(finess_certif_d)
        if key == "certification_ref_2021_decision":
            title = REVERSE_DECISION_MAPPING[value]
        certif_d = {
            "key": key,
            "title": title,
            "value": value,
        }

        score_type = None
        chapitre_id = None
        objectif_id = None
        found_type = CERTIF_2021_TYPE_PAT.search(key)
        if found_type:
            groups = found_type.groups()
            score_type = groups[0] or groups[1]

            if score_type == "objectif":
                chapitre_id = key.rsplit("_", 1)[-1]
                chapitre_id, objectif_id = chapitre_id.split(".")
            elif score_type == "chapitre":
                chapitre_id = key.rsplit("_", 1)[-1]
            elif key == "certification_ref_2021_decision":
                score_type = "decision"
            elif key == "certification_ref_2021_date_decision":
                score_type = "date_decision"

            certif_d["score_type"] = score_type
            certif_d["chapitre_id"] = chapitre_id
            certif_d["objectif_id"] = objectif_id

        finess_data_l.append(certif_d)
    return result_finess_certif_d


def build_finess_autorisation_d(
    autorisations_df: pd.DataFrame, finess_d: Optional[dict]
) -> dict:
    finesss_data_l = (
        autorisations_df[
            [
                "num_finess_et",
                "activite",
                "libelle_activite",
                "date_mise_en_oeuvre",
            ]
        ]
        .sort_values(["num_finess_et", "activite", "date_mise_en_oeuvre"])
        # activite est une clef de premier niveau,
        # il y'a plusieurs modalités/forme (lignes) par activité
        .drop_duplicates(["num_finess_et", "activite"], keep="first")
        .to_dict(orient="records")
    )
    result_finess_autorisation_d = finess_d or {}
    for finess_autorisation_d in tqdm.tqdm(
        finesss_data_l,
        desc="Formattage des données d'autorisation par finess",
    ):
        num_finess = finess_autorisation_d["num_finess_et"]
        finess_data_d = result_finess_autorisation_d.setdefault(num_finess, {})
        finess_data_l = finess_data_d.setdefault("autorisations", [])
        finess_data_l.append(
            {
                "activite": finess_autorisation_d["activite"],
                "libelle_activite": finess_autorisation_d["libelle_activite"],
                # 1. Certaines activité n'ont pas de dates de mise en oeuvre
                #  mais elles correspondent quand même à une donnée intéressante
                # 2. Les dates de mise en oeuvre concerne en fait les modalités
                #  et non pas les activités, on prend la première date de mise en oeuvre
                #  d'un modalité d'une activité à défaut.
                "date_mise_en_oeuvre": finess_autorisation_d[
                    "date_mise_en_oeuvre"
                ],
            }
        )
    return result_finess_autorisation_d


def build_finess_activite_d(
    valeurs_df: pd.DataFrame,
    metadata_df: pd.DataFrame,
    source: str,
    activite_name_key: str,
    finess_d: Optional[dict] = None,
) -> dict:
    activite_meta_df = metadata_df.loc[metadata_df["source"] == source]

    activite_meta_d = activite_meta_df.set_index("name").to_dict(
        orient="index"
    )

    activite_df = valeurs_df.loc[
        valeurs_df["key"].isin(activite_meta_df["name"])
    ].copy()

    finesss_data_l = activite_df.sort_values(
        [
            "finess",
            "annee",
            "key",
        ]
    ).to_dict(orient="records")

    result_finess_activite_d = finess_d or {}
    prev_finess = None
    finess_data_d: typing.Dict = {}
    finess_data_l: typing.List = []

    for finess_activite_d in tqdm.tqdm(
        finesss_data_l,
        desc="Formattage des données d'activités par finess",
    ):
        key = finess_activite_d["key"]
        num_finess = finess_activite_d["finess"]
        year = finess_activite_d["annee"]
        if num_finess != prev_finess:
            prev_year = None
            if finess_data_d:
                # save most recent year activity
                finess_data_d[activite_name_key] = finess_data_l
            finess_data_d = result_finess_activite_d.setdefault(num_finess, {})

        if year != prev_year:
            finess_data_l = []

        finess_data_l.append(
            {
                "id": key,
                "libelle": activite_meta_d[key]["title"],
            }
        )
        prev_finess = num_finess
        prev_year = year
    return result_finess_activite_d


def build_finess_taille_d(
    valeurs_df: pd.DataFrame, finess_d: Optional[dict]
) -> dict:

    # cette formule vient de l'ATIH, il reste des zones d'ombres
    # https://gitlab.com/has-sante/public/bqss/-/issues/131
    volumetrie_keys = [
        "mco_sej0_mco",
        "mco_sejhc_mco",
        "mco_jou_mco",
        "had_jou_had",
        "ssr_jouhc",
        "ssr_jouhp",
        # Aucune donnée pour ces finess
        # "psy_jou_acp_inf",
        "psy_jou_acp_gen",
        "psy_jou_htp_gen",
        "psy_ven_hdjn_gen",
        "psy_ven_hdjn_inf",
        "usld_jou",
        "dialyse_sean_112ba",
        "dialyse_sean_112be",
        "dialyse_sean_1130x",
        "dialyse_sean_1140x",
        "dialyse_sean_1150a",
        "dialyse_sean_1150e",
        "cancero_a10",
        "cancero_a15",
        "cancero_a16",
    ]
    taille_df = valeurs_df.loc[valeurs_df["key"].isin(volumetrie_keys)].copy()

    taille_df = (
        taille_df.set_index(["finess", "annee", "key"])["value_integer"]
        .unstack()
        .reset_index()
        .drop_duplicates(subset=["finess"], keep="last")
    ).fillna(0)

    mco_sej = taille_df[
        [
            "mco_sej0_mco",
            "mco_sejhc_mco",
            "mco_jou_mco",
        ]
    ].sum(axis=1)
    had_sej = taille_df["had_jou_had"]
    ssr_sej = taille_df[
        [
            "ssr_jouhc",
            "ssr_jouhp",
        ]
    ].sum(axis=1)
    psy_sej = taille_df[
        [
            # "psy_jou_acp_inf",
            "psy_jou_acp_gen",
            "psy_jou_htp_gen",
            "psy_ven_hdjn_gen",
            "psy_ven_hdjn_inf",
        ]
    ].sum(axis=1)
    usld_sej = taille_df["usld_jou"]
    dialyse_sej = (
        taille_df[
            [
                "dialyse_sean_112ba",
                "dialyse_sean_112be",
                "dialyse_sean_1130x",
                "dialyse_sean_1140x",
                "dialyse_sean_1150a",
                # N'est pas présent dans les établissements autorisés
                # "dialyse_sean_1150e",
            ]
        ]
    ).sum(axis=1) / 2
    cancero_sej = pd.concat(
        [
            taille_df["cancero_a15"] / 50,
            taille_df["cancero_a10"] / 2,
            taille_df["cancero_a16"],
        ],
        axis=1,
    ).sum(axis=1)
    taille_df["volume_activite"] = pd.concat(
        [
            mco_sej,
            had_sej,
            ssr_sej,
            psy_sej,
            usld_sej,
            dialyse_sej,
            cancero_sej,
        ],
        axis=1,
    ).mean(axis=1)

    taille_df["rank"] = taille_df["volume_activite"].rank(pct=True)
    taille_df["taille"] = "moyen"
    taille_df.loc[taille_df["rank"] >= 0.9, "taille"] = "grand"
    taille_df.loc[taille_df["rank"] <= 0.1, "taille"] = "petit"

    finesss_data_l = taille_df[["finess", "taille"]].to_dict(orient="records")

    result_finess_taille_d = finess_d or {}
    for finess_taille_d in tqdm.tqdm(
        finesss_data_l,
        desc="Formattage des données de taille par finess",
    ):
        num_finess = finess_taille_d["finess"]
        finess_data_d = result_finess_taille_d.setdefault(num_finess, {})
        finess_data_d["taille"] = finess_taille_d["taille"]
    return result_finess_taille_d


def build_national_stats(finess_d) -> dict:
    # # Structure de donnée finale
    # {
    #     "distributions_nationales_indicateurs": [
    #         {
    #             "indicateur": "mco_eteortho_eteortho_2018",
    #             # triée par année croissante
    #             "statistiques_annuelles": [
    #                 {
    #                     "annee_recueil": 2018,
    #                     # triée du moins bien à la meilleure classe
    #                     "distribution": [
    #                         {
    #                             "classe": "1..5|A..E",
    #                             "nb_etablissements": 1250,
    #                             "part_etablissements": 0.2
    #                         },
    #                         #...
    #                     ]
    #                 }
    #             ],
    #         },
    #         #...
    #     ]
    # }
    # # Structure de donnée intermédiaire
    # {
    #     "<indicateur_id>": {
    #         "stats": {
    #             "2018": {
    #                 "classes": {"A": 400, "B": 163, "C": 56},
    #                 "es_count": 619,
    #                 "missing_classes": {"Données insuffisantes": 2},
    #                 "missing_es_count": 2,
    #             }
    #         }
    #     },
    # }

    # En cas de présence de multiple clefs, on prend la première présente
    # dans cet ordre
    stats_keys = ["classe", "positionnement", "status"]
    no_classe_indicateurs = set()
    indicateur_keys = ["IQSS", "satisfaction_patient"]
    indicateurs_stats_d: dict = {}
    for finess_data in tqdm.tqdm(
        finess_d.values(), desc="Calcul des statistiques nationales"
    ):
        for main_field in indicateur_keys:
            for indicateur_d in finess_data[main_field]:
                indicateur_id = indicateur_d["indicateur"]
                for year_d in indicateur_d["valeurs"]:
                    annee_recueil = year_d["annee_recueil"]
                    if not set(stats_keys).intersection(year_d.keys()):
                        no_classe_indicateurs.add(indicateur_id)
                        continue

                    indicateur_stats_d = indicateurs_stats_d.setdefault(
                        indicateur_id, {"stats": {}}
                    )
                    classe_d = [
                        year_d[key] for key in stats_keys if key in year_d
                    ][0]

                    classe_value = classe_d.get("value")
                    count_field = "es_count"
                    classe_field = "classes"
                    # les valeurs manquantes n'ont que le champ "missing_value"
                    if classe_value is None:
                        classe_value = classe_d.get("missing_value")
                        count_field = "missing_es_count"
                        classe_field = "missing_classes"

                    year_stats = indicateur_stats_d["stats"].setdefault(
                        annee_recueil,
                        {
                            "missing_es_count": 0,
                            "es_count": 0,
                            "classes": {},
                            "missing_classes": {},
                        },
                    )
                    year_stats[count_field] += 1
                    year_stats[classe_field].setdefault(classe_value, 0)
                    year_stats[classe_field][classe_value] += 1

    final_stats_l = []
    for indicateur_id, indicateur_stats_d in indicateurs_stats_d.items():
        indicateur_d = {
            "indicateur": indicateur_id,
            "statistiques_annuelles": [],
        }

        for year, year_stats_d in sorted(indicateur_stats_d["stats"].items()):
            year_indicateur_d = {
                "annee_recueil": year,
                "distribution": [],
                "missing_distribution": [],
            }

            for distrib_field, count_field, classe_field in [
                ("distribution", "es_count", "classes"),
                (
                    "missing_distribution",
                    "missing_es_count",
                    "missing_classes",
                ),
            ]:

                distribution = year_indicateur_d[distrib_field]
                es_count = year_stats_d[count_field]
                if es_count == 0:
                    continue
                for classe, classe_count in sorted(
                    year_stats_d[classe_field].items()
                ):
                    distribution.append(
                        {
                            "classe": classe,
                            "nb_etablissements": classe_count,
                            "part_etablissements": classe_count / es_count,
                        }
                    )
            indicateur_d["statistiques_annuelles"].append(year_indicateur_d)
        final_stats_l.append(indicateur_d)

    no_classe_indicateurs = no_classe_indicateurs.difference(
        indicateurs_stats_d.keys()
    )
    if no_classe_indicateurs:
        logger.warning(
            "Les indicateurs suivants n'ont pas de classe: %s",
            no_classe_indicateurs,
        )

    return {"distributions_nationales_indicateurs": final_stats_l}


def build_document_database(
    finess_df: pd.DataFrame,
    valeurs_df: pd.DataFrame,
    metadata_df: pd.DataFrame,
    autorisations_df: pd.DataFrame,
):
    ###########################
    # Préparation des données #
    ###########################

    indicateur_meta_d = (
        metadata_df[["indicateur", "title", "theme", "secteur", "source"]]
        # les informations dont on a besoin sont à la finesse de l'indicateur
        .drop_duplicates(subset=["indicateur"])
        .set_index("indicateur")
        .to_dict(orient="index")
    )

    variable_meta_d = (
        metadata_df[
            ["indicateur", "title", "theme", "secteur", "source", "name"]
        ]
        # les informations dont on a besoin sont à la finesse de l'indicateur
        .drop_duplicates(subset=["name"])
        .set_index("name")
        .to_dict(orient="index")
    )

    # exclusion des finess juridiques, les valeurs sont redescendus jur => geo
    valeurs_df = valeurs_df.loc[valeurs_df["finess_type"] == "geo"].copy()
    valeurs_df.drop(columns=["finess_type"], inplace=True)

    # on ne garde que les valeurs qui concerne des finess actifs sur bqss
    # et que les finess pour lesquels ont a des valeurs
    latest_finess_df = finess_df.sort_values(
        ["num_finess_et", "date_export"]
    ).drop_duplicates(subset=["num_finess_et"], keep="last")
    latest_finess_df = latest_finess_df.loc[
        latest_finess_df["actif_qualiscope"]
    ]

    valeurs_df = valeurs_df.loc[
        valeurs_df["finess"].isin(latest_finess_df["num_finess_et"])
    ]

    latest_finess_df = latest_finess_df.loc[
        latest_finess_df["num_finess_et"].isin(valeurs_df["finess"])
    ]

    valeurs_df = valeurs_df.merge(
        metadata_df[["name", "source", "type_metier", "title", "theme"]],
        left_on="key",
        right_on="name",
        how="left",
    ).drop(columns=["name"])

    # exclusion des vieux IQSS
    valeurs_df = valeurs_df.loc[
        ~(
            (valeurs_df["annee"] < 2017)
            & (valeurs_df["source"].str.contains("IQSS"))
        )
    ]

    # on ne garde que les vieilles données de certif
    valeurs_df = valeurs_df.sort_values("annee")
    valeurs_df = valeurs_df.loc[
        ~(
            valeurs_df["source"].isin(["Certification v2014", "SAE"])
            & valeurs_df.duplicated(subset=["key", "finess"], keep="last")
        )
    ]

    valeurs_df["indicateur"] = valeurs_df["key"].map(
        {k: v["indicateur"] for k, v in variable_meta_d.items()}
    )

    ###########################
    # Construction de la base #
    ###########################
    finess_d = build_finess_metadata_d(latest_finess_df)

    iqss_df = valeurs_df.loc[
        (valeurs_df["source"].str.contains("IQSS"))
        & (valeurs_df["source"] != "IQSS questionnaire patient")
    ].copy()
    finess_d = build_finess_iqss_d(
        iqss_df, indicateur_meta_d, iqss_name_key="IQSS", finess_d=finess_d
    )

    satisfation_iqss_df = valeurs_df.loc[
        valeurs_df["source"] == "IQSS questionnaire patient"
    ].copy()
    finess_d = build_finess_iqss_d(
        satisfation_iqss_df,
        indicateur_meta_d,
        iqss_name_key="satisfaction_patient",
        finess_d=finess_d,
    )

    finess_d = build_finess_taille_d(valeurs_df, finess_d)

    finess_d = build_finess_sae_d(
        valeurs_df,
        metadata_df,
        type_metier="capacite",
        sae_name_key="capacite",
        finess_d=finess_d,
    )

    finess_d = build_finess_sae_d(
        valeurs_df,
        metadata_df,
        type_metier="volumetrie",
        sae_name_key="volumetrie",
        finess_d=finess_d,
    )

    finess_d = build_finess_certif_2014_d(
        valeurs_df,
        metadata_df,
        finess_d=finess_d,
    )

    finess_d = build_finess_certif_2021_d(
        valeurs_df,
        metadata_df,
        finess_d=finess_d,
    )

    latest_autorisations_df = autorisations_df.sort_values(
        ["num_finess_et", "date_export", "activite", "modalite", "forme"]
    ).drop_duplicates(
        subset=["num_finess_et", "activite", "modalite", "forme"], keep="last"
    )

    latest_autorisations_df = latest_autorisations_df.loc[
        latest_autorisations_df["num_finess_et"].isin(
            latest_finess_df["num_finess_et"]
        )
    ]

    finess_d = build_finess_autorisation_d(
        latest_autorisations_df, finess_d=finess_d
    )

    finess_d = build_finess_activite_d(
        valeurs_df,
        metadata_df,
        source="Activités ET",
        activite_name_key="activites",
        finess_d=finess_d,
    )

    ###########################
    # Statistiques nationales #
    ###########################
    stats_d = build_national_stats(finess_d)

    return list(finess_d.values()), stats_d
