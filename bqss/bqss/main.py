import logging
from typing import Final

from requests import ConnectionError  # pylint: disable=redefined-builtin
from requests import HTTPError

import bqss.certification_14_20.constants
import bqss.certification_14_20.main
import bqss.certification_21_25.constants
import bqss.certification_21_25.main
from bqss import data_acquisition_utils
from bqss.bqss.constants import RESOURCES_DOMAIN_PATH
from bqss.bqss.open_data import update_metadata, upload_open_data
from bqss.bqss.processing import process_document_data, process_key_value_data
from bqss.constants import DATA_GOUV_API_KEY
from bqss.esatis import constants as esatis_constants, main as esatis_main
from bqss.finess import constants as finess_constants, main as finess_main
from bqss.iqss import constants as iqss_constants, main as iqss_main
from bqss.sae import constants as sae_constants, main as sae_main

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main(
    force_full_run: bool,
    data_validation: bool,
    open_data: bool,
):
    """
    Point d'entrée du pipeline BQSS. Ce pipeline agrège les sorties des
    pipelines des autres domaines de données constitutifs des données BQSS
    """
    if force_full_run or not finess_constants.FINAL_DOMAIN_PATH.exists():
        logger.info("Lancement du pipeline FINESS")
        finess_main.main(
            skip_data_download=False, data_validation=data_validation
        )
    if (
        force_full_run
        or not bqss.certification_14_20.constants.FINAL_DOMAIN_PATH.exists()
    ):
        logger.info("Lancement du pipeline certification ref 2014-2020")
        bqss.certification_14_20.main.main(
            skip_data_download=False, data_validation=data_validation
        )
    if (
        force_full_run
        or not bqss.certification_21_25.constants.FINAL_DOMAIN_PATH.exists()
    ):
        logger.info("Lancement du pipeline certification ref 2021-2025")
        bqss.certification_21_25.main.main(
            skip_data_download=False, data_validation=data_validation
        )
    if force_full_run or not sae_constants.FINAL_DOMAIN_PATH.exists():
        logger.info("Lancement du pipeline SAE")
        sae_main.main(
            skip_data_download=False, data_validation=data_validation
        )
    if force_full_run or not iqss_constants.FINAL_IQSS_PATH.exists():
        logger.info("Lancement du pipeline IQSS")
        iqss_main.main(
            skip_data_download=False, data_validation=data_validation
        )
    if force_full_run or not esatis_constants.FINAL_ESATIS_PATH.exists():
        logger.info("Lancement du pipeline ESATIS")
        esatis_main.main(
            skip_data_download=False, data_validation=data_validation
        )

    process_key_value_data()
    process_document_data()

    if open_data:
        if DATA_GOUV_API_KEY is None:
            logger.warning(
                "Clef d'API manquante pour data.gouv.fr, pas d'upload en open data"
            )
            return
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        try:
            update_metadata(data_sources)
        except (ConnectionError, HTTPError) as error:
            raise RuntimeError(
                "Impossible de mettre à jour les metadonnées data.gouv.fr, pas d'upload en open data"
            ) from error

        try:
            upload_open_data(data_sources)
        except (ConnectionError, HTTPError) as error:
            raise RuntimeError("L'upload sur data gouv a échoué") from error
