import logging
from typing import Final

from bqss.bqss.constants import FINAL_DOMAIN_PATH
from bqss.constants import RESOURCES_PATH
from bqss.data_gouv_utils import update_dataset_metadata, update_resource_data

logger: Final[logging.Logger] = logging.getLogger(__name__)


def update_metadata(
    data_sources: dict,
):
    """
    Met à jour la description du jeu de données sur la plateforme d'open data
    "Data Gouv" d'après le fichier se trouvant dans
    resources/data_gouv/doc_page_data_gouv.md.
    """
    logger.info("Updating dataset metadata on data.gouv.fr...")
    update_dataset_metadata(
        data_sources["dataset"]["id"],
        RESOURCES_PATH / "data_gouv/doc_page_data_gouv.md",
    )
    logger.info("Updating dataset metadata on data.gouv.fr OK")


def upload_open_data(
    data_sources: dict,
):
    """
    Met à jour l'ensemble des données BQSS telles qu'elles existent sur
    disque vers le dépôt de données en open data (data.gouv.fr).
    """
    logger.info("Updating dataset resources on data.gouv.fr...")
    for resource in data_sources["resources"]:
        logger.info("Uploading: %s", resource["filename"])
        update_resource_data(
            data_sources["dataset"]["id"],
            resource["id"],
            FINAL_DOMAIN_PATH / resource["filename"],
        )
    logger.info("Updating dataset resources on data.gouv.fr OK")
