La base sur la qualité et la sécurité des soins (BQSS) regroupe les informations sur la qualité des hôpitaux et des cliniques produites par la Haute Autorité de Santé, à savoir :

- Les résultats de certification
- Les indicateurs de qualité et de sécurité des soins associés

Elle contient également différentes informations de contexte, caractérisant les établissements de santé :

- Les informations d'immatriculation et d'autorisation du répertoire FINESS
- Les statistiques annuelles d'activité des établissements de santé

La BQSS alimente un espace dédié sur le site internet de la HAS nommé [QualiScope](hhttps://www.has-sante.fr/qualiscope).

Elle contient tout l'historique de ces données, de façon à permettre des analyses, pour tout acteur du secteur de la santé ou tous les citoyens qui souhaiteraient réutiliser ces données.

La documentation du jeu de donnée est [accessible en ligne](https://has-sante.gitlab.io/public/bqss/), pour plus de détails voir:

- [Tutoriels d'utilisation des données](https://has-sante.gitlab.io/public/bqss/donnees/tutoriels/index.html)
- [Description générale des données](https://has-sante.gitlab.io/public/bqss/donnees/general-description.html)
- [Le modèle de données des fichiers composants la base](https://has-sante.gitlab.io/public/bqss/donnees/schemas-finaux.html)
