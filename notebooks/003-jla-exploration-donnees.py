# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # NOTEBOOK 3 - EXPLORATION DES DONNEES BQSS AVANT AGGREGATION

# %% [markdown]
# <font color='red'>AVERTISSEMENT : les analyses ci-dessous ne sont présentées qu'à titre démonstratif, elles ne répondent à aucune hypothèse et ne s'inscrivent pas dans le cadre d'une démarche statistique rigoureuse.</font>

# %% [markdown]
# Ce noteboook est semblable au notebook 2 par sa structure et son contenu.
#
# Il a été développé avant l'agrégation de la base afin de commencer les analyses à partir des sources séparées.
#
# Les 2 notebooks sont presque totalement identiques après les étapes d'import et d'aggrégation.
# Les dataframe analysées comptent 8429 lignes dans les 2 notebooks.
# Le notebook 2 est à utiliser en préférence. Le notebbook 3 n'est dorénavant plus utilisé.

# %% [markdown]
# # 0. CONFIGURATION

# %% [markdown]
# ### Modules

# %%
import os
import pathlib

import keplergl
import matplotlib.pyplot as plt

# %%
import numpy as np
import pandas as pd
import scipy
import scipy.stats as st
import seaborn as sns

from bqss.constants import DATA_PATH, RESOURCES_PATH

# %% [markdown]
# ### Display

# %%
pd.set_option("display.max_columns", None)


# %% [markdown]
# # Fonctions développées spécifiquements pour les graphiques du projet

# %%
def draw_distribution_stacked_bar(df, var, y=0.5):
    """ "
    cette fonction permet d'obtenir un diagramme en barres emplilées avec :
    - une couleur pour chaque valeur de la colonne var de la df
    - les labels correspondent aux proportions relatives de chaque valeur de la colonne
    """
    fig, ax = plt.subplots(figsize=(12, 4))

    freq = df[var].value_counts().to_frame().sort_index()
    freq = freq / sum(freq[var]) * 100
    labels = freq.index.values
    colors = plt.get_cmap("RdYlGn")(np.linspace(0.85, 0.15, freq.shape[0]))
    start = 0
    for i in range(freq.shape[0]):
        rects = ax.barh(
            y,
            width=freq.iloc[i],
            label=labels[i],
            color=colors[i],
            left=start,
            edgecolor="none",
        )
        start += freq.iloc[i]
        ax.bar_label(rects, color="black", label_type="center", fmt="%.0f%%")
    if freq.shape[0] < 10:
        ax.legend(ncol=freq.shape[0])

    return fig, ax
    # if freq.shape[0] < 10:
    #    ax.legend(ncol=freq.shape[0],loc=(0.5,y+0.2))


# %%
def draw_multiple_distribution_stacked_bars(
    df, var_lines, var_colors, relative=False
):
    """ "
    cette fonction permet d'obtenir un diagramme en barres emplilées avec :
    - une barre pour chaque valeur de la colonne var_lines de la df
    - dans chaque barre, des barres empilées avec une couleur pour chaque valeur de la colonne var_colors de la df
    - la longueur des barres correspondent aux données de la table de contingence en valeurs absolues
    - les labels correspondent aux pourcentages relatifs à la valeur totale de chaque ligne de la table
    """

    cont = (
        df[[var_lines, var_colors]]
        .pivot_table(index=var_lines, columns=var_colors, aggfunc=len)
        .copy()
        .fillna(0)
        .astype(int)
    )
    cont_proportion = cont.apply(lambda x: x / sum(x), axis=1)
    cont_percent = cont_proportion.applymap(lambda x: f"{x:.0%}").replace(
        "0%", ""
    )

    labels = cont.index.values
    colors = plt.get_cmap("RdYlGn")(np.linspace(0.85, 0.15, cont.shape[1]))
    starts = [0] * cont.shape[0]

    fig, ax = plt.subplots(figsize=(12, 10))

    for i in range(cont.shape[1]):
        value = cont.columns[i]
        if relative == False:
            rects = ax.barh(
                labels,
                width=cont.loc[:, value],
                label=value,
                color=colors[i],
                left=starts,
                edgecolor="none",
            )
            starts = [
                x + y for (x, y) in zip(starts, cont.loc[:, value].tolist())
            ]
        else:
            rects = ax.barh(
                labels,
                width=cont_proportion.loc[:, value],
                label=value,
                color=colors[i],
                left=starts,
                edgecolor="none",
            )
            starts = [
                x + y
                for (x, y) in zip(
                    starts, cont_proportion.loc[:, value].tolist()
                )
            ]
        ax.bar_label(
            rects,
            labels=cont_percent.to_numpy()[:, i],
            color="black",
            label_type="center",
            fmt="%.0f%%",
        )

    ax.legend(ncol=cont.shape[1])

    plt.ylim(-1, cont.shape[0] + 1)

    return fig, ax


# %%
def plot_distribution_all_variables(df):
    for name in df.columns:
        fig, ax = draw_distribution_stacked_bar(df, name)
        plt.title(name, fontsize=14)
        plt.ylim(-1, 2)
        plt.show()


# %%
def multiple_box_plot(df, var_qual, var_quant):

    df[var_quant] = pd.to_numeric(df[var_quant], errors="coerce")
    df = df[df[var_quant].notna()]  # df = df_certif_iqss_finess
    print(df.shape)

    box_plot = sns.boxplot(x=df[var_qual], y=df[var_quant], palette="Blues")

    medians = round(df.groupby([var_qual])[var_quant].median(), 1)
    means = df.groupby([var_qual])[var_quant].mean()

    vertical_offset = 0.1
    for xtick in box_plot.get_xticks():
        print(xtick)
        box_plot.text(
            xtick,
            medians.iloc[xtick] + vertical_offset,
            medians.iloc[xtick],
            color="black",
            weight="semibold",
        )

    return box_plot


# %%
def pairplot_scatter_kde(df):
    def corrfunc(x, y, **kws):
        nas = np.logical_or(np.isnan(x), np.isnan(y))
        r, p = scipy.stats.pearsonr(x[~nas], y[~nas])
        n = round(len(x[~nas]))
        ax = plt.gca()
        ax.annotate(f"r = {r:.2f}", xy=(0.6, 0.9), xycoords=ax.transAxes)
        ax.annotate(f"p = {p:.2f}", xy=(0.6, 0.82), xycoords=ax.transAxes)
        ax.annotate(f"n = {n:.0f}", xy=(0.6, 0.74), xycoords=ax.transAxes)

    def count(x, **kws):
        nas = np.isnan(x)
        n = round(len(x[~nas]))
        ax = plt.gca()
        ax.annotate(f"n = {n:.0f}", xy=(0.6, 0.9), xycoords=ax.transAxes)

    g = sns.PairGrid(df)
    g.map_upper(plt.scatter, s=1)
    g.map_diag(sns.distplot, kde=False)
    g.map_diag(count)
    g.map_lower(sns.kdeplot, cmap="Blues_d", fill=False)
    g.map_lower(corrfunc)

    # g.set_xticklabels(b.get_xticks(), size = 15)

    for ax in g.axes.flat:
        _ = plt.setp(ax.get_yticklabels(), visible=True, size=10)
        _ = plt.setp(ax.get_xticklabels(), visible=True, size=10)

    xlabels, ylabels = [], []

    for ax in g.axes[-1, :]:
        xlabel = ax.xaxis.get_label_text()
        xlabels.append(xlabel)

    for ax in g.axes[:, 0]:
        ylabel = ax.yaxis.get_label_text()
        ylabels.append(ylabel)

    for i in range(len(xlabels)):
        for j in range(len(ylabels)):
            g.axes[j, i].xaxis.set_label_text(xlabels[i], size=12)
            g.axes[j, i].yaxis.set_label_text(ylabels[j], size=12)

    return g


# %%
def correletion_matrix(df, center=0, text_size=12):

    fig, ax = plt.subplots(figsize=(25, 18))

    df_corr = df.corr()
    mask = np.triu(np.ones_like(df_corr, dtype=np.bool))
    mask = mask[1:, :-1]
    corr = df_corr.iloc[1:, :-1].copy()

    pval = np.zeros([df.shape[1], df.shape[1]])
    nb = np.zeros([df.shape[1], df.shape[1]])
    for i in range(df.shape[1]):  # rows are the number of rows in the matrix.
        for j in range(df.shape[1]):
            nas = np.logical_or(
                np.isnan(df.iloc[:, i]), np.isnan(df.iloc[:, j])
            )
            r, p = scipy.stats.pearsonr(
                df.iloc[:, i][~nas], df.iloc[:, j][~nas]
            )
            n = round(len(df.iloc[:, i][~nas]))
            pval[i, j] = p
            nb[i, j] = n

    mask = np.triu(np.ones_like(pval, dtype=np.bool))
    mask = mask[1:, :-1]
    pval = pval[1:, :-1].copy()

    cmap = sns.diverging_palette(0, 230, 90, 60, as_cmap=True)

    sns.set(font_scale=1)

    heatmap = sns.heatmap(
        corr,
        mask=mask,
        annot=True,
        annot_kws={"size": 15},
        fmt=".2f",
        linewidths=2,
        cmap=cmap,
        center=center,
        cbar_kws={"shrink": 0.8},
        square=True,
    )

    for y in range(pval.shape[0]):
        for x in range(y + 1):
            ax.text(
                x + 0.35,
                y + 0.8,
                "p = " "%.2E" % pval[y, x],
                color="black",
                size=text_size,
            )
            ax.text(
                x + 0.35,
                y + 0.9,
                "n = " "%.0f" % nb[y, x],
                color="black",
                size=text_size,
            )

    return (heatmap, fig, ax)


# %%
def clustermap(df, center=0, size_text=12):
    g = sns.clustermap(
        df.corr(),
        center=center,
        cmap=sns.diverging_palette(0, 230, 90, 60, as_cmap=True),
        annot=True,
        annot_kws={"size": 15},
        fmt=".2f",
        dendrogram_ratio=(0.1, 0.1),
        linewidths=0.75,
        figsize=(20, 20),
    )

    g.cax.set_visible(False)

    order = g.dendrogram_row.reordered_ind
    df = df.iloc[:, order]

    pval = np.zeros([df.shape[1], df.shape[1]])
    nb = np.zeros([df.shape[1], df.shape[1]])
    for i in range(df.shape[1]):  # rows are the number of rows in the matrix.
        for j in range(df.shape[1]):
            nas = np.logical_or(
                np.isnan(df.iloc[:, i]), np.isnan(df.iloc[:, j])
            )
            r, p = scipy.stats.pearsonr(
                df.iloc[:, i][~nas], df.iloc[:, j][~nas]
            )
            n = round(len(df.iloc[:, i][~nas]))
            pval[i, j] = p
            nb[i, j] = n

    for y in range(pval.shape[0]):
        for x in range(pval.shape[0]):
            g.ax_heatmap.text(
                x + 0.35,
                y + 0.8,
                "p = " "%.2E" % pval[y, x],
                color="black",
                size=size_text,
            )
            g.ax_heatmap.text(
                x + 0.35,
                y + 0.9,
                "n = " "%.0f" % nb[y, x],
                color="black",
                size=size_text,
            )
    return df, g


# %% [markdown]
# # 1. Import des données

# %% [markdown]
# ### Import fichier région

# %%
REGION_PATH = os.path.join(RESOURCES_PATH, "regions-france.csv")

df_geo = pd.read_csv(REGION_PATH, sep=";")

print(df_geo.shape)
df_geo.describe(include="all")

# %%
df_geo.head()

# %% [markdown]
# ### Import du fichier FINESS

# %%
FINESS_PATH = os.path.join(DATA_PATH, "finess/final/finess.csv")

df_finess = pd.read_csv(FINESS_PATH, sep=",")
df_finess.shape

# %%
df_finess.describe(include="all")

# %% [markdown]
# ### Import IQSS

# %%
IQSS_PATH = os.path.join(DATA_PATH, "iqss/final/iqss.csv")

df_iqss_all = pd.read_csv(IQSS_PATH, sep=",")
print(df_iqss_all.shape)

# %%
df_iqss_all["finess"] = df_iqss_all["finess"].astype(str)
b = df_iqss_all["finess"].apply(len)
print(b.value_counts())

df_iqss_all["finess"] = df_iqss_all["finess"].apply(lambda x: x.zfill(9))

# %%
df_iqss_all.describe(include="all")

# %%
df_iqss_all["annee"].value_counts()

# %% [markdown]
# ### Import CERTIF

# %%
CERTIF_PATH = os.path.join(DATA_PATH, "certification/final/certifications.csv")

df_certif = pd.read_csv(CERTIF_PATH, sep=",")
print(df_certif.shape)

# %%
df_certif.describe(include="all")

# %% [markdown]
# ### Import SAE

# %%
SAE_PATH = os.path.join(DATA_PATH, "sae/final/sae.csv")

df_sae = pd.read_csv(SAE_PATH, sep=",")
print(df_sae.shape)

# %%
df_sae.describe(include="all")

# %% [markdown]
# ## Import ESATIS

# %%
ESATIS_PATH = os.path.join(DATA_PATH, "esatis/final/esatis.csv")

df_esatis = pd.read_csv(ESATIS_PATH, sep=",")
print(df_esatis.shape)

# %%
df_esatis.describe(include="all")

# %%
df_esatis.query('finess_geo == "950000331"')

# %% [markdown]
# # AGREGGATION APROXIMATIVE : base IQSS 2018 = 3392 établissements

# %% [markdown]
# ## Préselection Année

# %% [markdown]
# ### IQSS : Pour les analyses, on ne prends que les années 2018 et 2019 (reccueil 2019 et 2020)

# %%
df_iqss_2018 = df_iqss_all.query("annee == 2018").dropna(
    axis="columns", how="all"
)

print(df_iqss_2018.shape)
df_iqss_2019 = df_iqss_all.query("annee == 2019").dropna(
    axis="columns", how="all"
)
print(df_iqss_2019.shape)
df_iqss_2018_2019 = df_iqss_2018.merge(df_iqss_2019, on="finess", how="left")
print(df_iqss_2018_2019.shape)

# %%
df_iqss_2018.describe(include="all")

# %%
df_iqss_2019.describe(include="all")

# %% [markdown]
# # 2. SELECTION LIGNES, MERGE

# %% [markdown]
# ### Finess : on ne prends que 2019 (date d'export 31-12-19)

# %%
df_finess_2019 = df_finess.query('dateexport == "2019-12-31"')
print(df_finess_2019.shape)

# %% [markdown]
# ### Certification : on enlève les doublons (rq : plus de doublons !)

# %%
print(df_certif.shape)
df_certif_clean = df_certif.drop_duplicates(["finess"])
print(df_certif_clean.shape)

# %% [markdown]
# ### ESATIS : on ne prends que 2020 (données de 2019 ?)

# %%
print(df_esatis.shape)
df_esatis_2020 = df_esatis.query("annee == 2020").dropna(
    axis="columns", how="all"
)
print(df_esatis_2020.shape)

# %% [markdown]
# ### IQSS with FINESS

# %%
df_iqss_finess = df_finess_2019.merge(
    df_iqss_2018_2019, left_on="nofinesset", right_on="finess", how="right"
)
print(df_iqss_finess.shape)

# %% [markdown]
# ### CERTIF with FINESS

# %%
df_certif_finess = df_finess_2019.merge(
    df_certif, left_on="nofinesset", right_on="finess", how="right"
)
print(df_certif_finess.shape)

# %% [markdown]
# ### IQSS + FINESS with CERTIF

# %%
df_certif_iqss_finess = df_iqss_finess.merge(
    df_certif_clean, on="finess", how="outer"
)
print(df_certif_iqss_finess.shape)

# %% [markdown]
# ### IQSS +CERTIF + FINESS with ESATIS

# %%
df_certif_iqss_finess_esatis = df_certif_iqss_finess.merge(
    df_esatis_2020, left_on="finess", right_on="finess_geo", how="outer"
)
print(df_certif_iqss_finess_esatis.shape)

# %% [markdown]
# ### IQSS/CERTIF/FINESS/ESATIS with GEO

# %%
df_certif_iqss_finess_esatis_geo = df_certif_iqss_finess_esatis.merge(
    df_geo, left_on="region", right_on="code_region", how="outer"
)
print(df_certif_iqss_finess_esatis_geo.shape)

# %%
df_certif_iqss_finess_esatis_geo.describe(include="all")

# %% [markdown]
# ## DF FINALE POUR ANALYSES

# %%
df = df_certif_iqss_finess_esatis_geo

# %% [markdown]
# # 3. ANALYSE UNIVARIEE

# %% [markdown]
# ## Conversion format numérique

# %% [markdown]
# ### Variables IQSS

# %%
df["mco_dpa_pcd_2018-resultat_pcd"] = (
    df["mco_dpa_pcd_2018-resultat_pcd"].astype(str).str.replace("%", "")
)
df["ssr_dpa_pcd_2018-resultat_pcd"] = (
    df["ssr_dpa_pcd_2018-resultat_pcd"].astype(str).str.replace("%", "")
)
df["had_dpa_dtn_2017-classe_dtn"] = (
    df["had_dpa_dtn_2017-classe_dtn"].astype(str).str.replace("%", "")
)
# df_certif_iqss_finess_esatis_geo["resultat_tre"] = (
#    df_certif_iqss_finess_esatis_geo["resultat_tre"]
#    .astype(str)
#    .str.replace("%", "")
# )

quantitative_columns = [
    "mco_eteortho_eteortho_2018-ete_ortho_alerte_sup",
    # "mco_eteortho_eteortho_2019-ete_ortho_alerte_sup",
    "mco_isoortho_isoortho_2019-iso_ortho_alerte_sup",
    "mhs_ias_icsha_2018-resultat_icsha",
    "psy_ias_icsha_2018-resultat_icsha",
    "mco_ca_qls_2018-resultat_qls",
    "ssr_dpa_qls_2018-resultat_qls",
    #   "resultat_qls.2",
    "mco_dpa_pcd_2018-resultat_pcd",
    "ssr_dpa_pcd_2018-resultat_pcd",
    "ssr_dpa_pspv_2018-resultat_pspv",
    #   "resultat_tre",
    "had_dpa_coord_2018-resultat_coord",
    "had_dpa_tdp_2018-resultat_tdp",
]

for var in quantitative_columns:
    df[var] = pd.to_numeric(df[var], errors="coerce")

# %% [markdown]
# ### Variables ESATIS

# %%
quantitative_columns_esatis = [
    "score_all_ajust",
    "score_avh_ajust",
    "score_acc_ajust",
    "score_pec_ajust",
    "score_cer_ajust",
    "score_ovs_ajust",
    "taux_reco_brut_ca",
    "taux_reco_brut_48h",
    "score_all_rea_ajust",
    "score_accueil_rea_ajust",
    "score_pecinf_rea_ajust",
    "score_pecmed_rea_ajust",
    "score_chambre_rea_ajust",
    "score_repas_rea_ajust",
    "score_sortie_rea_ajust",
]

for var in quantitative_columns_esatis:
    df[var] = pd.to_numeric(
        df[var].astype(str).str.replace(",", "."),
        errors="coerce",
    )

# %% [markdown]
# ### Missing values

# %%
nan = df.count()
nan = nan / 3392 * 100
nan = nan.sort_values()
print(nan)

sns.set(style="darkgrid", rc={"figure.figsize": (10, 20)})

fig = nan.plot(kind="barh", color="skyblue")

plt.title("missing values", fontsize=15)

plt.show()

# %% [markdown]
# ### Libmft

# %%
var = "libsph"
var = "libmft"

cat = df[var].value_counts().sort_index().to_frame("counts")
cat = cat.sort_values(by="counts")
print(cat)

sns.set(style="darkgrid", rc={"figure.figsize": (12, 9)})

plt.pie(
    cat["counts"],
    colors=plt.get_cmap("bwr")(np.linspace(1, 0.2, len(cat))),
    labels=cat.index,
    autopct="%.0f%%",
    explode=(0, 0, 0, 0, 0, 0.6, 0, 0, 0, 0, 0),
    textprops={"fontsize": 12},
    pctdistance=0.7,
    wedgeprops=dict(width=0.6, edgecolor="w"),
)
plt.title("libmft", fontsize=14)

plt.show()

# %% [markdown]
# ### Catégorie des établissements de la BASE

# %%
df_cat = df[["libcategetab", "libcategagretab", "finess"]]
df_cat = df_cat.groupby(["libcategagretab", "libcategetab"]).count()
print(df_cat)
first_level_index = np.unique(df_cat.index.get_level_values(0))
second_level_index = np.unique(df_cat.index.get_level_values(1))

vals = df_cat["finess"]
labels = first_level_index
second_labels = second_level_index

# Major category values = sum of minor category values
group_sum = df_cat.groupby("libcategagretab")["finess"].sum()

a, b, c, d, e, f, g, h, i, j, k, l, m, n = [
    plt.cm.Oranges,
    plt.cm.YlOrBr,
    plt.cm.YlGn,
    plt.cm.Greens,
    plt.cm.RdPu,
    plt.cm.Greys,
    plt.cm.Greys,
    plt.cm.Greys,
    plt.cm.YlGn,
    plt.cm.YlOrBr,
    plt.cm.Oranges,
    plt.cm.Reds,
    plt.cm.Blues,
    plt.cm.Purples,
]

outer_colors = [
    a(0.6),
    b(0.4),
    c(0.4),
    d(0.6),
    e(0.6),
    f(0.6),
    g(0.6),
    h(0.6),
    i(0.4),
    j(0.4),
    k(0.6),
    l(0.6),
    m(0.6),
    n(0.6),
]
inner_colors = [
    a(0.5),
    a(0.4),
    a(0.3),
    b(0.3),
    b(0.2),
    c(0.3),
    d(0.5),
    e(0.5),
    e(0.4),
    f(0.5),
    g(0.5),
    h(0.5),
    i(0.3),
    j(0.3),
    k(0.5),
    l(0.5),
    l(0.4),
    l(0.3),
    l(0.2),
    m(0.5),
    n(0.5),
]

fig, ax = plt.subplots(figsize=(12, 9))
size = 0.3
ax.pie(
    group_sum,
    labels=labels,
    colors=outer_colors,
    radius=1,
    autopct="%.0f%%",
    pctdistance=0.85,
    explode=(0, 0, 0, 0, 0, 0.3, 0.15, 0, 0, 0, 0, 0, 0, 0),
    wedgeprops=dict(width=size, edgecolor="w"),
)

ax.pie(
    vals,
    radius=1 - size,
    colors=inner_colors,
    autopct="%.0f%%",
    pctdistance=0.8,
    wedgeprops=dict(width=size, edgecolor="w"),
)

ax.set_title(
    "Categories des établissements de la BASE",
    fontsize=18,
    pad=15,
)
plt.tight_layout()
plt.show()

# %% [markdown]
# ### Catégorie des établissements certifiés

# %%
df_cat = df_certif_finess[["libcategetab", "libcategagretab", "finess"]]
df_cat = df_cat.groupby(["libcategagretab", "libcategetab"]).count()
print(df_cat)

first_level_index = np.unique(df_cat.index.get_level_values(0))
second_level_index = np.unique(df_cat.index.get_level_values(1))

vals = df_cat["finess"]
labels = first_level_index
second_labels = second_level_index

# Major category values = sum of minor category values
group_sum = df_cat.groupby("libcategagretab")["finess"].sum()

a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y = [
    plt.cm.Greens,
    plt.cm.Oranges,
    plt.cm.YlOrBr,
    plt.cm.YlGn,
    plt.cm.Greens,
    plt.cm.gist_heat,
    plt.cm.pink,
    plt.cm.RdPu,
    plt.cm.Greys,
    plt.cm.Greys,
    plt.cm.Greys,
    plt.cm.Greys,
    plt.cm.Greys,
    plt.cm.Greys,
    plt.cm.Greys,
    plt.cm.YlGn,
    plt.cm.Greys,
    plt.cm.YlOrBr,
    plt.cm.Oranges,
    plt.cm.Greys,
    plt.cm.Greys,
    plt.cm.Greys,
    plt.cm.Reds,
    plt.cm.Blues,
    plt.cm.Purples,
]

outer_colors = [
    a(0.6),
    b(0.6),
    c(0.4),
    d(0.4),
    e(0.6),
    f(0.6),
    g(0.6),
    h(0.6),
    i(0.6),
    j(0.4),
    k(0.6),
    l(0.6),
    m(0.6),
    n(0.6),
    o(0.6),
    p(0.4),
    q(0.6),
    r(0.4),
    s(0.6),
    t(0.6),
    u(0.6),
    v(0.6),
    w(0.6),
    x(0.6),
    y(0.6),
]
inner_colors = [
    a(0.5),
    a(0.4),
    b(0.5),
    b(0.4),
    b(0.3),
    c(0.4),
    c(0.4),
    c(0.3),
    c(0.3),
    c(0.2),
    c(0.2),
    c(0.1),
    c(0.1),
    d(0.3),
    e(0.5),
    f(0.5),
    g(0.5),
    h(0.5),
    h(0.4),
    i(0.5),
    j(0.3),
    k(0.5),
    l(0.5),
    m(0.5),
    n(0.5),
    o(0.5),
    p(0.3),
    q(0.5),
    r(0.3),
    s(0.5),
    t(0.5),
    t(0.4),
    u(0.5),
    u(0.4),
    u(0.3),
    v(0.5),
    w(0.5),
    w(0.4),
    w(0.3),
    w(0.2),
    x(0.5),
    y(0.5),
]


fig, ax = plt.subplots(figsize=(12, 9))
size = 0.3
ax.pie(
    group_sum,
    labels=labels,
    colors=outer_colors,
    radius=1,
    autopct="%.0f%%",
    pctdistance=0.85,
    explode=(
        0,
        0,
        0,
        0,
        0,
        0,
        0.3,
        0,
        0,
        0.15,
        0.3,
        0,
        0,
        0.15,
        0.3,
        0,
        0,
        0,
        0,
        0.1,
        0.2,
        0.3,
        0,
        0,
        0,
    ),
    wedgeprops=dict(width=size, edgecolor="w"),
)

ax.pie(
    vals,
    radius=1 - size,
    colors=inner_colors,
    autopct="%.0f%%",
    pctdistance=0.8,
    wedgeprops=dict(width=size, edgecolor="w"),
)

ax.set_title(
    "Categories de tous les établissements certifiés", fontsize=18, pad=15
)
plt.tight_layout()
plt.show()

# %% [markdown]
# ### Niveau de certification de tous les établissement certifiés

# %%
sns.set(style="darkgrid")

fig, ax = draw_distribution_stacked_bar(df_certif, "idNiveauCertification")

plt.title(
    "Niveau de certification de tous les établissements certifiés", fontsize=14
)

plt.ylim(-1, 2)

plt.show()

# %% [markdown]
# ### Niveau de certification BASE

# %%
sns.set(style="darkgrid")

fig, ax = draw_distribution_stacked_bar(df, "idNiveauCertification")

plt.title(
    "Niveau de certification de tous les établissements certifiés", fontsize=14
)

plt.ylim(-1, 2)

plt.show()

# %% [markdown]
# ## Distribution de toutes les variables IQSS

# %%
plot_distribution_all_variables(df_iqss_2018_2019)

# %% [markdown]
# ### Distribution de toutes les variables ESATIS

# %%
plot_distribution_all_variables(df_esatis_2020)

# %% [markdown]
# ## Distribution variables quantitatives

# %% [markdown]
# ### VARIABLES IQSS

# %%
n_rows = 3
n_cols = 3
# Create the subplots
fig, axes = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(15, 30))

quantitative_columns = [
    # "mco_eteortho_eteortho_2018-ete_ortho_alerte_sup",
    # "mco_eteortho_eteortho_2019-ete_ortho_alerte_sup",
    # "mco_isoortho_isoortho_2019-iso_ortho_alerte_sup",
    "mhs_ias_icsha_2018-resultat_icsha",
    "psy_ias_icsha_2018-resultat_icsha",
    "mco_ca_qls_2018-resultat_qls",
    "ssr_dpa_qls_2018-resultat_qls",
    #   "resultat_qls.2",
    "mco_dpa_pcd_2018-resultat_pcd",
    "ssr_dpa_pcd_2018-resultat_pcd",
    "ssr_dpa_pspv_2018-resultat_pspv",
    # "had_dpa_dtn_2017-classe_dtn",
    #   "resultat_tre",
    "had_dpa_coord_2018-resultat_coord",
    "had_dpa_tdp_2018-resultat_tdp",
]

for i, column in enumerate(quantitative_columns):
    sns.histplot(
        df[column],
        ax=axes[i // n_cols, i % n_cols],
        kde=True,
    )

# %% [markdown]
# ### VARIABLES ESATIS

# %%
n_rows = 5
n_cols = 3
# Create the subplots
fig, axes = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(15, 30))


quantitative_columns_esatis = [
    "score_all_ajust",
    "score_avh_ajust",
    "score_acc_ajust",
    "score_pec_ajust",
    "score_cer_ajust",
    "score_ovs_ajust",
    "taux_reco_brut_ca",
    "taux_reco_brut_48h",
    "score_all_rea_ajust",
    "score_accueil_rea_ajust",
    "score_pecinf_rea_ajust",
    "score_pecmed_rea_ajust",
    "score_chambre_rea_ajust",
    "score_repas_rea_ajust",
    "score_sortie_rea_ajust",
]

for i, column in enumerate(quantitative_columns_esatis):
    sns.histplot(df[column], ax=axes[i // n_cols, i % n_cols], kde=True)

# %% [markdown]
# # 4 ANALYSES BIVARIEES
# ## Entre 2 variables catégorielles

# %% [markdown]
# ### Lien entre niveau de certification et catégorie de l'établissement (singificatif)

# %%
X = "libcategagretab"
Y = "idNiveauCertification"

cont = (
    df[[X, Y]]
    .pivot_table(index=X, columns=Y, aggfunc=len)
    .copy()
    .fillna(0)
    .astype(int)
)
print(cont)
st_chi2, st_p, st_dof, st_exp = st.chi2_contingency(cont)
print(st_chi2, st_p)

fig, ax = draw_multiple_distribution_stacked_bars(
    df, "libcategagretab", "idNiveauCertification"
)

plt.title(
    "Niveau de certification des établissements de la base IQSS selon leur catégorie",
    fontsize=14,
)

plt.show()

# %% [markdown]
# ### Lien entre niveau de certification et région pour les hôpitaux (significatif)

# %%
X = "libregion"
Y = "idNiveauCertification"

cont = (
    df[[X, Y]]  # .query('libcategagretab == "Centres Hospitaliers"')
    .pivot_table(index=X, columns=Y, aggfunc=len)
    .copy()
    .fillna(0)
    .astype(int)
)
print(cont)
st_chi2, st_p, st_dof, st_exp = st.chi2_contingency(cont)
print(st_chi2, st_p)

fig, ax = draw_multiple_distribution_stacked_bars(
    df.query('libcategagretab == "Centres Hospitaliers"'),
    "libregion",
    "idNiveauCertification",
    relative=True,
)

plt.title(
    "Niveau de certification des centres hospitaliers de la base IQSS selon leur région",
    fontsize=14,
)

plt.show()

# %% [markdown]
# ### Lien entre indicateur ICSHA (Indice de consammation des solutions hydro-alcooliques) et catégorie/secteur d'activité (significatif)

# %%
X = "libcategagretab"
Y = "mhs_ias_icsha_2018-icsha_classe"

cont = (
    df[[X, Y]]
    .pivot_table(index=X, columns=Y, aggfunc=len)
    .copy()
    .fillna(0)
    .astype(int)
)
print(cont)
st_chi2, st_p, st_dof, st_exp = st.chi2_contingency(cont)
print(st_chi2, st_p)

fig, ax = draw_multiple_distribution_stacked_bars(
    df,
    "libcategagretab",
    "mhs_ias_icsha_2018-icsha_classe",
)

plt.title(
    "Indice de consommation de solutions hydro-alcooliques des établissements de la base IQSS selon leur catégorie",
    fontsize=14,
)

plt.show()

# %% [markdown]
# ### Corrélation ICSHA et Niveau de certif (significatif)

# %%
Y = "idNiveauCertification"
X = "mhs_ias_icsha_2018-icsha_classe"

cont = (
    df[[X, Y]]
    .pivot_table(index=X, columns=Y, aggfunc=len)
    .copy()
    .fillna(0)
    .astype(int)
)
print(cont)
st_chi2, st_p, st_dof, st_exp = st.chi2_contingency(cont)
print(st_chi2, st_p)

fig, ax = draw_multiple_distribution_stacked_bars(
    df,
    "mhs_ias_icsha_2018-icsha_classe",
    "idNiveauCertification",
    relative=True,
)

plt.title(
    "Indice de consommation de solutions hydro-alcooliques des établissements de la base IQSS selon leur catégorie",
    fontsize=14,
)

plt.show()

# %% [markdown]
# ## Entre 1 variable catégorielle et 1 quantitiative

# %% [markdown]
# ### ICSHA : sécoupage valeur/classe

# %%
fig, ax = plt.subplots(figsize=(8, 8))

var_quant = "mhs_ias_icsha_2018-resultat_icsha"
var_qual = "mhs_ias_icsha_2018-icsha_classe"


# df = df[df[var_quant].notna()]


histplot = sns.histplot(data=df, x=var_quant, hue=var_qual, kde="True").set(
    xlim=(0)
)
plt.title("classe ICSHA selon le score ICSHA", fontsize=14)
plt.xlim(0, 300)
plt.show()

grid = sns.FacetGrid(data=df, col=var_qual, height=5, aspect=1)
grid.map(sns.histplot, var_quant, kde="True", binwidth=5, hue=df[var_qual])
grid.set(xlim=(0, 300), ylim=(0, 200))
grid.add_legend()
grid.fig.subplots_adjust(top=0.9)
grid.fig.suptitle("classe ICSHA selon le score ICSHA", fontsize=14)
plt.show()


# %% [markdown]
# ### Niveau de certif vs eteortho (significatif)

# %% [raw]
# var_quant = "NA_ETEORTHO_ETEORTHO_v2-ete_ortho_ratio_oe_x"
# var_qual = "idNiveauCertification"
#
# fig, ax = plt.subplots(figsize=(10, 8))
#
# box_plot = multiple_box_plot(
#     df,
#     var_qual,
#     var_quant,
# )
#
# box_plot.set_xlabel("Niveau de Certif", fontsize=10)
# box_plot.set_ylabel(var_quant, fontsize=10)
# plt.title("ETEORTHO vs CERTIF", fontsize=15)
# plt.ylim(-1, 4)
# plt.show()
#
#
# scipy.stats.f_oneway(
#     df[df[var_quant].notnull()][var_quant][df[var_qual] == 1],
#     df[df[var_quant].notnull()][var_quant][df[var_qual] == 2],
# )

# %% [markdown]
# ### Niveau de certif vs MCO - QLS (significatif)

# %%
var_quant = "mco_ca_qls_2018-resultat_qls"
var_qual = "idNiveauCertification"


fig, ax = plt.subplots(figsize=(10, 8))

box_plot = multiple_box_plot(
    df,
    var_qual,
    var_quant,
)

box_plot.set_xlabel("Niveau de Certif", fontsize=10)
box_plot.set_ylabel(var_quant, fontsize=10)
plt.title("QLS vs CERTIF", fontsize=15)
plt.show()


scipy.stats.f_oneway(
    df[df[var_quant].notnull()][var_quant][df[var_qual] == 1],
    df[df[var_quant].notnull()][var_quant][df[var_qual] == 2],
)

# %% [markdown]
# ### Niveau de certif vs SSR - PSPV (significatif)

# %%
var_quant = "ssr_dpa_pspv_2018-resultat_pspv"
var_qual = "idNiveauCertification"


fig, ax = plt.subplots(figsize=(10, 8))

box_plot = multiple_box_plot(
    df,
    var_qual,
    var_quant,
)

box_plot.set_xlabel("Niveau de Certif", fontsize=10)
box_plot.set_ylabel(var_quant, fontsize=10)
plt.title("QLS vs CERTIF", fontsize=15)
plt.show()


scipy.stats.f_oneway(
    df[df[var_quant].notnull()][var_quant][df[var_qual] == 1],
    df[df[var_quant].notnull()][var_quant][df[var_qual] == 2],
)

# %% [markdown]
# ### Certif vs HAD - DTN (significatif)

# %% [raw]
# var_quant = "resultat_dtn"
# var_qual = "idNiveauCertification"
#
# fig, ax = plt.subplots(figsize=(10, 8))
#
# box_plot = multiple_box_plot(
#     df,
#     var_qual,
#     var_quant,
# )
#
# box_plot.set_xlabel("Niveau de Certif", fontsize=10)
# box_plot.set_ylabel(var_quant, fontsize=10)
# plt.title("QLS vs CERTIF", fontsize=15)
# plt.show()
#
#
# scipy.stats.f_oneway(
#     df[df[var_quant].notnull()][var_quant][df[var_qual] == 1],
#     df[df[var_quant].notnull()][var_quant][df[var_qual] == 2],
# )

# %% [markdown]
# ##  CORRELATIONS ENTRE VARIABLES QUANTITATIVES

# %% [raw]
# df = df.rename(
#     columns={"NA_ETEORTHO_ETEORTHO_v2-ete_ortho_ratio_oe_x": "Eteortho_2018"}
# )
# df = df.rename(
#     columns={"NA_ETEORTHO_ETEORTHO_v2-ete_ortho_ratio_oe_y": "Eteortho_2019"}
# )
# df = df.rename(
#     columns={"NA_ISOORTHO_ISOORTHO_v2-iso_ortho_ratio_oe": "Isoortho_2019"}
# )

# %% [markdown]
# ### VARIABLES IQSS MCO

# %%
df_mco = df[
    [
        "mhs_ias_icsha_2018-resultat_icsha",
        "mco_ca_qls_2018-resultat_qls",
        "mco_dpa_pcd_2018-resultat_pcd",
    ]
]

# %%
df_mco, g = clustermap(df_mco)
g.fig.suptitle("Correlation matrix : IQSS MCO", fontsize=30)
plt.show()

# %%
fig = plt.figure(
    num=None, figsize=(15, 15), dpi=80, facecolor="w", edgecolor="k"
)

g = pairplot_scatter_kde(df_mco)

# g.axes[0, 0].set(xlim=(0, 15), ylim=(0, 15))
# g.axes[1, 1].set(xlim=(0, 15), ylim=(0, 15))
# g.axes[2, 2].set(xlim=(0, 15), ylim=(0, 15))
# g.axes[3, 3].set(xlim=(40, 100), ylim=(40, 100))
g.axes[1, 1].set(xlim=(0, 200), ylim=(0, 200))


plt.show()

# %%
heatmap, fig, ax = correletion_matrix(df_mco)

plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.yticks(rotation=0)
plt.title("Correlation matrix : IQSS MCO", fontsize=30)
plt.show()

# %% [markdown]
# ## VARIABLES IQSS SSR

# %%
df_ssr = df[
    [
        "mhs_ias_icsha_2018-resultat_icsha",
        "ssr_dpa_qls_2018-resultat_qls",
        "ssr_dpa_pcd_2018-resultat_pcd",
        "ssr_dpa_pspv_2018-resultat_pspv",
    ]
]
df_ssr, g = clustermap(df_ssr)
plt.show()

# %%
fig = plt.figure(
    num=None, figsize=(15, 15), dpi=80, facecolor="w", edgecolor="k"
)

g = pairplot_scatter_kde(df_ssr)

g.axes[0, 0].set(xlim=(0, 200), ylim=(0, 200))
# g.axes[3, 3].set(xlim=(40, 100), ylim=(40, 100))

plt.show()

# %%
heatmap, fig, ax = correletion_matrix(df_ssr)

plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.yticks(rotation=0)
plt.title("Correlation matrix : IQSS SSR", fontsize=30)
plt.show()

# %% [markdown]
# ### VARIABLES IQSS HAD

# %%
df_had = df[
    [
        "mhs_ias_icsha_2018-resultat_icsha",
        "had_dpa_coord_2018-resultat_coord",
        "had_dpa_tdp_2018-resultat_tdp",
    ]
]

df_had, g = clustermap(df_had)
plt.show()

# %%
fig = plt.figure(
    num=None, figsize=(15, 15), dpi=80, facecolor="w", edgecolor="k"
)

g = pairplot_scatter_kde(df_had)

g.axes[0, 0].set(xlim=(0, 200), ylim=(0, 200))

plt.show()

# %%
heatmap, fig, ax = correletion_matrix(df_had)

plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.yticks(rotation=0)
plt.title("Correlation matrix : IQSS HAD", fontsize=30)
plt.show()

# %% [markdown]
# ### VARIABLES ESATIS : groupe fichier 'ca'

# %%
columns_esatis_group_1 = [
    "score_all_ajust",
    "score_avh_ajust",
    "score_acc_ajust",
    "score_pec_ajust",
    "score_cer_ajust",
    "score_ovs_ajust",
    "taux_reco_brut_ca",
]

columns_esatis_group_2 = [
    "taux_reco_brut_48h",
    "score_all_rea_ajust",
    "score_accueil_rea_ajust",
    "score_pecinf_rea_ajust",
    "score_pecmed_rea_ajust",
    "score_chambre_rea_ajust",
    "score_repas_rea_ajust",
    "score_sortie_rea_ajust",
]

df_esatis_ca = df[columns_esatis_group_1]

df_esatis_48 = df[columns_esatis_group_2]

# %%
df_esatis_ca, g = clustermap(df_esatis_ca, center=0.3)
plt.show()

# %%
fig = plt.figure(
    num=None, figsize=(15, 15), dpi=80, facecolor="w", edgecolor="k"
)


g = pairplot_scatter_kde(df_esatis_ca)

plt.show()

# %%
heatmap, fig, ax = correletion_matrix(df_esatis_ca, center=0.3)

plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.yticks(rotation=0)
plt.title("Correlation matrix : Esatis CA", fontsize=30)
plt.show()

# %% [markdown]
# ### VARIABLES ESATIS : groupe fichier '48'

# %%
df_esatis_48, g = clustermap(df_esatis_48, center=0.3)
plt.show()

# %%
fig = plt.figure(
    num=None, figsize=(15, 15), dpi=80, facecolor="w", edgecolor="k"
)

g = pairplot_scatter_kde(df_esatis_48)

plt.show()

# %%
heatmap, fig, ax = correletion_matrix(df_esatis_48, center=0.3)

plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.yticks(rotation=0)
plt.title("Correlation matrix : Esatis 48", fontsize=30)
plt.show()

# %% [markdown]
# ### Fichier 'ca' et fichier '48'

# %%
df_esatis_quanti = df[columns_esatis_group_1 + columns_esatis_group_2]

df_esatis_quanti, g = clustermap(df_esatis_quanti, center=0.4, size_text=8)
plt.show()

# %%
fig = plt.figure(
    num=None, figsize=(15, 15), dpi=80, facecolor="w", edgecolor="k"
)

g = pairplot_scatter_kde(df_esatis_quanti)

plt.show()

# %%
heatmap, fig, ax = correletion_matrix(
    df_esatis_quanti, center=0.4, text_size=8
)

plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.yticks(rotation=0)
plt.title("Correlation matrix : Esatis all", fontsize=30)
plt.show()

# %% [markdown]
# ### IQSS et ESATIS

# %%
quantitative_columns_mco_esatis = [
    "mhs_ias_icsha_2018-resultat_icsha",
    "mco_ca_qls_2018-resultat_qls",
    "mco_dpa_pcd_2018-resultat_pcd",
    "score_all_ajust",
    "score_avh_ajust",
    "score_acc_ajust",
    "score_pec_ajust",
    "score_cer_ajust",
    "score_ovs_ajust",
    "taux_reco_brut_ca",
    "taux_reco_brut_48h",
    "score_all_rea_ajust",
    "score_accueil_rea_ajust",
    "score_pecinf_rea_ajust",
    "score_pecmed_rea_ajust",
    "score_chambre_rea_ajust",
    "score_repas_rea_ajust",
    "score_sortie_rea_ajust",
]

df_esatis_quanti = df[quantitative_columns_mco_esatis]

df_esatis_quanti, g = clustermap(df_esatis_quanti, center=0, size_text=7)
plt.show()

# %% [markdown]
# ### IQSS et ESATIS 48 (48h)

# %%
quantitative_columns_mco_esatis = [
    "mhs_ias_icsha_2018-resultat_icsha",
    "mco_ca_qls_2018-resultat_qls",
    "mco_dpa_pcd_2018-resultat_pcd",
    "taux_reco_brut_48h",
    "score_all_rea_ajust",
    "score_accueil_rea_ajust",
    "score_pecinf_rea_ajust",
    "score_pecmed_rea_ajust",
    "score_chambre_rea_ajust",
    "score_repas_rea_ajust",
    "score_sortie_rea_ajust",
]

df_esatis_quanti = df[quantitative_columns_mco_esatis]

df_esatis_quanti, g = clustermap(df_esatis_quanti, center=0, size_text=8)
plt.show()

# %% [markdown]
# ## IQSS et ESATIS CA

# %%
quantitative_columns_mco_esatis = [
    "mhs_ias_icsha_2018-resultat_icsha",
    "mco_ca_qls_2018-resultat_qls",
    "mco_dpa_pcd_2018-resultat_pcd",
    "score_all_ajust",
    "score_avh_ajust",
    "score_acc_ajust",
    "score_pec_ajust",
    "score_cer_ajust",
    "score_ovs_ajust",
    "taux_reco_brut_ca",
]

df_esatis_quanti = df[quantitative_columns_mco_esatis]

df_esatis_quanti, g = clustermap(df_esatis_quanti, center=0, size_text=7)
plt.show()

# %% [markdown]
# # GEOLOCALISATION

# %%
kepler_map = keplergl.KeplerGl(height=400)
kepler_map.add_data(data=df, name="etablissements")
kepler_map

# %%
REGION_PATH = os.path.join(RESOURCES_PATH, "regions-france.csv")

df_geo = pd.read_csv(REGION_PATH, sep=";")

print(df_geo.shape)
df_geo.describe(include="all")

# %%
conf = {
    "version": "v1",
    "config": {
        "visState": {
            "filters": [],
            "layers": [
                {
                    "id": "nk7i12r",
                    "type": "geojson",
                    "config": {
                        "dataId": "etablissements",
                        "label": "etablissements",
                        "color": [255, 203, 153],
                        "columns": {"geojson": "geo"},
                        "isVisible": True,
                        "visConfig": {
                            "opacity": 0.8,
                            "strokeOpacity": 0.8,
                            "thickness": 0.5,
                            "strokeColor": [248, 149, 112],
                            "colorRange": {
                                "name": "Global Warming",
                                "type": "sequential",
                                "category": "Uber",
                                "colors": [
                                    "#5A1846",
                                    "#900C3F",
                                    "#C70039",
                                    "#E3611C",
                                    "#F1920E",
                                    "#FFC300",
                                ],
                            },
                            "strokeColorRange": {
                                "name": "Global Warming",
                                "type": "sequential",
                                "category": "Uber",
                                "colors": [
                                    "#5A1846",
                                    "#900C3F",
                                    "#C70039",
                                    "#E3611C",
                                    "#F1920E",
                                    "#FFC300",
                                ],
                            },
                            "radius": 10,
                            "sizeRange": [0, 10],
                            "radiusRange": [0, 50],
                            "heightRange": [0, 500],
                            "elevationScale": 5,
                            "enableElevationZoomFactor": True,
                            "stroked": True,
                            "filled": True,
                            "enable3d": False,
                            "wireframe": False,
                        },
                        "hidden": False,
                        "textLabel": [
                            {
                                "field": None,
                                "color": [255, 255, 255],
                                "size": 18,
                                "offset": [0, 0],
                                "anchor": "start",
                                "alignment": "center",
                            }
                        ],
                    },
                    "visualChannels": {
                        "colorField": {
                            "name": "code_region",
                            "type": "integer",
                        },
                        "colorScale": "quantile",
                        "strokeColorField": None,
                        "strokeColorScale": "quantile",
                        "sizeField": None,
                        "sizeScale": "linear",
                        "heightField": None,
                        "heightScale": "linear",
                        "radiusField": None,
                        "radiusScale": "linear",
                    },
                }
            ],
            "interactionConfig": {
                "tooltip": {
                    "fieldsToShow": {
                        "etablissements": [
                            {"name": "code_region", "format": None}
                        ]
                    },
                    "compareMode": False,
                    "compareType": "absolute",
                    "enabled": True,
                },
                "brush": {"size": 0.5, "enabled": False},
                "geocoder": {"enabled": False},
                "coordinate": {"enabled": False},
            },
            "layerBlending": "normal",
            "splitMaps": [],
            "animationConfig": {"currentTime": None, "speed": 1},
        },
        "mapState": {
            "bearing": 0,
            "dragRotate": False,
            "latitude": 46.99981729727944,
            "longitude": 6.261067613792845,
            "pitch": 0,
            "zoom": 3.63852988207011,
            "isSplit": False,
        },
        "mapStyle": {
            "styleType": "dark",
            "topLayerGroups": {},
            "visibleLayerGroups": {
                "label": True,
                "road": True,
                "border": False,
                "building": True,
                "water": True,
                "land": True,
                "3d building": False,
            },
            "threeDBuildingColor": [
                9.665468314072013,
                17.18305478057247,
                31.1442867897876,
            ],
            "mapStyles": {},
        },
    },
}

# %%
kepler_map = keplergl.KeplerGl(height=400, config=conf)
kepler_map.add_data(data=df_geo, name="etablissements")
kepler_map

# %%
conf = kepler_map.config

# %%
geometryString = {
    "type": "Polygon",
    "coordinates": [
        [
            [-74.158491, 40.835947],
            [-74.148473, 40.834522],
            [-74.142598, 40.833128],
            [-74.151923, 40.832074],
            [-74.158491, 40.835947],
        ]
    ],
}

# json_str = json.dumps(geometryString)
# print(json_str)

geometryString_2 = df_geo.iloc[1, 1].replace(",", ", ")
# print(geometryString_2)


# create data frame
df_with_geometry = pd.DataFrame(
    {"id": [1], "geometry_string": [geometryString_2]}
)

# add to map
map_1 = keplergl.KeplerGl(height=400)
map_1.add_data(df_with_geometry, "df_with_geometry")
map_1
