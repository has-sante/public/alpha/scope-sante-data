# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Tutoriel - Usage de la Base sur la Qualité et la Sécurité des Soins (BQSS)
#
# Ce notebook vise à montrer un usage type dont peut être l'objet BQSS, de façon programmatique grâce au langage Python.

# %% [markdown]
# ## Import des librairies et des données
#
# Les URLS des données sont récupérable sur la page de la Base sur la Qualité et la Sécurité des Soins sur la plateforme Data Gouv accessible via [ce lien](https://www.data.gouv.fr/fr/datasets/base-sur-la-qualite-et-la-securite-des-soins-anciennement-scope-sante/). La section __Fichiers__ contient les données disponibles, et pour chaque fichier, une description est présente incluant notamment un __URL stable__ qu'il est recommandé d'utiliser.
#
# La base BQSS adoptant un format clé-valeurs, il est nécessaire de récupérer la table des valeurs d'intérêts (concernant la qualité et la sécurité des soins) ainsi que les métadonnées associées spécifiant ces valeurs et les données caractérisant les établissements de santé via les numéro FINESS.

# %%
import numpy as np
import pandas as pd  # version >= 1.4.2

# chargement du fichier valeurs.csv dans un dataframe pandas
valeurs_df = pd.read_csv(
    "https://www.data.gouv.fr/fr/datasets/r/da295ccc-2011-4995-b810-ad1f1a82a83f",
    low_memory=False,
)

# chargement du fichier metadata.csv dans un dataframe pandas
metadata_df = pd.read_csv(
    "https://www.data.gouv.fr/fr/datasets/r/e56fb5a5-5a74-4507-ba77-e0411d4aa234",
    low_memory=False,
)

# chargement du fichier finess.csv dans un dataframe pandas
finess_df = pd.read_csv(
    "https://www.data.gouv.fr/fr/datasets/r/48dadbce-56a8-4dc4-ac51-befcdfd82521",
    low_memory=False,
)

# %% [markdown]
# ## Observation des données
#
# Il est désormais possible de consulter les informations caractérisant ces données et en observer un échantillon.

# %% [markdown]
# ### Caractérisation des données valeurs_df

# %%
valeurs_df.info()
valeurs_df.sample(n=5, random_state=187)

# %% [markdown]
# Un grand nombre de variables de nature différente sont regroupées dans cette table (d'où la table metadata venant spécifier la nature de chaque variables). Le nom des variables est stocké dans la colonne `key`.
#
# Afin de pouvoir distinguer les différents types de valeurs (booléen, entier, décimal, chaîne de caractères, valeurs manquantes, dates), des colonnes dédiées leur sont attachées.
#
# Ainsi, pour une variable donnée, un seul type est possible... :
# - sur l'ensemble de la classe (hormis la colonne `missing_value`)
# - pour chaque valeur (justifiant la nature lacunaire de ces colonnes, toutes les colonnes autre que celle prenant la valeur étant à NaN)

# %% [markdown]
# ### Caractérisation des données metadata_df

# %%
metadata_df.info()
metadata_df.sample(n=5, random_state=123)

# %% [markdown]
# La colonne `name` de cette table permet de faire la jointure avec la table `valeurs_df` via la colonne `key` associée.
#
# Les métadonnées permettent de compléter les informations associées aux variables, en particulier leur `type` (encodé implicitement dans `valeur.csv`, voir ci-dessus), leur `source` (provenant d'origines variées), leur `secteur`, leur `theme ` et leur `acronyme`.
#
# Pour une recherche rapide, vous pouvez aussi utiliser directement le fichier `metadata.xlsx` qui permet de faire des filtres rapides sur les différentes colonnes.

# %% [markdown]
# ### Caractérisation des données finess_df

# %%
finess_df.info()
finess_df.sample(n=5, random_state=123)

# %% [markdown]
# Cette table contient l'ensemble des données Finess **historisées**.
# Il y a une ligne par export et par Finess présent à la date de l'export.
# Pour un même Finess il ya donc très souvent plusieurs lignes.
#
# Les colonnes qui s'avèrent souvent utiles sont:
# - `num_finess_et`: numéro finess géographique
# - `num_finess_ej`: numéro finess juridique
# - `latitude` et `longitude`: les coordonnées géographiques de l'établissement dans le référentiel WGS84
# - `raison_sociale_et`: le nom de l'établissement
# - `dernier_enregistrement`: indique si cette ligne est la dernière connue pour le finess en question

# %% [markdown]
# ## Préparation des données

# %% [markdown]
# ### Récupération des clés associées à des catégories de valeurs
#
# Les cas d'usages pour lesquels la base est utilisé ne concernent souvant qu'un sous-ensemble des domaines présents dans la base. On peut par exemple vouloir récupérer l'historique complet des IQSS issus du questionnaire patient (ESATIS).
#
# La mécanique pour sélectionner le sous ensemble de données d'interêt est la suivante:
# - Dans le fichier `metadata.csv` utiliser la colonne `source` pour identifier le domaine d'intérêt
# - Dans le fichier `metadata.csv` recupérer le nom des variables de la `source` donnée qui sont stockées dans la colonne `name`
# - Parmis ces variables, utiliser les colonne `type_metier` (pour les IQSS) et/ou `description` pour sélectionner les variables d'intérêt
# - Dans le fichier `valeurs.csv` sélection toutes les lignes dont la colonne `key` contient une variable du domaine d'intérêt
#
# Ci-dessous un exemple de cette mécanique:

# %%
# Affichage des sources de données existantes
metadata_df["source"].value_counts()

# %%
# Récupération des noms des variables de certification v2014
certif_14_keys = metadata_df[metadata_df["source"] == "Certification v2014"][
    "name"
]

# Récupération des nom des variables esatis
esatis_keys = metadata_df[
    metadata_df["source"] == "IQSS questionnaire patient"
]["name"]

# %%
# Affichage des types métiers certification
# Les variables de certification n'ont pas de type métier
metadata_df.loc[
    metadata_df["name"].isin(certif_14_keys),
    ["name", "description", "type_metier"],
].head(5)

# %%
# Affichage des types métiers esatis
metadata_df.loc[
    metadata_df["name"].isin(esatis_keys),
    ["name", "description", "type_metier"],
].head(5)

# %%
# Affichage des variables de type score pour esatis
metadata_df.loc[
    metadata_df["name"].isin(esatis_keys)
    & (metadata_df["type_metier"] == "score"),
    ["name", "description", "type_metier"],
]

# %% [markdown]
# ### Isolation des domaines de données
#
# Afin de pouvoir préparer et analyser un domaine de données, il est nécessaire de l'isoler en vue de traitements décrits plus bas.
# Pour des raisons de volumétrie, le présent tutoriel se restreint aux données esatis. Mais il est naturellement possible d'effectuer ces analyses sur un ensemble de domaines de données, voire sur tous les domaines (les mêmes opérations décrites ici peuvent leur être appliquées).

# %%
# Isolation du domaine de données esatis
esatis_df = valeurs_df[valeurs_df["key"].isin(esatis_keys)]

# Isolation du domaine de données certification v2014, pour illustration
certif_df = valeurs_df[valeurs_df["key"].isin(certif_14_keys)]

# %% [markdown]
# ### Pivot de la table esatis
#
# Actuellement, le format clé-valeur de la base induit que chaque valeur de chaque variable pour chacun des établissements est associée à une ligne.
# Nous souhaitons pourvoir avoir un regroupement des valeurs pour chaque établissement.
#
# Pour cela, nous devons effectuer les opérations suivantes (cf. cellule de code ci-dessous) :
# - Effectuer un pivot à la table pour avoir une colonne par variable (par type), chaque ligne représentant un établissement pour une année donnée (puisque certaines valeurs sont collectées annuellement)
# - Chaque classe de valeur n'ayant qu'un type de valeur acceptable (hormis le type `missing_value`), toutes les colonnes des autres types seront remplis de `NaN` => il s'agit donc également de supprimer ces colonnes inutiles
# - Les colonnes des valeurs manquantes pour une clé sont fusionnées avec la colonne de valeurs de la clé correspondante pour ne pas perdre l'information correspondante
#

# %%
# Les colonnes de valeurs sont pivotées selon le colonne key, pour chaque triplet (annee, finess, finess_type)
esatis_df = esatis_df.pivot(
    index=["annee", "finess", "finess_type"], columns="key"
)

# Toutes les colonnes de valeurs liées à un type incorrect vis-à-vis du type attendu sont remplies par des NaN.
# Il s'agit ici de supprimer ces colonnes qui ne portent aucune information exploitable
esatis_df = esatis_df.dropna(axis=1, how="all")

# Les colonnes associées au type missing_value sont fusionnées avec leur colonne de valeur respective
# Cette étape est optionnelle si les valeurs manquantes sont sans intérêts pour l'étude
if "missing_value" in esatis_df:
    esatis_df = esatis_df.combine_first(esatis_df["missing_value"]).drop(
        columns="missing_value"
    )

# Les colonnes qui résultent de ces opérations ont plusieurs niveaux qui sont ici applatis pour n'en avoir plus qu'un
esatis_df = esatis_df.droplevel(0, axis=1)
# Les données d'années, de finess et de finess_type utilisées jusqu'ici comme index sont basculées en colonnes dans la table.
esatis_df = esatis_df.reset_index()

# %% [markdown]
# Un échantillon de la table résultante peut être observée ci-dessous, en notant qu'il manque des données pour de nombreux établissements sur certaines années car certains indicateurs n'étaient pas calculés à ce moment là.

# %%
esatis_df.sample(n=5, random_state=123)

# %% [markdown]
# ## Exploitation des données
#
# Dans cette section, une analyse simple sur le domaine esatis est effectuée à des fins d'illustration quant à la valorisation de ces données.
#
# Le cas d'usage de cette analyse est l'observation de l'évolution du positionnement national d'un établissement identifié (il s'agit ici de l'établissement associé au finess n°`060000478`) vis-à-vis des établissements de même catégorie (il s'agit ici de la catégorie _Centres Hospitaliers_, associée au code `1102`) sur l'indicateur `score_all_ajust_ca` (correspondant au "score de satisfaction globale ajusté" en chirurgie ambulatoire).
#
# Pour ce faire, il faut :
# - associer aux données esatis les données finess nécessaires à la jointure et à la catégorisation des établissements
# - calculer le classement de l'établissement (son rang absolu et le pourcentage associé) pour chaque année
# - représenter la distribution des établissements pour chaque année en positionnant l'établissement étudié
#
# Ces étapes sont réalisées dans les deux sections ci-dessous.

# %% [markdown]
# ### Finalisation de la préparation des données

# %%
# On ne prend que les informations des finess les plus récentes pour éviter les doublons à la jointure
finess_df = finess_df.loc[finess_df["dernier_enregistrement"]]

# Jointure entre les données esatis et les données Finess restreintes aux agrégats de catégories d'établissement
df = finess_df[
    [
        "num_finess_et",
        "categorie_agregat_et",
        "raison_sociale_et",
        "departement",
    ]
].merge(esatis_df, left_on="num_finess_et", right_on="finess", how="inner")
df.sample(5, random_state=123)

# %%
# Seuls les centres hospitaliers sont conservés
df = df[df["categorie_agregat_et"] == 1102]

# Pour cette étude, seules les colonnes suivantes sont nécessaires pour la suite : annee, taux_reco_brut_ca, finess
# Par ailleurs, cet indicateur n'est collecté qu'a partir de 2019.
df = df.loc[
    df["annee"] >= 2019,
    [
        "annee",
        "finess",
        "raison_sociale_et",
        "departement",
        "score_all_ajust_ca",
    ],
]

# Les établissements sans valeurs pour l'indicateur sont retirés
df = df.dropna()

# %% [markdown]
# Un extrait de la table correspondante peut être observée :

# %%
df.sample(5, random_state=123)

# %% [markdown]
# ### Visualisation des résultats

# %%
es_finess = "060000478"
axes = df.plot.hist(by="annee", bins=20, figsize=(10, 15), legend=False)
for i, annee in enumerate(np.sort(df["annee"].unique())):
    axes[i].set_xlim(0, 100)
    axes[i].set_ylim(0, 37)
    axes[i].set_ylabel("Fréquences", fontsize=12)
    df_tmp = (
        df[df["annee"] == annee]
        .sort_values("score_all_ajust_ca", ascending=False)
        .reset_index()
    )
    value = df_tmp.loc[
        df_tmp["finess"] == es_finess, "score_all_ajust_ca"
    ].values[0]
    axes[i].axvline(value, color="red")
    axes[i].text(value - 3.2, -4.5, str(value), color="red", fontsize=12)
    axes[i].text(
        value + 1, 38, "finess n°" + str(es_finess), color="red", fontsize=12
    )
    rank = df_tmp[df_tmp["finess"] == es_finess].index.item() + 1
    card_finess = df_tmp.shape[0]
    rank_ratio = 100 * rank / card_finess
    axes[i].text(
        5,
        30,
        f"classement : {rank} / {card_finess}\nposition (%) : {rank_ratio:,.1f}",
        backgroundcolor="lightgrey",
        color="red",
        fontsize=12,
        fontweight="roman",
    )
axes[2].set_xlabel(
    "score de satisfaction globale ajusté",
    fontsize=12,
    labelpad=15,
)


# %% [markdown]
# ⚠️ Attention : L’analyse et l’interprétation des résultats des IQSS nécessite de bien prendre connaissance de leur définition.
#

# %% [markdown]
# Nous pouvons constater que l'établissement considéré dans cette étude a:
# - Amélioré son score (de `77.73` à `80.78`)
# - Amélioré sa place dans la distribution de `54.6`ème percentile à `24.3`ème percentile
#
