# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
print(
    "This is a test notebook",
    "Try modifying it and check the integration with jupytext",
    "It should automatically save the file as a .py file",
)
