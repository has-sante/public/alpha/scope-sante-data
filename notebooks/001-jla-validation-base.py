# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # NOTEBOOK I - VALIDATION DE LA NOUVELLE BASE BQSS

# %% [markdown]
# <font color='red'>AVERTISSEMENT : les analyses ci-dessous ne sont présentées qu'à titre démonstratif, elles ne répondent à aucune hypothèse et ne s'inscrivent pas dans le cadre d'une démarche statistique rigoureuse.</font>

# %% [markdown]
# Ce notebook a pour objectif de valider la complétude et la cohérence de la nouvelle base agrégée.
#
# Cette analyse se décompose en 4 parties :
# - L'analyse des établissements (finess) présents dans la nouvelle base, dans chacune des tables.
# - L'analyse des différences entre les établissements présents dans la nouvelle base et ceux de l'ancienne base ATIH.
# - L'analyse univariée des indicateurs présents dans la nouvelle base; par type (numérique, booléen et catégoriel)
# - La comparaison (vérification de la correspondance) entre les valeurs présentes dans la nouvelle base et l'ancienne base ATIH. Seules les valeurs relatives à 3 indicateurs ont été comparées, les correspondances entre indicateurs de la nouvelle et de l'ancienne base n'ayant pas été établies de manière systématique/structurée.

# %% [markdown]
# ### Table des matières
#
# * [0 : Configuration du notebook](#0)
#     * [0.1 : Import des modules](#0.1)
#     * [0.2 : Option de display](#0.2)
#     * [0.3 : Fonctions développées spécifiquement pour le notebook](#0.3)
# * [1 : Import des données](#1)
#     * [1.1 : Import des données ATIH](#1.1)
#     * [1.2 : Import nouvelle base agrégée](#1.2)
#     * [1.3 : Import données avant agrégation + quelques analyses](#1.3)
# * [2 : Analayse des Finness nouvelle base](#2)
#     * [2.1 : Table indicateurs : nombres de finess geo/jur par année et par source](#2.1)
#     * [2.2 : Les établissements de la table finess sont-ils bien tous présents dans les données finess ?
#     Ceux avec le flag bqss santé sont-ils bien présents dans les données finess 2020/21 ?](#2.2)
#     * [2.3 Les finess de la table clé-valeur sont-ils bien dans la table finess ?](#2.3)
#     * [2.4 Les établissements des bases avant aggrégation sont-ils bien présents dans la table finess ?](#2.4)
#     * [2.5 Table autorisations](#2.5)
# * [3 : Analyse des finess : comparaison base BQSS avec ATIH](#3)
#     * [3.1 : ATIH vs notre base](#3.1)
#     * [3.2 : Pourquoi y a 75 établissements dans ATIH absents de notre base ?](#3.2)
#     * [3.3 : Pourquoi y a-t-il plus de 8000+ établissements dans notre base et absents dans ATIH ?](#3.3)
# * [4 Analyse des indicateurs : nouvelle base](#4)
#     * [4.1 : Ananlyse générale des indicateurs](#4.1)
#     * [4.2 : Indicateurs numériques](#4.2)
#     * [4.3 : Indicateurs booléens](#4.3)
#     * [4.4 : Indicateurs catégoriels](#4.4)
#     * [4.5 : Cohérence entre indicateurs](#4.5)
# * [5 Analyse des indicateurs : comparaison base BQSS avec ATIH](#5)
#     * [5.1 : Niveau de Certification](#5.1)
#     * [5.2 : Indicateur IQSS : ICSHA](#5.2)
#     * [5.3 : Indicateur Esatis : Note globale CA](#5.3)

# %% [markdown]
# ## 0. Configuration du notebook <a class="anchor" id="0"></a>

# %% [markdown]
# ### 0.1 Import des modules <a class="anchor" id="0.1"></a>

# %%
import os
import pathlib

import keplergl
import matplotlib.pyplot as plt

# %%
import numpy as np
import pandas as pd
import scipy
import scipy.stats as st
import seaborn as sns
from matplotlib_venn import venn2, venn3

from bqss.constants import DATA_PATH, RESOURCES_PATH

# %% [markdown]
# ### 0.2 Option de display<a class="anchor" id="0.2"></a>

# %%
pd.set_option("display.max_columns", None)


# %% [markdown]
# ### 0.3 Fonctions développées spécifiquements pour les graphiques du projet <a class="anchor" id="0.3"></a>

# %%
def draw_distribution_stacked_bar(df, var, y=0.5):
    """ "
    cette fonction permet d'obtenir un diagramme en barres emplilées avec :
    - une couleur pour chaque valeur de la colonne var de la df
    - les labels correspondent aux proportions relatives de chaque valeur de la colonne
    """
    fig, ax = plt.subplots(figsize=(12, 4))

    freq = df[var].value_counts().to_frame().sort_index()
    freq = freq / sum(freq[var]) * 100
    labels = freq.index.values
    colors = plt.get_cmap("RdYlGn")(np.linspace(0.85, 0.15, freq.shape[0]))
    start = 0
    for i in range(freq.shape[0]):
        rects = ax.barh(
            y,
            width=freq.iloc[i],
            label=labels[i],
            color=colors[i],
            left=start,
            edgecolor="none",
        )
        start += freq.iloc[i]
        ax.bar_label(rects, color="black", label_type="center", fmt="%.0f%%")
    if freq.shape[0] < 10:
        ax.legend(ncol=freq.shape[0])

    return fig, ax
    # if freq.shape[0] < 10:
    #    ax.legend(ncol=freq.shape[0],loc=(0.5,y+0.2))


# %%
def draw_multiple_distribution_stacked_bars(
    df, var_lines, var_colors, relative=False
):
    """ "
    cette fonction permet d'obtenir un diagramme en barres emplilées avec :
    - une barre pour chaque valeur de la colonne var_lines de la df
    - dans chaque barre, des barres empilées avec une couleur pour chaque valeur de la colonne var_colors de la df
    - la longueur des barres correspondent aux données de la table de contingence en valeurs absolues
    - les labels correspondent aux pourcentages relatifs à la valeur totale de chaque ligne de la table
    """

    cont = (
        df[[var_lines, var_colors]]
        .pivot_table(index=var_lines, columns=var_colors, aggfunc=len)
        .copy()
        .fillna(0)
        .astype(int)
    )
    cont_proportion = cont.apply(lambda x: x / sum(x), axis=1)
    cont_percent = cont_proportion.applymap(lambda x: f"{x:.0%}").replace(
        "0%", ""
    )
    print(cont)
    labels = cont.index.values
    colors = plt.get_cmap("RdYlGn")(np.linspace(0.85, 0.15, cont.shape[1]))
    starts = [0] * cont.shape[0]

    fig, ax = plt.subplots(figsize=(12, 10))

    for i in range(cont.shape[1]):
        value = cont.columns[i]
        if relative == False:
            rects = ax.barh(
                labels,
                width=cont.loc[:, value],
                label=value,
                color=colors[i],
                left=starts,
                edgecolor="none",
            )
            starts = [
                x + y for (x, y) in zip(starts, cont.loc[:, value].tolist())
            ]
        else:
            rects = ax.barh(
                labels,
                width=cont_proportion.loc[:, value],
                label=value,
                color=colors[i],
                left=starts,
                edgecolor="none",
            )
            starts = [
                x + y
                for (x, y) in zip(
                    starts, cont_proportion.loc[:, value].tolist()
                )
            ]
        ax.bar_label(
            rects,
            labels=cont_percent.to_numpy()[:, i],
            color="black",
            label_type="center",
            fmt="%.0f%%",
        )

    ax.legend(ncol=cont.shape[1])

    # plt.ylim(-1, cont.shape[0] + 1)

    return fig, ax


# %%
def plot_distribution_all_variables(df):
    for name in df.columns:
        fig, ax = draw_distribution_stacked_bar(df, name)
        plt.title(name, fontsize=14)
        plt.ylim(-1, 2)
        plt.show()


# %%
def compare_venn(df1, df2, field1, field2, label1="df1", label2="df2"):

    list1 = np.unique(df1[field1].astype(str)).tolist()
    print(f"Il y a {len(list1)} éléments uniques dans la première df")

    list2 = np.unique(df2[field2].astype(str)).tolist()
    print(f"Il y a {len(list2)} éléments uniques dans la seconde df")

    c = list(set(list1) & set(list2))
    print(f"Il y a {len(c)} éléments en communs entre les 2 df")

    v = venn2([set(list1), set(list2)], set_labels=(label1, label2))
    v.get_patch_by_id("10").set_color("pink")
    v.get_patch_by_id("01").set_color("skyblue")
    v.get_patch_by_id("11").set_color("purple")
    return v


def compare_venn_3(
    df1,
    df2,
    df3,
    field1,
    field2,
    field3,
    label1="df1",
    label2="df2",
    label3="df3",
):
    list1 = np.unique(df1[field1].astype(str)).tolist()
    print(f"Il y a {len(list1)} éléments uniques dans la première df")

    list2 = np.unique(df2[field2].astype(str)).tolist()
    print(f"Il y a {len(list2)} éléments uniques dans la seconde df")

    list3 = np.unique(df3[field3].astype(str)).tolist()
    print(f"Il y a {len(list3)} éléments uniques dans la troisième df")

    v = venn3(
        [set(list1), set(list2), set(list3)],
        set_labels=(label1, label2, label3),
    )
    return v


# %% [markdown]
# # 1. Import des données <a class="anchor" id="1"></a>

# %% [markdown]
# # 1.1 Import des données ATIH <a class="anchor" id="1.1"></a>

# %% [markdown]
# ### 1.1.1 Import ATIH Etablissements

# %%
ATIH_PATH = os.path.join(RESOURCES_PATH, "atih/etablissement.csv")

df_atih_finess = pd.read_csv(ATIH_PATH, sep=",")

print(df_atih_finess.shape)
df_atih_finess.describe(include="all")

# %%
no_atih = np.unique(df_atih_finess["finess"]).tolist()
print(f"il y a {len(no_atih)} établissement dans la base ATIH etablissements")

# %% [markdown]
# ### 1.1.2 Import ATIH valeur

# %%
ATIH_PATH = os.path.join(RESOURCES_PATH, "atih/valeur.csv")

df_valeur_atih = pd.read_csv(ATIH_PATH, sep=",")

print(df_valeur_atih.shape)
df_valeur_atih.describe(include="all")

# %%
no_atih_valeur = np.unique(df_valeur_atih["etablissement_id"]).tolist()
print(f"Il y a {len(no_atih_valeur)} établissement dans la base ATIH valeur")

# %%
no_atih_meta = np.unique(df_valeur_atih["meta_donnee_id"]).tolist()
print(f"Il y a {len(no_atih_meta)} indicateurs dans la base ATIH valeur")

# %% [markdown]
# ### 1.1.3 Import ATIH métadonnées

# %%
ATIH_PATH = os.path.join(RESOURCES_PATH, "atih/meta_donnee.csv")

df_meta_atih = pd.read_csv(ATIH_PATH, sep=",")

print(df_meta_atih.shape)
df_meta_atih.describe(include="all")

# %% [markdown]
# ### 1.1.4 Merge finess + valeurs + metadata

# %%
df_atih = df_valeur_atih.merge(
    df_atih_finess, left_on="etablissement_id", right_on="id", how="inner"
)
print(df_atih.shape)

# %%
df_atih = df_atih.merge(
    df_meta_atih, left_on="meta_donnee_id", right_on="id", how="inner"
)
print(df_atih.shape)

df_atih["etablissement_id"] = df_atih["etablissement_id"].astype(str)
df_atih["meta_donnee_id"] = df_atih["meta_donnee_id"].astype(str)
df_atih.describe(include="all")

# %% [markdown]
# # 1.2 Import Nouvelle Base agrégée <a class="anchor" id="1.2"></a>

# %% [markdown]
# ### 1.2.1 Import Finess

# %%
FINESS_PATH = os.path.join(DATA_PATH, "bqss/final/finess.csv")

df_base_finess = pd.read_csv(FINESS_PATH, sep=",")

print(df_base_finess.shape)
df_base_finess.describe(include="all")

# %%
df_base_finess_bqss = df_base_finess.query(
    "actif_qualiscope == True and dernier_enregistrement == True"
)
print(df_base_finess_bqss.shape)

# %%
df_base_finess["nofinesset"] = df_base_finess["nofinesset"].astype(str)
b = df_base_finess["nofinesset"].apply(len)
print(b.value_counts())

# %% [markdown]
# ### 1.2.2 Import valeurs

# %%
VALUE_PATH = os.path.join(DATA_PATH, "bqss/final/valeurs.csv")

df_base_valeurs = pd.read_csv(VALUE_PATH, sep=",")

print(df_base_valeurs.shape)
df_base_valeurs.describe(include="all")

# %% [markdown]
# ### 1.2.3 Import Autorisations

# %%
VALUE_PATH = os.path.join(DATA_PATH, "bqss/final/autorisations_as.csv")

df_base_autorisations = pd.read_csv(VALUE_PATH, sep=",")

print(df_base_autorisations.shape)
df_base_autorisations.describe(include="all")

# %%
df_base_autorisations_2021 = df_base_autorisations.query(
    'datefin > "2021-01-01"'
)
print(df_base_autorisations_2021.shape)

# %% [markdown]
# ### 1.2.4 Import métadonnées

# %%
VALUE_PATH = os.path.join(DATA_PATH, "bqss/final/metadata.csv")

df_base_meta = pd.read_csv(VALUE_PATH, sep=",")

print(df_base_meta.shape)
df_base_meta.describe(include="all")
df_base_meta["name"] = df_base_meta["name"].str.lower()

# %% [markdown]
# ### 1.2.5 : merge valeurs et metadonnées

# %%
df_base_valeurs_meta = df_base_valeurs.merge(
    df_base_meta[["name", "source", "type"]].drop_duplicates(),
    left_on="key",
    right_on="name",
    how="left",
)

print(df_base_valeurs_meta.shape)
print(df_base_valeurs_meta.sample(20))

# %% [markdown]
# ## 1.3 Import Données Avant aggrégation + quelques analyses <a class="anchor" id="1.3"></a>

# %% [markdown]
# ### 1.3.1 Import Finess

# %%
FINESS_PATH = os.path.join(DATA_PATH, "finess/final/finess.csv")

df_finess = pd.read_csv(FINESS_PATH, sep=",")
print(df_finess.shape)

# %% [markdown]
# Attention au format des finess : 9 caractères

# %%
df_finess["nofinesset"] = df_finess["nofinesset"].astype(str)
b = df_finess["nofinesset"].apply(len)
print(b.value_counts())

df_finess["nofinesset"] = df_finess["nofinesset"].apply(lambda x: x.zfill(9))

df_finess["nofinessej"] = df_finess["nofinessej"].astype(str)
b = df_finess["nofinessej"].apply(len)
print(b.value_counts())

df_finess["nofinessej"] = df_finess["nofinessej"].apply(lambda x: x.zfill(9))

# %%
df_finess.describe(include="all")

# %% [markdown]
# #### Evolution finess au cours des années

# %%
sns.set(style="darkgrid", rc={"figure.figsize": (10, 7)})

df_finess["dateexport"] = df_finess["dateexport"].astype(str)

print(df_finess["dateexport"].value_counts())

countplot = sns.countplot(
    x=("dateexport"), data=df_finess.sort_values("dateexport"), color="skyblue"
)

countplot.set(xlabel="dateexport")
countplot.set(ylabel="nb lignes")
plt.title("FINESS : nb lignes par annee", fontsize=20)
plt.show()

# %% [markdown]
# Envrion 100 000 finess géographiques chaque année

# %%
v = compare_venn(
    df_finess.query('dateexport == "2004-12-31"'),
    df_finess.query('dateexport == "2021-09-13"'),
    "nofinesset",
    "nofinesset",
    "2004",
    "2021",
)
plt.title(
    "Etablissements présents dans la base FINESS \n en 2004 et en 2021",
    fontsize=20,
)
plt.savefig("Etablissements présents dans la base FINESS en 2004 et en 2021")
plt.show()

# %% [markdown]
# ### 1.3.2 Import AUTORISATIONS

# %%
FINESS_PATH = os.path.join(DATA_PATH, "finess/final/autorisations_as.csv")

df_autorisations = pd.read_csv(FINESS_PATH, sep=",")
print(df_autorisations.shape)

# %%
df_autorisations.describe(include="all")

# %% [markdown]
# ### 1.3.3 Import IQSS

# %%
IQSS_PATH = os.path.join(DATA_PATH, "iqss/final/iqss.csv")

df_iqss_all = pd.read_csv(IQSS_PATH, sep=",")
print(df_iqss_all.shape)

# %%
df_iqss_all["finess"] = df_iqss_all["finess"].astype(str)
b = df_iqss_all["finess"].apply(len)
print(b.value_counts())

df_iqss_all["finess"] = df_iqss_all["finess"].apply(lambda x: x.zfill(9))

# %% [markdown]
# #### ISSUE : 43 colonnes vides :

# %%
nan = df_iqss_all.count()
nan = nan / len(df_iqss_all) * 100
nan = nan.sort_values()
print(nan)

sns.set(style="darkgrid", rc={"figure.figsize": (10, 200)})

fig = nan.plot(kind="barh", color="skyblue")

plt.title("data completeness", fontsize=15)

plt.show()

# %%
nan = nan[nan == 0]
print(len(nan))
nan

# %%
df_iqss_all.describe(include="all")

# %% [markdown]
# Peu de données en 2014 et 2019 :

# %%
sns.set(style="darkgrid", rc={"figure.figsize": (10, 7)})


df_iqss_all_clean = df_iqss_all.drop_duplicates(["finess", "annee"])

countplot = sns.countplot(x=("annee"), data=df_iqss_all_clean, color="skyblue")

countplot.set(xlabel="annee")
countplot.set(ylabel="nb finess")
plt.title("IQSS : nb etablissement différents par année", fontsize=20)
plt.show()

# %% [markdown]
# Valeurs manquantes IQSS : des données très sparses

# %%
sns.set(style="darkgrid", rc={"figure.figsize": (25, 12)})
df_iqss_all = df_iqss_all.sort_values(by=["annee", "finess"])
sns.heatmap(df_iqss_all.isnull(), cbar=False)

# %% [markdown]
# ### 1.3.4 Import CERTIF

# %%
CERTIF_PATH = os.path.join(DATA_PATH, "certification/final/certifications.csv")

df_certif = pd.read_csv(CERTIF_PATH, sep=",")
print(df_certif.shape)

# %%
df_certif.describe(include="all")

# %%
sns.set(style="darkgrid", rc={"figure.figsize": (8, 8)})
plt.scatter(df_certif["longitude"], df_certif["latitude"], s=1)
plt.ylim(41, 52)
plt.xlim(-6, 12)
plt.show()

# %% [markdown]
# ### 1.3.5 Import SAE

# %%
SAE_PATH = os.path.join(DATA_PATH, "sae/final/sae.csv")

df_sae = pd.read_csv(SAE_PATH, sep=",")
print(df_sae.shape)

# %%
df_sae.describe(include="all")

# %% [markdown]
# #### ISSUE : quelques colonnes vides

# %%
nan = df_sae.count()
nan = nan / len(df_sae) * 100
nan = nan.sort_values()
print(nan)

sns.set(style="darkgrid", rc={"figure.figsize": (10, 15)})

fig = nan.plot(kind="barh", color="skyblue")

plt.title("data completeness", fontsize=15)

plt.show()

# %%
sns.heatmap(df_sae.isnull(), cbar=False)

# %% [markdown]
# ## 1.3.6 Import ESATIS

# %%
ESATIS_PATH = os.path.join(DATA_PATH, "esatis/final/esatis.csv")

df_esatis = pd.read_csv(ESATIS_PATH, sep=",")
print(df_esatis.shape)

# %%
df_esatis.describe(include="all")

# %% [markdown]
# Pas de colonne vide

# %%
nan = df_esatis.count()
nan = nan / len(df_esatis) * 100
nan = nan.sort_values()
print(nan)

sns.set(style="darkgrid", rc={"figure.figsize": (10, 15)})

fig = nan.plot(kind="barh", color="skyblue")

plt.title("data completeness", fontsize=15)

plt.show()

# %%
sns.heatmap(df_esatis.isnull(), cbar=False)

# %% [markdown]
# # 2. Analayse des Finness nouvelle base <a class="anchor" id="2"></a>

# %% [markdown]
# ## 2.1 Table clé-valeur : nombres de finess geo/jur par année et par source <a class="anchor" id="2.1"></a>

# %% [markdown]
# ### 2.1.1 Nombre de finess geo/jur finess uniques dans la clé valeur

# %% [markdown]
# 9 613 finess uniques dans la clé valeur, 90% géographiques, 10% juridiques
#
# 40 finess de type 'inconnu'

# %%
a = len(np.unique(df_base_valeurs_meta["finess"].astype(str)).tolist())
print(f"Il y a {a} finess différents dans la clé valeur")

draw_distribution_stacked_bar(
    df_base_valeurs_meta.drop_duplicates(["finess"]), "finess_type"
)

# %% [markdown]
# ### 2.1.2. Finess geo/jur uniques par année

# %% [markdown]
# - Finess juridiques dans les années les plus anciennes
#
# - Beaucoup d'établissements en 2021, à cause des données de certification

# %%
fig, ax = draw_multiple_distribution_stacked_bars(
    df_base_valeurs.drop_duplicates(["finess", "annee"]),
    "annee",
    "finess_type",
)


# %% [markdown]
# ### 2.1.3 Finess geo/jur uniques par source
#
# Plus de finess uniques pour les données de certification : près de 9 000
#
# Les finess juridiques sont associés aux données IQSS

# %%
fig, ax = draw_multiple_distribution_stacked_bars(
    df_base_valeurs_meta.drop_duplicates(["finess", "source"]),
    "source",
    "finess_type",
)


# %% [markdown]
# #### Si on regarde source par source, on s'aperçoit que bcp d'établissement ne sont que dans certification
#
# 4 264 établissements que dans CERTIF

# %%
df = df_base_valeurs_meta.query('source != "Certification"')

a = len(np.unique(df_base_valeurs_meta["finess"].astype(str)).tolist()) - len(
    np.unique(df["finess"].astype(str)).tolist()
)
print(f"Il y a {a} finess différents avec uniquement des données CERTIF")


# %% [markdown]
# 1 076 finess que dans IQSS : les finess juridiques

# %%
df = df_base_valeurs_meta.query('source != "IQSS"')

a = len(np.unique(df_base_valeurs_meta["finess"].astype(str)).tolist()) - len(
    np.unique(df["finess"].astype(str)).tolist()
)
print(
    f"Il y a {a} finess différents dans la clé valeur avec uniquement des données IQSS"
)


# %% [markdown]
# En revanche peu d'établissements n'ont que des indicateurs SAE ou ESATIS

# %%
df = df_base_valeurs_meta.query('source != "SAE"')

a = len(np.unique(df_base_valeurs_meta["finess"].astype(str)).tolist()) - len(
    np.unique(df["finess"].astype(str)).tolist()
)
print(
    f"Il y a {a} finess différents dans la clé valeur avec uniquement des données SAE"
)


# %%
df = df_base_valeurs_meta.query('source != "e-Satis"')

a = len(np.unique(df_base_valeurs_meta["finess"].astype(str)).tolist()) - len(
    np.unique(df["finess"].astype(str)).tolist()
)
print(
    f"Il y a {a} finess différents dans la clé valeur avec uniquement des données ESATIS"
)


# %% [markdown]
# ## 2.2 Les établissements de la nouvelle base (table finess) sont-ils bien tous présents dans les données finess ? Ceux avec le flag bqss santé sont-ils bien tous présents dans les données finess 2020/21 ? <a class="anchor" id="2.2"></a>

# %% [markdown]
# ### 2.2.1 Les établissements de la nouvelle base (table finess) sont-ils bien tous présents dans les données finess (toutes années confondues) ?

# %% [markdown]
# Finess géographiques :
# 15 285 finess différents dans notre base (tous dans la base finess)
#
# 139 447 finess dans la base finess

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_finess,
    df_base_finess,
    "nofinesset",
    "nofinesset",
    "FINESS GEO BASE FINESS",
    "FINESS GEO TABLE FINESS BASE",
)
plt.title(
    "Comparaison des finess géographiques \n présents dans la table finess de la nouvelle base et dans la base finess",
    fontsize=20,
)

plt.show()

# %% [markdown]
# Finess juridiques : 3 172 différents dans notre base (tous dans la base finess)
#
# 73 445 finess dans la base finess

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_finess,
    df_base_finess,
    "nofinessej",
    "nofinessej",
    "FINESS JUR BASE FINESS",
    "FINESS JUR TABLE FINESS BASE",
)
plt.title(
    "Comparaison des finess juridiques \n présents dans la table finess de la nouvelle base et dans la base finess",
    fontsize=20,
)

plt.show()

# %% [markdown]
# ### 2.2.2 Les finess de notre base avec le flag bqss santé sont-ils bien tous présents dans les données finess 2020/21 ?

# %% [markdown]
# Finess géographiques : 12 147 différents tous dans la base finess 20/21
#
# 97 215 finess dans la base finess 2020/21

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_finess.query(
        'dateexport == "2021-07-09" or dateexport == "2020-12-31"'
    ),
    df_base_finess_bqss,
    "nofinesset",
    "nofinesset",
    "FINESS GEO BASE FINESS 2020/21",
    "FINESS GEO BASE BQSS",
)
plt.title(
    "Comparaison des finess géographiques \n présents dans la nouvelle base BQSS et dans la base finess 2020/21",
    fontsize=20,
)

plt.show()

# %% [markdown]
# Finess juridiques : 2 244 différents tous dans la base finess 20/21
#
# 48 126 finess dans la base finess 2020/21

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_finess.query(
        'dateexport == "2021-07-09" or dateexport == "2020-12-31"'
    ),
    df_base_finess_bqss,
    "nofinessej",
    "nofinessej",
    "FINESS JUR BASE FINESS 2020/21",
    "FINESS JUR BASE BQSS",
)
plt.title(
    "Comparaison des finess juridiques \n présents dans la nouvelle base BQSS et dans la base finess 2020/21",
    fontsize=20,
)

plt.show()

# %% [markdown]
# ## 2.3 Les finess de la table cle-valeur sont-ils bien dans la table finess de la base ? <a class="anchor" id="2.3"></a>

# %% [markdown]
# ### 2.3.1 : juridiques et géo ensemble

# %% [markdown]
# Ils y sont bien tous, sauf 40 (ceux de type 'inconnus')

# %%
compare_venn_3(
    df_base_finess,
    df_base_valeurs,
    df_base_finess,
    "nofinesset",
    "finess",
    "nofinessej",
    "TABLE FINESS GEO",
    "TABLE CLE-VALEUR",
    "TABLE FINESS JUR",
)

# %% [markdown]
# Si on prend ceux avec le flag actif_qualiscope, il en manque bien plus : 699
#
# Nous allons voir pourquoi de façon séparée géographique/juridique dans les pragaraphes suivants

# %%
compare_venn_3(
    df_base_finess_scope,
    df_base_valeurs,
    df_base_finess_scope,
    "nofinesset",
    "finess",
    "nofinessej",
    "TABLE FINESS GEO BQSS",
    "TABLE CLE-VALEUR",
    "TABLE FINESS JUR BQSS",
)

# %% [markdown]
# ### 2.3.2 Pour les finess géo
#
# Tous les finess de la clé-valeur sont bien dans la table finess

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_base_valeurs.query('finess_type == "geo"'),
    df_base_finess,
    "finess",
    "nofinesset",
    "FINESS GEO TABLE CLE VALEUR",
    "FINESS GEO TABLE FINESS",
)
plt.title(
    "Comparaison des finess geo \n présents dans la table cla-valeur et la table finess de nouvelle base",
    fontsize=20,
)

plt.show()

# %% [markdown]
# Si maintenant on ne considère que les établissements de la table finess avec le flag 'actif_qualiscope'
#
# 548 finess ont des indicateurs mais ne sont pas dans la table finess base BQSS

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_base_valeurs.query('finess_type == "geo"'),
    df_base_finess_scope,
    "finess",
    "nofinesset",
    "FINESS GEO TABLE CLE VALEUR",
    "FINESS GEO TABLE FINESS BASE BQSS",
)
plt.title(
    "Comparaison des finess geo \n présents dans la table cla-valeur et la table finess de nouvelle base BQSS",
    fontsize=20,
)

plt.show()

# %% [markdown]
# Cela s'explique car ils sont absents de la base finess 2020/21 (donc normal)

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn_3(
    df_base_valeurs.query('finess_type == "geo"'),
    df_base_finess_scope,
    df_finess.query(
        'dateexport == "2021-07-09" or dateexport == "2020-12-31"'
    ),
    "finess",
    "nofinesset",
    "nofinesset",
    "FINESS GEO TABLE CLE VALEUR",
    "FINESS GEO TABLE FINESS",
    "FINESS GEO BASE FINESS 2020/21",
)

# %% [markdown]
# ### 2.3.3 Pour les finess juridiques
#
# Tous les finess de la clé-valeur sont bien dans la table finess

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_base_valeurs.query('finess_type == "jur"'),
    df_base_finess,
    "finess",
    "nofinessej",
    "FINESS JUR TABLE CLE VALEUR",
    "FINESS JUR TABLE FINESS",
)
plt.title(
    "Comparaison des finess juridiques \n présents dans la table cla-valeur et la table finess de nouvelle base",
    fontsize=20,
)

plt.show()

# %% [markdown]
# Si maintenant on ne considère que les établissements de la table finess avec le flag 'actif_qualiscope'
#
# 111 finess ont des indicateurs mais ne sont pas dans la table finess base BQSS

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_base_valeurs.query('finess_type == "jur"'),
    df_base_finess_scope,
    "finess",
    "nofinessej",
    "FINESS JUR TABLE CLE VALEUR",
    "FINESS JUR TABLE FINESS",
)
plt.title(
    "Comparaison des finess juridiques \n présents dans la table cla-valeur et la table finess de nouvelle base BQSS",
    fontsize=20,
)

plt.show()

# %% [markdown]
# Pour 103 d'entre eux, cela s'explique car ils sont absents de la base finess 20/21 (donc normal)

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn_3(
    df_base_valeurs.query('finess_type == "jur"'),
    df_base_finess_scope,
    df_finess.query(
        'dateexport == "2021-07-09" or dateexport == "2020-12-31"'
    ),
    "finess",
    "nofinessej",
    "nofinessej",
    "FINESS JUR TABLE CLE VALEUR",
    "FINESS JUR TABLE FINESS",
    "FINESS JUR BASE FINESS 2020/21",
)

# %% [markdown]
# Mais 8 finess juridiques avec un indicateur, et présents dans la base finess 2020/21 absents de la base BQSS

# %%
no_key = np.unique(
    df_base_valeurs.query('finess_type == "jur"')["finess"]
).tolist()

no_base_finess = np.unique(
    df_base_finess_scope["nofinessej"].astype(str)
).tolist()

no_finess = np.unique(
    df_finess.query(
        'dateexport == "2021-07-09" or dateexport == "2020-12-31"'
    )["nofinessej"].astype(str)
).tolist()

missing_2020 = [
    x for x in no_key if x not in no_base_finess and x in no_finess
]
print(
    f"Il y a {len(missing_2020)} établissement présents dans la clé valeur et finess 2020/21 mais pas dans notre base"
)
print(missing_2020)

# %% [markdown]
# #### Raison : changement de finess juridique
#
# 8 établissements (fines juridiques) sont présents dans la clé-valeur avec des indicateurs (souvent IQSS, entre 2013 et 2017). Ces établissements sont aussi présents dans les données finess du 31-12-2020.
# Ces 2 conditions devraient suffire à ce que ces établissement obtiennent le flag 'actif_qualiscope'.
# Or ils sont bien présents dans la table fines mais sans ce flag.
#
#
# Le(les) fines géographiques correspondant à ces 8 finess juridiques ont changé de finess juridique entre 2020 et 2021. Or seule la ligne la plus récente de chaque finess géographique peut obtenir le flag. La ligne 2021 avec le nouveau finess juridique obtient donc le flag, et le 'vieux' finess juridique (ligne 2020) ne l'obtient pas.

# %% [markdown]
#  #### ex : finess juridique 030180020
# On a des indicateurs dans la table clé valeur entre 2013 et 2017
# et l'établissement est bien présent dans la table finess (données 2020) associé avec les finess geo 030000012 (qui a aussi des indicateurs) et 030785216
#  #### MAIS les finess géo 030000012 et 030785216 sont maintenant associé au finess jur 030780100 (2021)

# %% [markdown]
# ## 2.4 Les établissements des bases avant aggrégation sont-ils bien dans la nouvelle base ? <a class="anchor" id="2.4"></a>

# %% [markdown]
# ### 2.4.1 IQSS vs Nouvelle Base <a class="anchor" id="2.4"></a>

# %% [markdown]
# #### Complétude clé-valeur
#
# Tout est bien dans la clé-valeur

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_base_valeurs,
    df_iqss_all,
    "finess",
    "finess",
    "FINESS TABLE CLE VALEUR",
    "FINESS IQSS",
)
plt.title(
    "Comparaison des finess \n présents dans la table clé-valeur et dans la base IQSS",
    fontsize=20,
)

plt.show()

# %% [markdown]
# #### Complétude table finess

# %%
compare_venn_3(
    df_base_finess,
    df_iqss_all,
    df_base_finess,
    "nofinesset",
    "finess",
    "nofinessej",
    "TABLE FINESS GEO",
    "BASE IQSS",
    "TABLE FINESS JUR",
)

# %% [markdown]
# 7 établissements dans IQSS et pas dans la table finess de la nouvelle base
#
# Explication : ils sont absents dans la base FINESS aussi
#
# Ce sont 7 finess de type 'na'. Ils sont bien dans la clé valeur mais pas dans la table finess

# %%
no_base_finesset = np.unique(df_base_finess["nofinesset"].astype(str)).tolist()

no_base_finessej = np.unique(df_base_finess["nofinessej"].astype(str)).tolist()

no_iqss = np.unique(df_iqss_all["finess"].astype(str)).tolist()

no_finesset = np.unique(df_finess["nofinesset"].astype(str)).tolist()

no_finessej = np.unique(df_finess["nofinessej"].astype(str)).tolist()

missing_iqss = [
    x
    for x in no_iqss
    if (x not in no_base_finesset and x not in no_base_finessej)
]
print(
    f"Il y a {len(missing_iqss)} établissement présents dans IQSS mais pas dans notre base"
)

print(missing_iqss)

missing_iqss = [
    x
    for x in no_iqss
    if (x not in no_base_finesset and x not in no_base_finessej)
    and (x in no_finesset or x in no_finessej)
]
print(
    f"Il y a {len(missing_iqss)} établissement présents dans IQSS et Finess mais pas dans notre base"
)

# %% [markdown]
# ### 2.4.2 CERTIF/Nouvelle Base

# %% [markdown]
# #### Complétude clé-valeur
#
# Tout est bien dans la clé-valeur

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_base_valeurs,
    df_certif,
    "finess",
    "finess",
    "FINESS TABLE CLE VALEUR",
    "FINESS CERTIF",
)
plt.title(
    "Comparaison des finess juridiques \n présents dans la table cla-valeur et la table finess de nouvelle base",
    fontsize=20,
)

plt.show()

# %% [markdown]
# #### Complétude table finess

# %%
compare_venn_3(
    df_base_finess,
    df_certif,
    df_base_finess,
    "nofinesset",
    "finess",
    "nofinessej",
    "TABLE FINESS GEO",
    "CERTIF",
    "TABLE FINESS JUR",
)

# %% [markdown]
# Par contre 33 établissements dans CERTIF et pas dans la nouvelle BASE : TABLE FINESS
#
# Explication : ils sont absents la base FINESS
#
# Ce sont encore des finess de type 'na', présents dans la clé-valeur mais pas dans la table finess

# %%
no_base_finesset = np.unique(df_base_finess["nofinesset"].astype(str)).tolist()

no_base_finessej = np.unique(df_base_finess["nofinessej"].astype(str)).tolist()

no_certif = np.unique(df_certif["finess"].astype(str)).tolist()

no_finesset = np.unique(df_finess["nofinesset"].astype(str)).tolist()

no_finessej = np.unique(df_finess["nofinessej"].astype(str)).tolist()

missing_certif = [
    x
    for x in no_certif
    if (x not in no_base_finesset and x not in no_base_finessej)
]
print(
    f"Il y a {len(missing_certif)} établissement présents dans CERTIF mais pas dans notre base"
)

missing_certif = [
    x
    for x in no_certif
    if (x not in no_base_finesset and x not in no_base_finessej)
    and (x in no_finesset or x in no_finessej)
]
print(
    f"Il y a {len(missing_certif)} établissement présents dans CERTIF et Finess mais pas dans notre base"
)

# %% [markdown]
# ### 2.4.3 ESATIS/Nouvelle Base

# %% [markdown]
# Tout les finess de la base ESATIS sont dans la table valeur et aussi dans la table finess de la nouvelle base

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_base_valeurs,
    df_esatis,
    "finess",
    "finess_geo",
    "FINESS TABLE CLE VALEUR",
    "FINESS ESATIS",
)


plt.show()

# %%
compare_venn_3(
    df_base_finess,
    df_esatis,
    df_base_finess,
    "nofinesset",
    "finess_geo",
    "nofinessej",
    "TABLE FINESS GEO",
    "ESATIS",
    "TABLE FINESS JUR",
)

# %% [markdown]
# ### 2.4.4. SAE vs Nouvelle base

# %% [markdown]
# #### Complétude clé-valeur
#
# il manque 9 établissements
#
# Explication : ce sont des établissements avec aucune valeur, que des NA dans la table SAE

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_base_valeurs,
    df_sae,
    "finess",
    "id_fi",
    "FINESS TABLE CLE VALEUR",
    "FINESS SAE",
)


plt.show()

# %% [markdown]
# #### Complétude table finess

# %%
compare_venn_3(
    df_base_finess,
    df_sae,
    df_base_finess,
    "nofinesset",
    "id_fi",
    "nofinessej",
    "TABLE FINESS GEO",
    "SAE",
    "TABLE FINESS JUR",
)

# %% [markdown]
# 7 établissements dans SAE et pas dans la nouvelle base
#
# Il sont pourtant présents dans la base FINESS
#
# Explication : pas dans la clé-valeur non plus (que des NA) donc ont ne les a pas intégré

# %%
no_base_finesset = np.unique(df_base_finess["nofinesset"].astype(str)).tolist()

no_base_finessej = np.unique(df_base_finess["nofinessej"].astype(str)).tolist()

no_sae = np.unique(df_sae["id_fi"].astype(str)).tolist()

no_finesset = np.unique(df_finess["nofinesset"].astype(str)).tolist()

no_finessej = np.unique(df_finess["nofinessej"].astype(str)).tolist()

missing_sae = [
    x
    for x in no_sae
    if (x not in no_base_finesset and x not in no_base_finessej)
]
print(
    f"Il y a {len(missing_sae)} établissement présents dans SAE mais pas dans notre base"
)

missing_sae = [
    x
    for x in no_sae
    if (x not in no_base_finesset and x not in no_base_finessej)
    and (x in no_finesset or x in no_finessej)
]
print(
    f"Il y a {len(missing_sae)} établissement présents dans SAE et Finess mais pas dans notre base"
)

print(missing_sae)

# %% [markdown]
# ### 2.4.5 Comparaison finess geo  ESATIS IQSS CERTIF

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn_3(
    df_iqss_all.query("annee == 2018 or annee == 2019"),
    df_esatis,
    df_certif,
    "finess",
    "finess_geo",
    "finess",
    "IQSS",
    "ESATIS",
    "CERTIF",
)
v.get_patch_by_id("100").set_color("pink")
# v.get_patch_by_id('010').set_color('skyblue')
# v.get_patch_by_id('110').set_color('purple')
v.get_patch_by_id("111").set_color("beige")
plt.title(
    "Etablissements présents dans IQSS 2019-20, \n CERTIF et ESATIS 2020",
    fontsize=20,
)
plt.savefig("Etablissements présents dans IQSS 2019-20, CERTIF et ESATIS 2020")
plt.show()

# %% [markdown]
# ## 2.5 : Table autorisations <a class="anchor" id="2.5"></a>

# %% [markdown]
# ### 2.5.1 : Table autorisations vs Table finess <a class="anchor" id="2.5.1"></a>

# %% [markdown]
# Tous les établissements autorisés ne sont pas dans la table finess certaines autorisations correspondent à des domaines d'activité non pris en compte

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_base_autorisations,
    df_base_finess,
    "nofinesset",
    "nofinesset",
    "FINESS GEO TABLE AUTORISATIONS",
    "FINESS GEO TABLE FINESS",
)
plt.title(
    "Comparaison des finess geo \n présents dans la table autorisations et la table finess de nouvelle base",
    fontsize=20,
)

plt.show()

# %% [markdown]
# ### 2.5.2 Table autorisations vs Table finess vs Indicateurs

# %% [markdown]
# Les établissement de la table "clé-valeur" sont bien tous inclus dans la table "finess", au contraire de ceux de la table "autorisations"
#
# Beaucoup d'établissement de la table finess ne sont dans aucune des 2 tables "autorisations" et "clé-valeur"
#
# Raison : on récupère tous les finess géo correspondant aux finess juridiques que l'on veut (ceux présents dans la table clé-valeur : données IQSS), donc on en récupère bcp qui ne sont dans aucune des 2 tables

# %%
compare_venn_3(
    df_base_finess,
    df_base_autorisations,
    df_base_valeurs.query('finess_type == "geo"'),
    "nofinesset",
    "nofinesset",
    "finess",
    "TABLE FINESS GEO",
    "AUTORISATIONS GEO",
    "VALEURS GEO",
)

# %% [markdown]
# Pour les finess juridiques : peu d'établissements ne sont dans aucune des 2 tables
#
# Les finess géographiques 'en trop' on bien été récupér2s lors de la 'redscente' des finess juridiques vers géographiques

# %%
compare_venn_3(
    df_base_finess,
    df_base_autorisations,
    df_base_valeurs.query('finess_type == "jur"'),
    "nofinessej",
    "nofinessej",
    "finess",
    "TABLE FINESS JUR",
    "AUTORISATIONS JUR",
    "VALEURS JUR",
)

# %% [markdown]
# # 3. Analyse des Finess : comparaison base BQSS avec ATIH <a class="anchor" id="3"></a>

# %% [markdown]
# ## 3.1 ATIH vs Nouvelles base <a class="anchor" id="3.1"></a>

# %% [markdown]
# ### 3.1.1 : ATIH vs Nouvelle base : table finess 'entière'
#
# Les finess de la base ATIH sont géographiques
#
# Il nous manque 1 seul établissement de ATIH dans notre nouvelle base

# %%
compare_venn_3(
    df_base_finess,
    df_atih_finess,
    df_base_finess,
    "nofinesset",
    "finess",
    "nofinessej",
    "TABLE FINESS GEO",
    "ATIH",
    "TABLE FINESS JUR",
)

# %% [markdown]
# ### 3.1.2 : ATIH vs Nouvelle base : table finess avec le flag actif_qualiscope

# %% [markdown]
# Il nous manque 75 finess de ATIH dans notre base avec le flag actif_qualiscope

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn(
    df_atih_finess,
    df_base_finess_scope,
    "finess",
    "nofinesset",
    "ATIH",
    "NOUVELLE BASE BQSS",
)
plt.title(
    "Comparaison des établissement (numéros finess) \n présents dans la base ATIH et la nouvelle base",
    fontsize=20,
)
plt.savefig(
    "Comparaison des établissement présents dans la base ATIH et la nouvelle base.png"
)
plt.show()

# %% [markdown]
# ## 3.2 Pourquoi y a-t-il 75 établissements dans ATIH et absents de notre base ? <a class="anchor" id="3.2"></a>

# %% [markdown]
# ### 3.2.1 Explication numéro 1 : on a considéré que les établissements présents à N et N-1 dans finess (2020/21)

# %% [markdown]
# Notre nouvelle base contient seulement des établissements présents dans les fichiers finess exportés en 2020 ou en 2021 : (cf partie 2.2)

# %%
compare_venn(
    df_base_finess_scope,
    df_finess.query(
        'dateexport == "2021-07-09" or dateexport == "2020-12-31"'
    ),
    "nofinesset",
    "nofinesset",
)

# %% [markdown]
# Alors que, en comparant ATIH à finess 2020/2021, on remarque que 74 établissement de ATIH ne sont pas dans FINESS 2020/21

# %%
fig, ax = plt.subplots(figsize=(10, 10))
compare_venn(
    df_atih_finess,
    df_finess.query(
        'dateexport == "2021-07-09" or dateexport == "2020-12-31"'
    ),
    "finess",
    "nofinesset",
)
plt.show()

# %% [markdown]
# Pour récupérer tous les finess ATIH avec la base FINESS, il faut remonter aux finess exportés le 12-31-2016 :

# %%
fig, ax = plt.subplots(figsize=(10, 10))
compare_venn(
    df_atih_finess,
    df_finess.query(
        'dateexport == "2021-07-09" or dateexport == "2020-12-31" or dateexport == "2019-12-31" or dateexport == "2018-12-31" or dateexport == "2017-12-31" or dateexport == "2016-12-31"'
    ),
    "finess",
    "nofinesset",
)
plt.show()

# %% [markdown]
# ### > CCL : Il nous manque 74 établissements par rapport à ATIH que nous avons éliminé car établissements pas présents dans les reccueil finess 2020 et 2021

# %% [markdown]
# ### 3.2.2 : 1 autre établissement manquant

# %%
no_atih = np.unique(df_atih_finess["finess"]).tolist()

no_base_finess_scope = np.unique(
    df_base_finess_scope["nofinesset"].astype(str)
).tolist()

no_base_finess = np.unique(df_base_finess["nofinesset"].astype(str)).tolist()

no_finess = np.unique(
    df_finess.query(
        'dateexport == "2021-07-09" or dateexport == "2020-12-31"'
    )["nofinesset"].astype(str)
).tolist()


missing_2020 = [x for x in no_atih if x not in no_base_finess_scope]
print(
    f"Il y a {len(missing_2020)} établissement présents dans ATIH mais pas dans notre base BQSS"
)

missing_2020 = [x for x in no_atih if x not in no_base_finess]
print(
    f"Il y a {len(missing_2020)} établissement présents dans ATIH mais pas dans notre base tout court"
)
print(missing_2020)

missing_2020 = [
    x for x in no_atih if x not in no_base_finess and x in no_finess
]
print(
    f"Il y a {len(missing_2020)} établissement présents dans ATIH et finess 2020/21 mais pas dans notre base"
)
print(missing_2020)
extra_2020 = [x for x in no_base_finess if x not in no_atih and x in no_finess]
print(
    f"Il y a {len(extra_2020)} établissement présents dans notre base mais pas dans ATIH"
)


# %% [markdown]
# Il s'agit de l'établissement 680021680
#
# Il n'a pas d'autorisation ni d'indicateur

# %% [markdown]
# ## 3.3 Pourquoi y a-t-il plus de 8000+ établissements dans notre base et absents dans ATIH ? <a class="anchor" id="3.3"></a>

# %% [markdown]
# ### 3.3.1 Explication 1 : données de certification (table CLE-VALEUR)

# %% [markdown]
# Plus de 4000 établissement sont dans CERTIF et pas dans ATIH
#
# Ils sont quasiement tous dans notre base puisqu'on a des valeurs dans la table clé-valeur

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn_3(
    df_base_finess_scope,
    df_atih_finess,
    df_certif,
    "nofinesset",
    "finess",
    "finess",
    "BASE BQSS",
    "ATIH",
    "CERTIF",
)
v.get_patch_by_id("100").set_color("pink")
# v.get_patch_by_id('010').set_color('skyblue')
# v.get_patch_by_id('110').set_color('purple')
v.get_patch_by_id("111").set_color("beige")
plt.title(
    "Etablissements présents dans la nouvelle BASE, ATIH et CERTIF",
    fontsize=20,
)

plt.show()

# %% [markdown]
# Les établissements correspondants aux autres sources (IQSS, SAE et ESATIS) sont presque tous déjà inclus dans ATIH

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn_3(
    df_base_finess_scope,
    df_atih_finess,
    df_sae,
    "nofinesset",
    "finess",
    "id_fi",
    "BASE BQSS",
    "ATIH",
    "SAE",
)
v.get_patch_by_id("100").set_color("pink")
# v.get_patch_by_id('010').set_color('skyblue')
# v.get_patch_by_id('110').set_color('purple')
v.get_patch_by_id("111").set_color("beige")
plt.title(
    "Etablissements présents dans la nouvelle BASE, ATIH et SAE",
    fontsize=20,
)

plt.show()

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn_3(
    df_base_finess_scope,
    df_atih_finess,
    df_iqss_all.query("annee == 2018 or annee == 2019"),
    "nofinesset",
    "finess",
    "finess",
    "BASE BQSS",
    "ATIH",
    "IQSS",
)
v.get_patch_by_id("100").set_color("pink")
# v.get_patch_by_id('010').set_color('skyblue')
# v.get_patch_by_id('110').set_color('purple')
v.get_patch_by_id("111").set_color("beige")
plt.title(
    "Etablissements présents dans la nouvelle BASE, ATIH et IQSS",
    fontsize=20,
)

plt.show()

# %%
fig, ax = plt.subplots(figsize=(10, 10))
v = compare_venn_3(
    df_base_finess_scope,
    df_atih_finess,
    df_esatis,
    "nofinesset",
    "finess",
    "finess_geo",
    "BASE BQSS",
    "ATIH",
    "ESATIS",
)
v.get_patch_by_id("100").set_color("pink")
# v.get_patch_by_id('010').set_color('skyblue')
# v.get_patch_by_id('110').set_color('purple')
v.get_patch_by_id("111").set_color("beige")
plt.title(
    "Etablissements présents dans la nouvelle BASE, ATIH et ESATIS",
    fontsize=20,
)

plt.show()

# %% [markdown]
# ### 3.3.2 Explication 2 : table autorisations (activité 4 et 7)

# %%
no_atih = np.unique(df_atih_finess["finess"]).tolist()

no_base_finess = np.unique(
    df_base_finess_scope["nofinesset"].astype(str)
).tolist()

no_certif = np.unique(df_certif["finess"].astype(str)).tolist()

no_valeurs = np.unique(
    df_base_valeurs.query('finess_type == "geo"')["finess"].astype(str)
).tolist()


missing_2020 = [
    x for x in no_atih if x not in no_base_finess and x in no_finess
]
print(
    f"Il y a {len(missing_2020)} établissement présents dans ATIH et finess 2020/21 mais pas dans notre base"
)
print(missing_2020)

extra_2020 = [x for x in no_base_finess if x not in no_atih]
print(
    f"Il y a {len(extra_2020)} établissement présents dans notre base mais pas dans ATIH"
)

extra_2020_2 = [
    x for x in no_base_finess if (x not in no_atih and x not in no_valeurs)
]
print(
    f"Il y a {len(extra_2020_2)} établissement présents dans notre base mais pas dans ATIH ni dans la clé-valeur (CERTIF et autres sources)"
)

# %% [markdown]
# Parmi les 8000 établissements qu'on a en plus que ATIH, 2000 ont été intégrés via la table autorisations et leur activité (4 ou 7)

# %%
df = df_base_autorisations[
    df_base_autorisations["nofinesset"].isin(extra_2020)
]

df = df.query('datefin > "2021-01-01"')
print(len(df))

df = df[["nofinesset", "activite", "libactivite"]].drop_duplicates()
print(len(df))
print(df["activite"].value_counts())

countplot = sns.countplot(x=("activite"), data=df, color="skyblue")

countplot.set(xlabel="activite")
countplot.set(ylabel="nb etablissements")
plt.title(
    "Activité des etablissements présents dans la nouvelle base \n mais pas dans ATIH",
    fontsize=20,
)
plt.savefig("Etablissements présents dans notre base mais pas dans ATIH")
plt.show()

# %% [markdown]
# Cependant, quand on considère les 4000 établissements n'étant ni dans ATIH ni dans CERTIF,
#
# Seuls 200 on été intégrés via la table autorsation (actéivités 4 et 7) : "redondance" de l'effet avec CERTIF

# %%
df = df_base_autorisations[
    df_base_autorisations["nofinesset"].isin(extra_2020_2)
]

df = df.query('datefin > "2021-01-01"')
print(len(df))

df = df[["nofinesset", "activite", "libactivite"]].drop_duplicates()
print(len(df))
print(df["activite"].value_counts())

countplot = sns.countplot(x=("activite"), data=df, color="skyblue")

countplot.set(xlabel="activite")
countplot.set(ylabel="nb etablissements")
plt.title(
    "Activité des etablissements présents dans la nouvelle base \n mais pas dans ATIH",
    fontsize=20,
)
plt.savefig("Etablissements présents dans notre base mais pas dans ATIH")
plt.show()

# %%
no_auto = np.unique(df_base_autorisations["nofinesset"].astype(str)).tolist()


no_atih = np.unique(df_atih_finess["finess"]).tolist()

no_base_finess = np.unique(
    df_base_finess_scope["nofinesset"].astype(str)
).tolist()

no_certif = np.unique(df_certif["finess"].astype(str)).tolist()

no_valeurs = np.unique(
    df_base_valeurs.query('finess_type == "geo"')["finess"].astype(str)
).tolist()


missing_2020 = [
    x for x in no_atih if x not in no_base_finess and x in no_finess
]
print(
    f"Il y a {len(missing_2020)} établissement présents dans ATIH et finess 2020/21 mais pas dans notre base"
)
print(missing_2020)

extra_2020 = [x for x in no_base_finess if x not in no_atih]
print(
    f"Il y a {len(extra_2020)} établissement présents dans notre base mais pas dans ATIH"
)

extra_2020_2 = [
    x for x in no_base_finess if (x not in no_atih and x not in no_valeurs)
]
print(
    f"Il y a {len(extra_2020_2)} établissement présents dans notre base mais pas dans ATIH ni dans la clé-valeur (CERTIF et autres sources)"
)
extra_2020_3 = [
    x
    for x in no_base_finess
    if (x not in no_atih and x not in no_valeurs and x not in no_auto)
]
print(
    f"Il y a {len(extra_2020_3)} établissement présents dans notre base mais pas dans ATIH ni dans la clé-valeur (CERTIF et autres sources) ni dans AUTORISATION"
)

# %% [markdown]
# ### 3.3.3 : Il reste donc 3757 établissement de la base BQSS qui ne sont ni dans ATIH,  ni dans la table AUTORISATIONS (activités 4 et 7), ni dans la table CLE-VALEUR (données CERTIF et autres)
#
# En fait, ils sont été intégrés via leur finess juridique.
#
# Quand un indicateur est présent au niveau juridique (certains indictateurs IQSS 2013 à 2017), on récupère toutes les lignes correspondantes dans la base FINESS.
#
# On récupère donc tous les FINESS géographiques associés aux FINESS juridiques.

# %% [markdown]
# # 4 Analyse des indicateurs <a class="anchor" id="4"></a>

# %% [markdown]
# ## 4.1 Analyse générale des indictateurs <a class="anchor" id="4.1"></a>
#
# ### 4.1.1 Cohérence Table de métadonnées et Table clé-valeur
#
# Toutes les variables de la clé valeur sont bien dans la table de métadonnées
#
# L'inverse n'est pas vrai : 136 variables n'ont pas de lignes dans la table clé-valeur

# %%
compare_venn(
    df_base_valeurs,
    df_base_meta,
    "key",
    "name",
    "Table clé-valeur",
    "Table metadonnées",
)

# %% [markdown]
# ###  4.1.2 Sources de données de la table clé-valeur

# %%
sns.countplot(x="source", data=df_base_valeurs_meta.drop_duplicates("key"))

# %% [markdown]
# Par année

# %%
fig, ax = draw_multiple_distribution_stacked_bars(
    df_base_valeurs_meta, "annee", "source"
)


# %% [markdown]
# Par type de variable

# %%
fig, ax = draw_multiple_distribution_stacked_bars(
    df_base_valeurs_meta.drop_duplicates("key"), "type", "source"
)

# %% [markdown]
# ## 4.2 Indicateurs numériques <a class="anchor" id="4.2"></a>

# %% [markdown]
# ### 4.2.1 Création d'une colonne contenant value_integer et value_float

# %%
df_base_valeurs_meta["value_num"] = df_base_valeurs_meta.apply(
    lambda x: x["value_integer"]
    if pd.isna(x["value_integer"]) == False
    else (x["value_float"] if pd.isna(x["value_float"]) == False else "NA"),
    axis=1,
)

df_base_valeurs_meta["value_num"] = pd.to_numeric(
    df_base_valeurs_meta["value_num"], errors="coerce"
)

print(len(df_base_valeurs_meta))

# %% [markdown]
# ### 4.2.2 Sélection lignes de la clé-valeur avec valeur numérique

# %%
df_num = df_base_valeurs_meta.query("value_num >= 0")
print(len(df_num))

# %% [markdown]
# Plus de 400 000 lignes sont relatives à des données numériques (sur 1 million de lignes au total)
#
#
# Il s'agit de données provenant des 4 sources différentes

# %%
fig, ax = draw_multiple_distribution_stacked_bars(df_num, "annee", "source")

# %% [markdown]
# ### 4.2.3 Table pivot tabulaire avec une ligne par finess/année et une colonne par variable numérique + plot

# %%
df_num["finess_year"] = df_num["finess"] + df_num["annee"].astype("str")

df_num_tab = df_num.pivot(
    index=["finess_year"], columns="key", values="value_num"
)
print(df_num_tab.shape)
df_num_tab.describe(include="all")
df_num_tab["finess"] = df_num_tab.index

# %%
n_rows = 6
n_cols = 5
# Create the subplots
fig, axes = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(15, 30))


for i, column in enumerate(df_num_tab.columns[0:30]):
    sns.histplot(
        df_num_tab[column],
        ax=axes[i // n_cols, i % n_cols],
        kde=True,
    )

# %% [markdown]
# ### 4.2.4 Describe des colonnes et ajout des métadonnées

# %%
table = df_num_tab.describe(include="all").transpose()
table = table.drop(["top", "freq", "unique"], axis=1)
table = table.drop("finess", axis=0)

# %%
table_meta = table.merge(
    df_base_meta, left_on="key", right_on="name", how="inner"
)

table_meta = table_meta.drop(["description", "type", "theme"], axis=1)

# %%
sns.set(style="darkgrid", rc={"figure.figsize": (10, 10)})

sns.countplot(x="source", data=table_meta)

# %%
table_meta

# %% [markdown]
# ## 4.3 : indicateurs boolléens <a class="anchor" id="4.3"></a>

# %% [markdown]
# ### 4.3.1 Sélection lignes de la clé-valeur

# %% [markdown]
# Près de 400 000 lignes sont relatives à des données booléennes (sur 1 million de lignes au total)
#
# Il s'agit de données SAE exclusivement
#
# 38 indicateurs

# %%
df_boolean = df_base_valeurs_meta[df_base_valeurs_meta.value_boolean.notna()]
print(len(df_boolean))

# %%
fig, ax = draw_multiple_distribution_stacked_bars(
    df_boolean, "annee", "source"
)

# %% [markdown]
# ### 4.3.2 Table tabulaire pivot avec une ligne par finess/année et une colonne par variable + plot

# %%
df_boolean["finess_year"] = df_boolean["finess"] + df_boolean["annee"].astype(
    "str"
)

df_boolean_tab = df_boolean.pivot(
    index=["finess_year"], columns="key", values="value_boolean"
)
print(df_boolean_tab.shape)
df_boolean_tab["finess"] = df_boolean_tab.index

# %%
plot_distribution_all_variables(df_boolean_tab)

# %% [markdown]
# ### 4.3.3 Describe des colonnes et ajout des métadonnées, calcul des taux de TRUE et FALSE

# %%
table_bool = df_boolean_tab.describe(include="all").transpose()

table_bool = table_bool.drop("finess", axis=0)

table_bool_meta = table_bool.merge(
    df_base_meta, left_on="key", right_on="name", how="inner"
)
table_bool_meta = table_bool_meta.drop(
    ["secteur", "theme", "type", "description"], axis=1
)

# %%
table_bool_meta["TRUE"] = table_bool_meta.apply(
    lambda x: x["freq"] if (x["top"] == True) else x["count"] - x["freq"],
    axis=1,
)
table_bool_meta["TRUE_%"] = (
    table_bool_meta["TRUE"] / table_bool_meta["count"] * 100
)
table_bool_meta["TRUE_%"] = table_bool_meta["TRUE_%"].apply(lambda x: round(x))

table_bool_meta["FALSE"] = table_bool_meta["count"] - table_bool_meta["TRUE"]
table_bool_meta["FALSE_%"] = (
    table_bool_meta["FALSE"] / table_bool_meta["count"] * 100
)
table_bool_meta["FALSE_%"] = table_bool_meta["FALSE_%"].apply(
    lambda x: round(x)
)

table_bool_meta = table_bool_meta.drop(["unique", "top", "freq"], axis=1)

# %%
table_bool_meta

# %% [markdown]
# ## 4.4 Indicateurs catégoriels <a class="anchor" id="4.4"></a>

# %% [markdown]
# ### 4.4.1 Sélection lignes

# %% [markdown]
# Près de 200 000 lignes relatives à des variables catégorielles (sur 1 million de lignes au total)
#
# Il s'agit surtout de données IQSS/CERTIF

# %%
df_cat = df_base_valeurs_meta[df_base_valeurs_meta.value_string.notna()]
print(len(df_cat))

# %%
fig, ax = draw_multiple_distribution_stacked_bars(df_cat, "annee", "source")

# %% [markdown]
# ### 4.4.2 Table pivot avec une ligne par finess/année, 1 colonne par variable + plot

# %%
df_cat["finess_year"] = df_cat["finess"].astype("str") + df_cat[
    "annee"
].astype("str")

df_cat_tab = df_cat.pivot(
    index=["finess_year"], columns="key", values="value_string"
)
print(df_cat_tab.shape)
df_cat_tab["finess"] = df_cat_tab.index

# %%
df_cat_tab

# %%
plot_distribution_all_variables(df_boolean_tab)

# %%
sns.set(style="darkgrid", rc={"figure.figsize": (10, 10)})

sns.countplot(x="source", data=table_meta)

# %% [markdown]
# ## 4.5 Cohérence entre indicateurs <a class="anchor" id="4.5"></a>

# %% [markdown]
# ### 4.5.1 Création colonne 'value' = concaténation des 4 autres colonnes valeurs de la table clé-valeur

# %%
df_base_valeurs_meta["value"] = df_base_valeurs_meta.apply(
    lambda x: x["value_boolean"]
    if pd.isna(x["value_boolean"]) == False
    else (
        x["value_integer"]
        if pd.isna(x["value_integer"]) == False
        else (
            x["value_float"]
            if pd.isna(x["value_float"]) == False
            else x["value_string"]
        )
    ),
    axis=1,
)

# %% [markdown]
# ### 4.5.2 Table pivot avec une ligne par finess/année et 1 colonne par variable

# %%
df_base_valeurs_meta["finess_year"] = df_base_valeurs_meta["finess"].astype(
    "str"
) + df_base_valeurs_meta["annee"].astype("str")

df_pivot = df_base_valeurs_meta.pivot(
    index=["finess_year"], columns="key", values="value"
)

# %% [markdown]
# ### 4.5.3 Cohérence ICSHA

# %%
fig, ax = plt.subplots(figsize=(8, 8))

var_quant = "mhs_ias_icsha_2018-resultat_icsha"
var_qual = "mhs_ias_icsha_2018-icsha_classe"

df_pivot[var_quant] = pd.to_numeric(df_pivot[var_quant], errors="coerce")

histplot = sns.histplot(
    data=df_pivot, x=var_quant, hue=var_qual, kde="True"
).set(xlim=(0))

plt.xlim(0, 300)

# %% [markdown]
# ### 4.5.4 Cohérence COORD

# %%
fig, ax = plt.subplots(figsize=(8, 8))

var_quant = "had_dpa_coord_2018-resultat_coord"
var_qual = "had_dpa_coord_2018-coord_classe"

df_pivot[var_quant] = pd.to_numeric(df_pivot[var_quant], errors="coerce")

histplot = sns.histplot(
    data=df_pivot, x=var_quant, hue=var_qual, kde="True", bins=40
).set(xlim=(0))

plt.xlim(0, 100)

# %% [markdown]
# # 5 Analyse des indicateurs : comparaison base BQSS avec ATIH <a class="anchor" id="5"></a>

# %% [markdown]
# ## 5.1 NIVEAU DE CERTIFICATION <a class="anchor" id="5.1"></a>

# %% [markdown]
# Incohérences pour environ 80 finess
#
# Explication : données de certif de notre nouvelle base plus récentes que celles de ATIH ?

# %%
indicateur_atih = "Certification de l'établissement"
indicateur_base = "certif_id_niveau"
colonne_atih = "cotation"
colonne_base = "value_integer"
annee = 2021


val_indicateur_atih = df_atih[
    ["libelle_long", "finess", "valeur", "cotation"]
][df_atih["libelle_long"] == indicateur_atih]
print(val_indicateur_atih.shape)

val_indicateur_base = df_base_valeurs[
    df_base_valeurs["key"] == indicateur_base
]
val_indicateur_base = val_indicateur_base[
    val_indicateur_base["annee"] == annee
]
print(val_indicateur_base.shape)


indicateur_atih_base = val_indicateur_atih.merge(
    val_indicateur_base, left_on="finess", right_on="finess", how="inner"
)

print(indicateur_atih_base.shape)

cont = (
    indicateur_atih_base[[colonne_atih, colonne_base]]
    .pivot_table(index=colonne_atih, columns=colonne_base, aggfunc=len)
    .copy()
    .fillna(0)
    .astype(int)
)
print(cont)
cont_proportion = cont.apply(lambda x: x / sum(x), axis=1)
sns.heatmap(
    cont_proportion,
    cmap=sns.diverging_palette(0, 230, 90, 60, as_cmap=True),
    center=0,
)

# %%
ecarts = indicateur_atih_base.query('cotation == "C" and value_integer == 2')
print(ecarts)

# %% [markdown]
# ## 5.2 INDICATEUR IQSS : ICSHA <a class="anchor" id="5.2"></a>

# %% [markdown]
# Cohérence parfaite

# %%
indicateur_atih = "Indicateur de consommation de produits hydro-alcooliques "
indicateur_base = "mhs_ias_icsha_2018-icsha_classe"
colonne_atih = "cotation"
colonne_base = "value_string"
annee = 2018


val_indicateur_atih = df_atih[
    ["libelle_long", "finess", "valeur", "cotation"]
][df_atih["libelle_long"] == indicateur_atih]
print(val_indicateur_atih.shape)

val_indicateur_base = df_base_valeurs[
    df_base_valeurs["key"] == indicateur_base
]
val_indicateur_base = val_indicateur_base[
    val_indicateur_base["annee"] == annee
]
print(val_indicateur_base.shape)


indicateur_atih_base = val_indicateur_atih.merge(
    val_indicateur_base, left_on="finess", right_on="finess", how="inner"
)

print(indicateur_atih_base.shape)

cont = (
    indicateur_atih_base[[colonne_atih, colonne_base]]
    .pivot_table(index=colonne_atih, columns=colonne_base, aggfunc=len)
    .copy()
    .fillna(0)
    .astype(int)
)
print(cont)
cont_proportion = cont.apply(lambda x: x / sum(x), axis=1)
sns.heatmap(
    cont_proportion,
    cmap=sns.diverging_palette(0, 230, 90, 60, as_cmap=True),
    center=0,
)

# %% [markdown]
# ## 5.3 INDICATEUR ESATIS : Note globale CA <a class="anchor" id="5.3"></a>

# %% [markdown]
# Cohérence parfaite

# %%
indicateur_atih = (
    "Note globale des patients admis pour une chirurgie ambulatoire"
)
indicateur_base = "classement_ca"
colonne_atih = "cotation"
colonne_base = "value_string"
annee = 2020


val_indicateur_atih = df_atih[
    ["libelle_long", "finess", "valeur", "cotation"]
][df_atih["libelle_long"] == indicateur_atih]
print(val_indicateur_atih.shape)

val_indicateur_base = df_base_valeurs[
    df_base_valeurs["key"] == indicateur_base
]
val_indicateur_base = val_indicateur_base[
    val_indicateur_base["annee"] == annee
]
print(val_indicateur_base.shape)


indicateur_atih_base = val_indicateur_atih.merge(
    val_indicateur_base, left_on="finess", right_on="finess", how="inner"
)

print(indicateur_atih_base.shape)

cont = (
    indicateur_atih_base[[colonne_atih, colonne_base]]
    .pivot_table(index=colonne_atih, columns=colonne_base, aggfunc=len)
    .copy()
    .fillna(0)
    .astype(int)
)
print(cont)
cont_proportion = cont.apply(lambda x: x / sum(x), axis=1)
sns.heatmap(
    cont_proportion,
    cmap=sns.diverging_palette(0, 230, 90, 60, as_cmap=True),
    center=0,
)
