import os

import pandas as pd

from bqss.certification_21_25.constants import (
    FINAL_DOMAIN_PATH,
    RAW_DOMAIN_PATH,
)
from bqss.certification_21_25.main import main


def test_process_certifications():
    main(data_validation=True)

    assert len(os.listdir(RAW_DOMAIN_PATH)) > 0
    for filename in [
        "demarches.csv",
        "etablissements_geo.csv",
        "etablissements_jur.csv",
        "resultats_chapitres.csv",
        "resultats_objectifs.csv",
    ]:
        assert os.path.exists(RAW_DOMAIN_PATH / filename)
        assert os.stat(RAW_DOMAIN_PATH / filename).st_size > 0

    kv_df = pd.read_csv(FINAL_DOMAIN_PATH / "certifications_key_value.csv")

    assert len(kv_df) > 5000
    assert kv_df.shape[1] == 7
    assert kv_df.columns.tolist() == [
        "annee",
        "finess",
        "finess_type",
        "key",
        "value_integer",
        "value_float",
        "value_date",
    ]
