import os
from typing import Final

import pytest

from bqss import data_acquisition_utils
from bqss.certification_14_20 import processing
from bqss.certification_14_20.constants import (
    DATA_DOMAIN_PATH,
    FINAL_DOMAIN_PATH,
    RESOURCES_DOMAIN_PATH,
)
from bqss.certification_14_20.data_download import download_data
from bqss.certification_14_20.processing import build_metadata

CERTIFICATIONS_COLUMNS: Final[list] = [
    "finess",
    "nom",
    "cp",
    "ville",
    "typeStructure",
    "courriel",
    "urlSite",
    "longitude",
    "latitude",
    "finessEJ",
    "nomEJ",
    "demarche",
    "typeVisite",
    "ordreVisite",
    "dateDebutVisite",
    "dateFinVisite",
    "nomUsuel",
    "finessEP",
    "nomEP",
    "departementEP",
    "typeEP",
    "dateCertification",
    "idNiveauCertification",
]

THEMATIQUES_COLUMNS: Final[list] = [
    "demarche",
    "thematique",
    "decision",
    "libelle_thematique",
]


@pytest.fixture(name="data_download", scope="session", autouse=True)
def fixture_data_download():
    # Since data download is time/network consuming, we only do it if they have
    # not been previously downloaded by another test
    if not DATA_DOMAIN_PATH.exists():
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)


def test_process_certifications():
    certifications_df = processing.process_certifications()

    assert len(certifications_df) > 8000
    assert certifications_df.shape[1] == 23
    assert certifications_df.columns.tolist() == CERTIFICATIONS_COLUMNS
    # check against duplicates
    number_of_duplicated = certifications_df.duplicated(
        subset=["finess", "demarche"],
        keep=False,
    ).sum()
    assert number_of_duplicated == 0


def test_process_thematiques():
    thematiques_df = processing.process_thematiques()

    assert len(thematiques_df) > 17000
    assert thematiques_df.shape[1] == 4
    assert thematiques_df.columns.tolist() == THEMATIQUES_COLUMNS


def test_format_certifications_to_key_value():
    certifications_df = processing.process_certifications()
    certifications_kv_df = processing.format_certifications_to_key_value(
        certifications_df
    )

    assert len(certifications_kv_df) > 15000
    assert certifications_kv_df.shape[1] == 6
    assert certifications_kv_df.columns.tolist() == [
        "annee",
        "finess",
        "finess_type",
        "key",
        "value_integer",
        "value_date",
    ]


def test_build_metadata():
    if not FINAL_DOMAIN_PATH.exists():
        FINAL_DOMAIN_PATH.mkdir(parents=True)
    build_metadata()

    assert os.path.exists(FINAL_DOMAIN_PATH / "certification_metadata.csv")
