import logging
from typing import Final

import pandas as pd
import pytest
from frictionless import Schema

from bqss import data_acquisition_utils
from bqss.bqss.metadata_reader import get_iqss_metadata
from bqss.constants import (
    FINESS_TYPE_GEO,
    FINESS_TYPE_JUR,
    FINESS_TYPE_UNKNOWN,
)
from bqss.data_acquisition_utils import clean_directory
from bqss.iqss.constants import (
    DATA_IQSS_PATH,
    FINAL_IQSS_PATH,
    KEY_VALUE_DTYPES_DICT,
    RESOURCES_IQSS_PATH,
    SCHEMAS_IQSS_PATH,
)
from bqss.iqss.data_acquisition import download_data
from bqss.iqss.processing import merged_iqss_data, process_data
from bqss.iqss.processing_clean import clean_data
from bqss.iqss.processing_finess import set_finess_type
from bqss.iqss.processing_reformate import reformate_historized_iqss
from bqss.validation_utils import is_metadata_valid, tableschema_to_pandera

logger: Final[logging.Logger] = logging.getLogger(__name__)
HISTORIZED_IQSS_COLUMNS: Final[list] = [
    "finess",
    "finess_type",
    "raison_sociale",
    "annee",
    "key",
    "value_float",
    "value_string",
]


@pytest.fixture(name="data_download", scope="session", autouse=True)
def fixture_data_download():
    # Since data download is time/network consuming, we only do it if they have
    # not been previously downloaded by another test
    if not DATA_IQSS_PATH.exists():
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_IQSS_PATH
        )
        download_data(data_sources)


@pytest.fixture(name="data_processing", scope="session")
@pytest.mark.usefixtures("data_download")
def fixture_data_processing():
    process_data()


@pytest.mark.skip(
    reason="Certains indicateurs du thème DPA des receuils 2019 et 2020 ne sont pas remontés qu'au niveau géographique, à investiguer"
)
def test_build_historized_iqss_data():
    clean_directory(FINAL_IQSS_PATH)

    metadata_df = pd.read_csv(RESOURCES_IQSS_PATH / "metadata.csv", sep=",")
    clean_data(metadata_df)
    merged_iqss_data()
    set_finess_type()
    historized_iqss_key_value = reformate_historized_iqss()

    assert historized_iqss_key_value.shape[0] > 10000
    assert historized_iqss_key_value.shape[1] == 7
    assert (
        len(
            set(historized_iqss_key_value["key"])
            - set(metadata_df["code"].str.lower())
        )
        == 0
    )
    assert set(HISTORIZED_IQSS_COLUMNS) == set(
        historized_iqss_key_value.columns
    )

    # Validate type finess
    # Pour les indicateurs ete | iso avant 2019, le finess est mixte (mélange de
    # geo et jur). A partir de 2019, le finess est géographique uniquement
    iso_ete_after_2019 = historized_iqss_key_value[
        historized_iqss_key_value["key"].str.contains("isoortho|eteortho")
        & (historized_iqss_key_value["annee"] >= 2019)
    ]
    assert (
        iso_ete_after_2019["finess_type"]
        .isin([FINESS_TYPE_GEO, FINESS_TYPE_UNKNOWN])
        .all()
    ), "Certains indicateurs ete/iso ne sont pas remontés qu'au niveau géographique"

    # Pour les indicateurs du thèmes DPA des recueil 2019 et 2020 le finess doit
    # être géographique
    dpa_2018_2019 = historized_iqss_key_value[
        historized_iqss_key_value["annee"].isin([2018, 2019])
        & historized_iqss_key_value["key"].str.contains("dpa")
    ]
    assert (
        dpa_2018_2019["finess_type"]
        .isin([FINESS_TYPE_GEO, FINESS_TYPE_UNKNOWN])
        .all()
    ), "Certains indicateurs du thème DPA des receuils 2019 et 2020 ne sont pas remontés qu'au niveau géographique"

    # Le type de finess des indicateurs psy et icsha doit être mixte (geo + jur)
    psy_icsha_finess_types = historized_iqss_key_value[
        historized_iqss_key_value["key"].str.contains("psy|icsha")
    ]["finess_type"].unique()
    assert all(
        finess_type in psy_icsha_finess_types
        for finess_type in [FINESS_TYPE_GEO, FINESS_TYPE_JUR]
    ), "Le type de finess des indicateurs psy/icsha devrait être mixte (géo et jur)"


def test_validate_iqss_metadata():
    schema = Schema(SCHEMAS_IQSS_PATH / "iqss_key_value.json")

    assert is_metadata_valid(schema)


@pytest.mark.usefixtures("data_processing")
def test_validate_full_iqss_data():
    schema = tableschema_to_pandera(
        SCHEMAS_IQSS_PATH / "iqss_key_value.json",
        nullable_bool=True,
        nullable_int=True,
        coerce=False,
    )

    kv_df = pd.read_csv(
        FINAL_IQSS_PATH / "iqss_key_value.csv", dtype=KEY_VALUE_DTYPES_DICT
    )
    schema.validate(kv_df)


@pytest.mark.usefixtures("data_processing")
def test_regression_full_iqss_data():
    kv_df = pd.read_csv(
        FINAL_IQSS_PATH / "iqss_key_value.csv", dtype=KEY_VALUE_DTYPES_DICT
    )
    assert (
        kv_df[
            kv_df["key"] == "mco_eteortho_eteortho_2017-ete_ortho_cible_etbt"
        ]
        .notna()
        .any()
        .any()
    )


@pytest.mark.usefixtures("data_processing")
def test_validate_iqss_resultat_data():
    kv_df = pd.read_csv(
        FINAL_IQSS_PATH / "iqss_key_value.csv", dtype=KEY_VALUE_DTYPES_DICT
    )
    meta_df = get_iqss_metadata()

    resv_df = kv_df[
        kv_df["key"].isin(
            meta_df[meta_df["type_metier"] == "resultat"]["name"]
        )
    ]

    resb_df = resv_df.groupby(["key"]).agg(
        {"value_float": ["min", "max"], "value_integer": ["min", "max"]}
    )
    for value_col in ["value_integer", "value_float"]:
        # Toutes les valeurs doivent être comprises entre 0 et 100
        assert resb_df[resb_df[value_col]["max"] < 1.1].empty
        assert resb_df[resb_df[value_col]["max"] > 100].empty
