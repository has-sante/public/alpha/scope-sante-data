import os

from bqss import data_acquisition_utils
from bqss.esatis.constants import RAW_ESATIS_PATH, RESOURCES_ESATIS_PATH
from bqss.esatis.data_acquisition import download_data

esatis_raw_data = {
    "2016": ["mco_48h.csv"],
    "2017": ["mco_48h.csv"],
    "2018": ["mco_48h.csv", "mco_ca.csv"],
    "2019": ["mco_48h.csv", "mco_ca.csv"],
    "2020": ["mco_48h.xlsx", "mco_ca.xlsx"],
}


def test_download_data():
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_ESATIS_PATH
    )

    download_data(data_sources)

    for source_name, theme_activity in esatis_raw_data.items():
        assert len(os.listdir(RAW_ESATIS_PATH)) > 0

        esatis_data_path = RAW_ESATIS_PATH / source_name

        for file in theme_activity:
            esatis_file_path = esatis_data_path / file
            assert esatis_file_path.exists()
            assert esatis_file_path.stat().st_size > 0
