import pytest

from bqss import data_acquisition_utils
from bqss.sae.constants import DATA_DOMAIN_PATH, RESOURCES_DOMAIN_PATH
from bqss.sae.data_download import download_data
from bqss.sae.processing import process_id_data
from bqss.sae.processing_cancero import process_cancero_data
from bqss.sae.processing_dialyse import process_dialyse_data
from bqss.sae.processing_filtre import process_filtre_data
from bqss.sae.processing_had import process_had_data
from bqss.sae.processing_mco import process_mco_data
from bqss.sae.processing_psy import process_psy_data
from bqss.sae.processing_ssr import process_ssr_data
from bqss.sae.processing_usld import process_usld_data


@pytest.fixture(name="data_download", scope="session", autouse=True)
def fixture_data_download():
    # Since data download is time/network consuming, we only do it if they have
    # not been previously downloaded by another test
    if not DATA_DOMAIN_PATH.exists():
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)


def test_process_id_data():
    sae_df = process_id_data()

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 4
    es_test = sae_df[
        (sae_df["id_fi"] == "920000650") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["id_rs"] == "HOPITAL FOCH"


def test_process_filtre_data():
    sae_df = process_id_data()

    sae_df = process_filtre_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 42
    es_test = sae_df[
        (sae_df["id_fi"] == "920000650") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["filtre_heb_med"]


def test_process_mco_data():
    sae_df = process_id_data()

    sae_df = process_mco_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 33
    es_test = sae_df[
        (sae_df["id_fi"] == "920000650") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["mco_lit_med"] == 267


def test_process_had_data():
    sae_df = process_id_data()

    sae_df = process_had_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 6
    es_test = sae_df[
        (sae_df["id_fi"] == "750806226") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["had_platot"] == 803


def test_process_ssr_data():
    sae_df = process_id_data()

    sae_df = process_ssr_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 8
    es_test = sae_df[
        (sae_df["id_fi"] == "750000499") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["ssr_lit"] == 10


def test_process_psy_data():
    sae_df = process_id_data()

    sae_df = process_psy_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 14
    es_test = sae_df[
        (sae_df["id_fi"] == "010000495") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["psy_jou_htp_gen"] == 96916


def test_process_dialyse_data():
    sae_df = process_id_data()

    sae_df = process_dialyse_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 34
    es_test = sae_df[
        (sae_df["id_fi"] == "920000650") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["dialyse_capa_112ba"] == 12


def test_process_cancero_data():
    sae_df = process_id_data()

    sae_df = process_cancero_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 7
    es_test = sae_df[
        (sae_df["id_fi"] == "010000024") & (sae_df["id_an"] == 2018)
    ]
    assert len(es_test) == 1
    data = es_test.iloc[0, :]
    assert data["cancero_a10"] == 6888
    assert data["cancero_a15"] == 18510
    assert data["cancero_a16"] == 0


def test_process_usld_data():
    sae_df = process_id_data()

    sae_df = process_usld_data(sae_df)
    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 5
    es_test = sae_df[
        (sae_df["id_fi"] == "010000081") & (sae_df["id_an"] == 2018)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["usld_jou"] == 10316
