import glob
import os

from bqss import data_acquisition_utils
from bqss.sae.constants import RAW_DOMAIN_PATH, RESOURCES_DOMAIN_PATH
from bqss.sae.data_download import download_data


def test_download_data():
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_DOMAIN_PATH
    )

    download_data(data_sources)

    assert len(os.listdir(RAW_DOMAIN_PATH)) >= 3
    # the following asserts are based on at least 3 years of data including 2019
    filenames = glob.glob(
        str(RAW_DOMAIN_PATH / f"**{os.path.sep}*.csv"), recursive=True
    )
    assert len(filenames) > 150
    mco_2019_file = glob.glob(
        str(RAW_DOMAIN_PATH / f"**{os.path.sep}MCO_2019r.csv"), recursive=True
    )
    assert len(mco_2019_file) == 1
    mco_2019_file = mco_2019_file[0]
    assert os.stat(mco_2019_file).st_size > 0
