# Mise en Production

🚧 WIP 🚧

## Pipeline CI/CD

🚧 WIP 🚧

La validation des données est réalisée au cours du pipeline de CI/CD en partie seulement et via la suite de tests automatisés. Dans ces tests, seules les 1000 premières lignes de données de chaque fichier sont extraites pour être validées. Une validation de l'intégralité des données est toujours réalisable mais le temps nécessaire à son déroulement est significatif, à savoir une dizaine de minutes.

## Validation Fonctionnelle

🚧 WIP 🚧

Des tests automatisés s'assurent de la cohérence entre le référentiel FINESS et les valeurs du fichier clé-valeur, et procèdent à des validations complémentaires via des analyses exploratoires.
