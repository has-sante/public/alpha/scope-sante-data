# Mises à jour des données

## Mise à jour du périmètre FINESS

Une mise à jour des URLs des dernières extractions FINESS peut être nécessaire lorsque le jeu de donnée en open data est mis à jour.
En effet, les extractions les plus récentes sont datées et les permalinks sont modifiés lorsque ces extractions sont mises à jour.
Si tel est le cas, il suffit de renseigner les nouvelles URLs dans le fichier `resources/finess/data_sources.yml`, dans les clés `sources.finesset.files.latest` et `sources.finessej.files.latest`.
L'URL de récupération des données historiques ne devrait pas changer, même en cas de mise à jour du jeu de données.
Une mise à jour des extractions FINESS allant généralement de pair avec une mise à jour des autorisations d'activités de soins, la manipulation est à répéter.
La nouvelle URL est à renseigner dans le même fichier resources, dans la clé `sources.autorisations_as.files.latest`.

Pour toutes demandes concernant spécifiquement ces données, s'adresser au producteur : l'Agence du Numérique en Santé (ANS).
Un formulaire de prise de contact est disponible sur la page correspondante du portail data.gouv.fr.

## Mise à jour du périmètre Certification

Les données de certification étant déjà historisées lors de leur import, aucune manipulation particulière ne devrait être requise. Si les URLs du jeu de données venaient à changer, il faudrait alors mettre à jour ces dernières dans le fichier `resources/certification/data_sources.yml`.

Pour toutes demandes concernant spécifiquement ces données, s'adresser au producteur : Haute Autorité de Santé - Service de Certification des Établissements de Santé (SCES).
Un formulaire de prise de contact est disponible sur la page correspondante du portail data.gouv.fr.

## Mise à jour du périmètre IQSS

Les sources de données du périmètre IQSS se situent dans le fichier `resources/iqss/data_sources.yml`.
Pour ajouter les IQSS d'une nouvelle année, la première étape consiste à compléter ce fichier en respectant le formalisme en place. À savoir : un nouvel item yml contenant l'année du recueil et la liste des fichiers (avec thème/secteur) que le pipeline doit collecter (les données seront téléchargées via l'URL `data_url`).
La seconde étape consiste à mettre à jour le fichier de metadata IQSS `resources/iqss/metadata.csv`.
Ce fichier a pour but de renseigner les variables collectées lors de l'acquisition/agrégation des données.

Pour toutes demandes concernant spécifiquement ces données, s'adresser au producteur : Haute Autorité de Santé - service Évaluation et Outils pour la Qualité et la Sécurité des Soins (EvOQSS).
Un formulaire de prise de contact est disponible sur la page correspondante du portail data.gouv.fr.

## Mise à jour du périmètre e-Satis

Les sources de données du périmètre e-Satis se situent dans le fichier `resources/esatis/data_sources.yml`.
Pour ajouter les données e-Satis d'une nouvelle année, il suffit de compléter ce fichier en respectant le formalisme en place.

Pour toutes demandes concernant spécifiquement ces données, s'adresser au producteur : Haute Autorité de Santé - service Évaluation et Outils pour la Qualité et la Sécurité des Soins (EvOQSS).
Un formulaire de prise de contact est disponible sur la page correspondante du portail data.gouv.fr.

## Mise à jour du périmètre SAE

Comme pour les autres domaines, la mise à jour des URLs du jeu de données exposé par la DREES est réalisable dans le fichier `resources/sae/data_sources.yml`.
Pour rajouter une nouvelle année, il faut ajouter un nouvel item à la liste `sources` présente dans le fichier resources en renseignant l'année et l'URL des données.

Pour toutes demandes concernant spécifiquement ces données, s'adresser au producteur : Ministère des Solidarités et de la Santé, DREES, Bureau des Établissements de Santé (BES).

### Traitement d'un nouveau bordereau

Pour traiter les données d'un nouveau bordereau (exemple : bordereau XYZ), il faut créer un fichier python suivant la nomenclature existante (exemple: processing_xyz.py) et ajouter dans ce fichier une logique similaire à celle existante pour le traitement des autres bordereaux.
Ce travail n'a volontairement pas été mutualisé au sein d'une même fonction dans le cas où des traitements spécifiques seraient nécessaires (comme c'est le cas pour certains des bordereaux déjà traités).
Néanmoins la présence d'un dictionnaire nommé `DTYPES_DICT` listant les champs à extraire est fortement conseillé.
Pour extraire de nouveaux champs à l'intérieur d'un bordereau déjà traité, il suffit d'ajouter dans le dictionnaire `DTYPES_DICT` du fichier du bordereau correspondant les noms et types des champs que l'on souhaite extraire.

## Mise à jour du périmètre BQSS

La mise à jour du périmètre BQSS est conditionnée par la mise à jour des domaines de données dont la base est issue.
Pour mettre à jour la base BQSS, il suffit donc de lancer le pipeline avec le flag `force-full-run` (`cli bqss --force-full-run`) ce qui relancera tous les pipelines des domaines de données en amont (finess, certification, iqss, esatis, sae) ainsi que le pipeline BQSS.
Dans le cas où l'on ne souhaiterait pas relancer tous les pipelines amont, ces pipelines peuvent être lancés individuellement avec leur [commandes cli](cli-usage.md) respectives.
Dans ce dernier cas, il faudra relancer le pipeline BQSS en dernier avec la commande `cli bqss` afin de produire la base BQSS.
