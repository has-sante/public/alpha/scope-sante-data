# Execution du projet

Le point d'entrée principal pour lancer le pipeline de constitution de la base est une CLI dont le code est situé dans `bin/cli`.

Pour lancer la CLI, il faut avoir d'abord avoir configuré [l'environnement](development.md).
Une fois cela fait, on peut accéder à la CLI avec:

```shell
poetry run cli
```

Ou en activant le virtualenv python:

```shell
poetry shell
cli
```

Cela devrait afficher l'ensemble des commandes disponibles.

Pour faire tourner l'ensemble du pipeline, utilisez:

```shell
cli bqss --force-full-run
```

Pour faire l'upload sur data.gouv.fr (en ayant configuré les bonnes variables d'env):

```shell
cli bqss --force-full-run --open-data
```

Il est aussi possible d'executer individuellement chaque étape du pipeline en utilisant les commandes associées.
Par exemple:

```shell
cli bqss finess
```

Permet de ne lancer que la partie relative au FINESS.

## Détails des commandes disponibles


**Usage**:

```{eval-rst}
.. click:: bin.cli:cli
    :prog: cli
    :nested: full
```
