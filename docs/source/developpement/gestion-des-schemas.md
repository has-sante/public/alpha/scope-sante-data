# Gestion des schémas

Pour renforcer le contrôle qualité des données produites, il est essentiel de mettre en place des contraintes et des vérifications sur ces dernières.

Pour assurer ces bonnes pratiques de développement, la validation des données est assurée par le [framework Frictionless](https://framework.frictionlessdata.io/) (qui correspond à une version améliorée de goodtables).
Ce framework s'accompagne d'un ensemble de [spécifications](https://specs.frictionlessdata.io/) dont le standard [Table Schema](https://specs.frictionlessdata.io/table-schema/), sur lesquelles s'appuie le projet.


**Exemples d'utilisation du framework frictionless :**
- pour valider le "table schema" associé à *historized_finess* :
```
frictionless validate schemas/finess/historized_finess.json --type schema
```
- pour valider un fichier de données (ici *historized_finess.csv*) sur la base du "table schema" correspondant :
```
frictionless validate --path data/finess/processed/finess/historized_finess.csv --schema schemas/finess/historized_finess.json
```
