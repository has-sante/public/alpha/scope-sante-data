# Développement

> 📝 **Note**
> Par commodité, la majorité des processus ci-dessous sont encapsulés dans des cibles [make](https://www.gnu.org/software/make/).

> 🔥 **Astuce**
> Invoquer `make` sans autres arguments renverra la documentation auto-générée décrivant les commandes disponibles.

## Architecture du Projet

L'arborescence du projet se structure comme suit :

- **bin** : ce dossier contient les commandes invocables via la [CLI BQSS](cli-usage)
- **data** : ce dossier contient les données et metadonnées "générées" par le projet, celles qu'on ne souhaite pas versionner. Cela comprend les données brutes téléchargées depuis différentes sources (dans les dossiers `raw` de chaque domaine) ainsi que les données finales qui seront uploadées en open data (dossiers `final`) mais également les données intermédiaires lorsque cela est nécessaire
- **docs** : ce dossier contient les pages de markdown générant la présente documentation
- **notebooks** : ce dossier contient les notebooks ayant servis à des analyses exploratoires ou de validation
- **resources** : ce dossier contient les ressources statiques du projet, principalement les fichiers de configuration contenant les URL des sources de données qui seront téléchargées par le pipeline. On y trouve également des fichiers de métadonnées tels que les nomenclatures par domaine ou le fichier de métadonnée IQSS. Enfin, un dossier data_gouv contient un fichier markdown contenant la description de la base BQSS publiée sur Data Gouv.
- **schemas** : ce dossier contient les [table schemas](https://specs.frictionlessdata.io/table-schema/) (fichier de métadonnées contenant les contraintes de validation à appliquer) utilisés par le framework Frictionless pour valider nos données
- **bqss** : ce dossier contient le code python de la chaîne de traitements générant la base BQSS. Comme d'autres dossiers (`resources`, `data`, etc) il est directement découpé par [domaines de données](../creation/data-processing-description)
- **tests** : ce dossier contient le code python de notre suite de tests unitaires basés sur pytest. L'intégralité du pipeline y est testée séquentiellement
- _Makefile_ : ce projet utilise le gestionnaire de build/tâches [Make](https://www.gnu.org/software/make/) dont les différentes cibles (tâches) sont explicitées dans ce fichier
- _poetry.lock_ : fichier listant les dépendances utilisées par le projet ainsi que leur numéro de version
- _pyproject.toml_ : ce projet utilise le gestionnaire de dépendances [Poetry](https://python-poetry.org) dont ce fichier est le fichier de configuration principal

## Exécuter le Projet

> 📝 **Note**
> Toutes les commandes suivantes sont relatives au répertoire racine du projet et présupposent que `make` soit installé.

### 1. Environnement du Conteneur Docker du Projet (recommandé)

Exécuter :

```shell script
# Décommenter les lignes ci-dessous pour une exécution avec les options correspondantes.

# Note: *toute* valeur autre que `false` déclenchera une option
#export IS_INTERACTIVE_SESSION=true
#export BIND_MOUNT_APPLICATION_DIR_ON_CONTAINER=true
make docker-build-run
```

ce qui construira et exécutera l'image du conteneur du projet.

### 2. Localement via Poetry (workflow de développement)

Exécuter :

```shell script
make provision-environment # Note: installe TOUTES les dépendances!
poetry shell # Active l'environnement virtuel du projet

# TODO
```

## Installation du Paquet et ses Dépendances

Assurez-vous d'avoir installé et configuré Python 3.8+ et [poetry](https://python-poetry.org/).

Pour installer le paquet et toutes les dépendances de développement, exécutez :

```shell script
make provision-environment
```

> 🔥 **Astuce**
> Invoquer la commande ci-dessus sans avoir au préalable installé `poetry` renverra un message d'erreur explicite faisant savoir comment installer poetry.

## Orchestration de la Construction et du Déploiement de l'Image du Conteneur Docker

L'ensemble des cibles `make` suivantes orchestre les étapes de construction et de déploiement de l'image du conteneur du projet :

```
docker-build        Construit le conteneur bqss
docker-rm           Force l'arrêt du conteneur (i.e., lorsque l'interruption clavier est désactivée)

docker-run          Exécute le Dockerfile avec les points d'entrée par défaut
docker-run-interactive Exécute le Dockerfile avec un point d'entrée bash
```

Il est à noter que l'image du conteneur du projet est séparée des détails d'implémentation de la configuration globale de l'application et de sa logique d'exécution, qui dépendent du script de point d'entrée du projet. De fait, des modifications du Dockerfile ne devraient être nécessaires que lors des mises à jour des dépendances d'environnement non liées à Python (la mise à jour des dépendances Python est automatiquement réalisée lors de la construction d'une nouvelle image via les fichiers `pyproject.toml` et `poetry.lock`).

## Testing

La bibliothèque [pytest](https://pytest.readthedocs.io/) est utilisée comme suite de tests automatisés pour ce projet.

Pour invoquer les tests, exécutez :

```shell script
make test
```

## Qualité du Code

La bibliothèque [pre-commit](https://pre-commit.com/) est utilisée pour la gestion et l'analyse automatique de la qualité du code.

Pour invoquer les analyses et l'autoformatage de tous les fichiers suivis par un contrôle de version, exécutez :

```shell script
make lint
```

> 🚨 **Danger**
> La CI échouera si le test ou la qualité du code échoue également, il est donc recommandé d'exécuter localement automatiquement les commandes ci-dessus avant que tout "commit" ne soit poussé vers un répertoire distant.

### Automatisation via Git Pre-Commit Hooks

Pour appliquer automatiquement la validation de la qualité du code sur chaque commit et les fichiers associés, exécutez :

```shell script
make install-pre-commit-hooks
```

> ⚠️ **Attention**
> Les commits ne seront pas effectués si le moindre pre-commit hook échoue (à moins que l'échec ne soit explicitement autorisé) ou si le moindre fichier est modifié par un processus d'autoformatage ; dans ce dernier cas, il suffit de répéter la commande de commit qui devrait alors être effectif.

## Documentation

L'outil [Sphinx](https://www.sphinx-doc.org/en/master/) est utilisé pour générer la documentation du projet, avec la commande suivante :

```shell script
make docs-clean docs-html
```
