# Domaine SAE

## Description générale

La Statistique Annuelle des Établissements de santé (SAE) est une enquête administrative, exhaustive et obligatoire auprès des établissements de santé installés en France (métropole et DROM), y compris les structures qui ne réalisent qu'un seul type d'hospitalisation et/ou qui ont une autorisation pour une seule activité de soins. Les données sont collectées par la DREES (Direction de la Recherche, des Études, de l'Évaluation et des Statistiques), direction du Ministère des Solidarités et de la Santé.

**Borderaux thématiques de la SAE** : Le questionnaire de collecte est modulable. Un bordereau filtre initial a pour but de définir les modules de questionnement devant être soumis à chaque établissement selon leurs secteurs d'activités de soins pour rendre le processus plus ciblé. Pour chaque périmètre, un bordereau est proposé lorsque c'est pertinant. Voici quelques exemples d'acronymes de bordereaux thématiques : ID2, FILTRE, MCO, PSY, SSR, HAD, BLOCS, IMAGES, PHARMA, PERINAT, CHIRCAR, DOULEUR, URGENCES, COVID19, Q21, Q22, Q23. Plus d'information est disponible dans [ce document](https://drees.solidarites-sante.gouv.fr/sites/default/files/2021-02/Aide%20au%20remplissage%20%28SAE%202020%29.pdf).

**Spécificité des établissements PSY** : Les établissements réalisant uniquement une activité de santé mentale font l’objet d’une interrogation groupée. Un établissement par département est ainsi désigné pour être interrogé et pour répondre pour l’ensemble des structures de psychiatrie situées dans son département. Dans le cas particulier des établissements privés ne faisant que de la psychiatrie et tous situés dans le même département, c’est l’entité juridique (EJ) de ces établissements qui est interrogée et qui répond pour l’ensemble des établissements de psychiatrie qui lui sont rattachés. Dans certains cas, cette EJ n’est pas située dans le même département ou dans la même région que les établissements qui lui sont rattachés.
Au final, les données collectées sont localisées sur le lieu d’implantation de l’établissement répondant, y compris lorsque la réponse est collectée au niveau de l’EJ.
**Source**: https://www.sae-diffusion.sante.gouv.fr/sae-diffusion/accueil.htm

Le schéma de ce domaine est décrit sur [cette page](../schemas/sae/sae.md).

## Acquisition des données

Les données de la SAE sont téléchargées à partir du site d'Open Data de la DREES et plus précisément du jeu de données "[Bases statistiques SAE](https://data.drees.solidarites-sante.gouv.fr/explore/dataset/708_bases-statistiques-sae/information/)" (les sources sont exhaustivement répertoriées dans le fichier `resources/sae/data_sources.yml`). Elles sont stockées et réparties par année dans le dossier `data/sae/raw`. Chaque fichier CSV associé à une année correspond aux réponses à un des bordereaux thématiques de la SAE.

Il est important de noter que les bases dites "statistiques" exploitées dans ce projet sont différentes des bases dites "administratives" : les bases "statistiques" sont un jeu de fichiers qui améliorent les données des bases "administratives" par des retraitements statistiques, et du nettoyage des non-réponses et des erreurs déclaratives manifestes.

Pour toutes demandes concernant spécifiquement ces données, s'adresser au producteur : Ministère des Solidarités et de la Santé, DREES, Bureau des Établissements de Santé (BES).

## Traitements et agrégation des données

Grâce aux scripts présents dans le dossier `bqss/sae/`, les données de la SAE sont traitées par bordereaux, desquels sont extraits les champs d'intéret. Il est à noter que certains bordereaux ne sont pas pertinents dans le cadre de la base de données BQSS, et n'ont donc pas été inclus. Voici les bordereaux exploités :

- bordereau ID (fichier `processing_id.py`) : bordereau sur l'identification de l'entité interrogée
- bordereau FILTRE (fichier `processing_filtre.py`) : bordereau caractérisant précisément l'offre de soins disponible
- bordereau MCO (fichier `processing_mco.py`) : description des capacités et activités MCO
- bordereau PERINAT (fichier `processing_mco.py`) : périnatalité (obstétrique, néonatologie, IVG, etc)
- bordereau HAD (fichier `processing_had.py`) : description des capacités et activités d'HAD
- bordereau SSR (fichier `processing_ssr.py`) : description des capacités et activités de SSR
- bordereau PSY (fichier `processing_psy.py`) : description des capacités et activités de psychiatrie et de santé mentale
- bordereau DIALYSE (fichier `processing_dialyse.py`) : traitement de l'insuffisance rénale chronique par épuration extra-rénale
- bordereau BLOCS (fichier `processing_blocs.py`) : sites opératoires et salles d'intervention

Chaque fichier dispose d'un dictionnaire généralement nommé `DTYPES_DICT` spécifiant les noms des champs à extraire du bordereau ainsi que le type de la donnée.

À l'issue de la phase d'extraction des données, une agrégation en clé-valeur est réalisée produisant le fichier `data/sae/final/sae_key_value.csv` ainsi que la génération d'un fichier CSV de métadonnées documentant les champs extraits (voir [section correspondante](#présentation-du-fichier-csv-de-métadonnées) ci-dessous).

## Validation des données

La validation des données du domaine SAE est réalisée par la librairie [frictionless](https://framework.frictionlessdata.io/docs/guides/introduction) et le standard [Table Schema](https://specs.frictionlessdata.io/table-schema/). Cette validation s'appuie sur le fichier de métadonnées `schemas/sae/sae.json`.

## Présentation du fichier CSV de métadonnées

Le fichier CSV de métadonnées du domaine SAE `data/sae/final/sae_metadata.csv` a été généré automatiquement grâce au fichier `schemas/sae/sae.json` décrivant le schéma de données au standard [Table Schema](https://specs.frictionlessdata.io/table-schema/). Il permet de documenter la version clé-valeur des données.

Le fichier de métadonnées SAE est ensuite enrichi de la colonne `secteur` à l'aide du dictionnaire `METADATA_SECTEUR_DICT` présent dans le fichier de constantes du domaine (`bqss/sae/constants.py`). La correspondance entre le secteur d'un indicateur et son nom s'effectue grâce au bordereau dont l'indicateur est issu. Pour certains bordereaux (FILTRE par exemple), cette correspondance n'est pas évidente et n'est donc pas renseignée.

Le fichier de métadonnées contient au final 5 champs :

- `name`: le nom de la variable,
- `title`: le titre de la variable, explicite la variable en une phrase
- `description`: correspond à une description plus détaillée du champ,
- `type`: type primitif de la variable (chaîne de caractères, entier ou décimal).
- `secteur`: défini grâce soit au nom de la variable, soit au nom du fichier issu de l’Open Data ou soit a été explicité par la HAS. Il en existe 5:
  _ **MCO** : **M**édecine, **C**hirurgie, **O**bstétrique,
  _ **SSR** : **S**oins de **S**uite et de **R**éadaptation,
  _ **HAD** : **H**ospitalisation **A** **D**omicile,
  _ **PSY** : **PSY**chiatrie, \* **MHS** (MCO-HAD-SSR) : secteur créé par la HAS pour les variables calculées de la même façon pour ces 3 secteurs
  TODO: add type_metier doc
