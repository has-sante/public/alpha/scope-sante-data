# Domaine Certification - Référentiel 2021-2025

## Description générale

La démarche de certification a pour objectif de porter une appréciation indépendante sur la qualité des prestations d’un établissement de santé. La version actuelle est appelée référentiel 2021-2025.
Des professionnels de santé mandatés par la HAS réalisent des visites de certification sur la base d'objectifs regroupés en plusieurs chapitres.
A l’issue de la procédure de certification, la HAS prononce un niveau global de certification.
Le niveau global de certification résulte des scores des différents chapitres, eux mêmes composés des objectifs.
Pour plus de détails, vous pouvez vous référez à la [page open-data dédiée](https://www.data.gouv.fr/fr/datasets/certification-des-etablissements-de-sante-pour-la-qualite-des-soins-referentiel-deploye-2021-2025/).

Le schéma des données de certifications sont décrits ici:

- [certifications](../schemas/certification_21_25/certifications.md)
- [chapitres](../schemas/certification_21_25/chapitres.md)
- [objectifs](../schemas/certification_21_25/objectifs.md)

## Acquisition des données

Les données de certification sont téléchargées à partir du jeu de données [Certification des établissements de santé pour la qualité des soins (référentiel déployé 2021-2025)](https://www.data.gouv.fr/fr/datasets/certification-des-etablissements-de-sante-pour-la-qualite-des-soins-referentiel-deploye-2021-2025/) mis à disposition par la HAS sur data.gouv.fr (les sources sont exhaustivement répertoriées dans le fichier `resources/certification_21_25/data_sources.yml`). Les fichiers constituant ce jeu de données sont déposés dans le dossier `data/certification_21_25/raw/` avant d'être traités.

Pour toutes demandes concernant spécifiquement ces données, s'adresser au producteur : Haute Autorité de Santé - Service de Certification des Établissements de Santé (SCES).
Un formulaire de prise de contact est disponible sur la page correspondante du portail data.gouv.fr.

## Traitements des données

Le traitement des données de certification du référentiel 2021-2025 est assez simple:

- On fusionne les tables présentants les resultats par démarche de certification, par établissement, par chapitre et par objectif.
- On dénormalise (au format "clef-valeur") le résultat de la fusion pour obtenir une table avec une ligne par etablissement et par résultat (certification, score de chapitre, score d'objectif etc.)

## Validation des données

La validation des données de certification est réalisée par la librairie [frictionless](https://framework.frictionlessdata.io/docs/guides/introduction/) et le standard [Table Schema](https://specs.frictionlessdata.io/table-schema/). Cette validation s'appuie sur 3 fichiers de métadonnées (un par table) au format JSON et se trouvant dans le dossier `schemas/certifications_21_25`.

## Présentation du fichier CSV de métadonnées

Le fichier CSV de métadonnées du domaine Certification `data/certifications_21_25/final/certification_metadata.csv` a été généré automatiquement à partir des fichiers de l'open-data. Il permet de connaître les libellés de chacun des clefs de la version clef-valeur des données

Le fichier de métadonnées contient 4 champs :

- `name`: le nom de la variable,
- `title`: le titre de la variable, explicite la variable en une phrase
- `description`: correspond à une description plus détaillée du champ,
- `type`: type primitif de la variable (chaîne de caractères, entier ou décimal).
