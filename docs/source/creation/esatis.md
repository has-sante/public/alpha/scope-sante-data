# Domaine e-Satis

## Description générale

Le domaine e-Satis correspond aux indicateurs de qualité et de soins obtenus grâce à l’interface e-Satis. Cette dernière a été déployée sur deux périmètres d'activités de soins : les hospitalisations de plus de 48 heures en Médecine/Chirurgie/Obstrétrique (à partir de 2016) et la chirurgie ambulatoire (à partir de 2018).
Le pipeline procède à la concaténation des données issues des recueils annuels collectés sur ces deux périmètres.

Le schéma de ce domaine est décrit sur [cette page](../schemas/esatis/esatis.md) ainsi que sur [cette page](../schemas/esatis/esatis_key_value.md) pour la version clé-valeur.

## Acquisition des données

Les données e-Satis sont téléchargées depuis le portail [data.gouv.fr](https://www.data.gouv.fr/fr/), où elles sont regroupées par année ([recueil 2016](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-mesure-de-la-satisfaction-dispositif-e-satis-recueil-2016/), [recueil 2017](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-mesure-de-la-satisfaction-dispositif-e-satis-recueil-2017/), [recueil 2018](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-mesure-de-la-satisfaction-dispositif-e-satis-recueil-2018/), [recueil 2019](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2019/), [recueil 2020](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2020/) ; les sources sont exhaustivement répertoriées dans le fichier `resources/esatis/data_sources.yml`). Les fichiers correspondants sont stockés par année au format CSV dans le dossier `data/esatis/raw/`.

Pour toutes demandes concernant spécifiquement ces données, s'adresser au producteur : Haute Autorité de Santé - service Évaluation et Outils pour la Qualité et la Sécurité des Soins (EvOQSS).
Un formulaire de prise de contact est disponible sur la page correspondante du portail data.gouv.fr.

## Traitements et agrégation des données

Cette phase se décompose en 5 étapes :

- **nettoyage des données** : 7 colonnes sont abandonnées car non pertinentes (`participation`, `depot`, `rs_finess`, `rs_finess_geo`, `region`, `type`, `region_id`) ; et normalisation des champs ayant des valeur mixtes (des valeurs numériques stockées sous forme d’entiers ou de chaînes de caractères). Les données résultantes sont stockées dans le dossier `data/esatis/cleaned/`
- **agrégation des données** : cette étape fusionne tous les fichiers et dépose le fichier résultant `esatis.csv` dans `data/esatis/final`
- **ajout du type du finess** : grâce au fichier final des finess, une nouvelle colonne est ajoutée au fichier du domaine e-Satis agrégé, correspondant au type du finess.
- **reformatage du fichier final** : le fichier est reformaté pour suivre le format clé-valeur. Ce fichier `esatis_key_value.csv` est stocké dans le dossier `data/final`.
- **génération de métadonnées** : génération d'un fichier CSV de métadonnées documentant les champs extraits (voir [section correspondante](#présentation-du-fichier-csv-de-métadonnées) ci-dessous).

## Validation des données

La validation des données du domaine e-Satis est réalisée par la librairie [frictionless](https://framework.frictionlessdata.io/docs/guides/introduction/) et le standard [Table Schema](https://specs.frictionlessdata.io/table-schema/). Cette validation s'appuie sur deux fichiers de métadonnées au format JSON et se trouvant dans le dossier `schemas/esatis` : `esatis.json` et `esatis_key_value.json`.

## Présentation du fichier CSV de métadonnées

Le fichier CSV de métadonnées du domaine e-Satis `data/esatis/final/esatis_metadata.csv` a été généré automatiquement grâce au fichier `schemas/esatis/esatis.json` décrivant le schéma de données au standard [Table Schema](https://specs.frictionlessdata.io/table-schema/). Il permet de documenter la version clé-valeur des données et contient 6 champs :

- `name` : le nom de la variable
- `title` : le titre de la variable, explicite la variable en une phrase
- `description` : correspond à une description plus détaillée du champ
- `type` : type primitif de la variable (chaîne de caractères, entier ou décimal)
- `secteur` : correspond au secteur que cible l'enquête (par exemple MCO)
- `theme` : correspond à la spécification de l'enquête au sein du secteur cible (pour la MCO, par exemple, hospitalisation de plus de 48h ou encore chirurgie ambulatoire)
