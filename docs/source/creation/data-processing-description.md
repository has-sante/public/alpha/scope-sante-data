Description générale du processus
=================================

## Composition de la chaîne de traitements

La chaîne de traitements constituant la base BQSS s'effectue en deux temps :
1. pour chaque domaine – *i.e.* FINESS, SAE, IQSS, e-Satis, Certification –, un pipeline produit les données raffinées correspondantes
2. les données générées dans la précédente phase sont agrégées pour constituer le domaine BQSS.

Ce choix permet de paralléliser certains développements et de faire évoluer des différents domaines de données d'origine indépendamment les uns des autres. Pour que ces avantages demeurent, il a également été fait le choix de limiter au maximum les intéractions entre domaines de données avant leur agrégation finale.

La plupart des domaines de données sont produits selon un processus similaire en 3 temps :
1. acquisition des données en Open Data grâce aux URLs répertoriées par domaine dans le dossier `resources` (voir section [Architecture du répertoire](../developpement/development.md))
2. traitement et agrégation des données (notamment en un fichier clé-valeur et un fichier de metadonnées les documentant)
3. validation des données via le [framework Frictionless](https://framework.frictionlessdata.io/) et le standard [Table Schema](https://specs.frictionlessdata.io/table-schema/).

Il est à noter que la 3e phase de validation n'est qu'optionnelle et est désactivée par défaut dans la [CLI](../developpement/cli-usage.md).

## Schéma de la chaîne de traitements

Ci-dessous un schéma présente macroscopiquement la chaîne de traitements générant la base BQSS : on y retrouve les différents domaines de données et leur agrégation finale.

```{eval-rst}
.. mermaid::

  graph LR

  histo_finess --> out_finess
  histo_autorisations_as --> out_autorisations_as
  activites_et --> out_valeurs
  in_kv_iqss --> out_valeurs
  in_kv_sae --> out_valeurs
  in_kv_esatis --> out_valeurs
  in_kv_certification --> out_valeurs

  subgraph "BQSS"
    out_finess[Référentiel FINESS] --> out_doc_db
    out_autorisations_as[Autorisations AS historisées] --> out_doc_db
    out_valeurs[Fichier clé-valeur historisé] --> out_doc_db
    out_doc_db[Base Document]
  end

  subgraph CERTIFICATION
    histo_certification[Données de certification historisées] --> in_kv_certification[Certification clé-valeur]
  end

  subgraph ESATIS
    in_open_esatis[Extractions annuelles e-Satis] --> histo_esatis[Données e-Satis historisées]
    histo_esatis --> in_kv_esatis[e-Satis clé-valeur]
  end

  subgraph SAE
    in_open_sae[Bases statistiques annuelles SAE] --> histo_sae[Données SAE historisées]
    histo_sae --> in_kv_sae[SAE clé-valeur]
  end

  subgraph IQSS
    in_open_qualhas[Extractions annuelles Qualhas] --> histo_iqss[IQSS historisés]
    histo_iqss --> in_kv_iqss[IQSS clé-valeur]
  end

  subgraph FINESS
    in_open_finess[Extractions annuelles du fichier des établissements] --> histo_finess[FINESS historisés]
    in_open_autorisations_as[Extractions annuelles des autorisations AS] --> histo_autorisations_as[Autorisations AS historisées]
    in_open_autorisations_as[Extractions annuelles des autorisations AS] --> activites_et[Activités des ET]
  end
```
