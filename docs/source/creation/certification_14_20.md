# Domaine Certification - Référentiel 2014-2020

## Description générale

La démarche de certification a pour objectif de porter une appréciation indépendante sur la qualité des prestations d’un établissement de santé. La première version présente dans la base, appelée référentiel 2014-2020, s’effectue tous les 4 ans.
Des professionnels de santé, mandatés par la HAS, réalisent des visites de certification sur la base d’un manuel constitué de plusieurs thématiques.
A l’issue de la procédure de certification, la HAS prononce un niveau global de certification ainsi que des décisions par thématique.

Le schéma des données de certifications est décrit sur [cette page](../schemas/certification_14_20/certifications.md), et celui des données de thématiques décrit sur [cette page](../schemas/certification_14_20/thematiques.md).

## Acquisition des données

Les données de certification sont téléchargées à partir du jeu de données [Certification des établissements de santé 2020](https://www.data.gouv.fr/fr/datasets/certification-des-etablissements-de-sante-2020/) mis à disposition par la HAS sur data.gouv.fr (les sources sont exhaustivement répertoriées dans le fichier `resources/certification_14_20/data_sources.yml`). Les 3 fichiers constituant ce jeu de données sont déposés dans le dossier `data/certification_14_20/raw/` avant d'être traités :

- `certifications.csv` : les résultats du niveau global de certification
- `thematiques.csv` : les décisions de certification par thématique
- `libelle-thematiques.csv` : les libellés des différentes thématiques, ces dernières n'étant référencées que par un numéro d'identification dans le fichier thematiques.csv

Pour toutes demandes concernant spécifiquement ces données, s'adresser au producteur : Haute Autorité de Santé - Service de Certification des Établissements de Santé (SCES).
Un formulaire de prise de contact est disponible sur la page correspondante du portail data.gouv.fr.

## Traitements des données

Le traitement des données de certification se fait en 2 temps :

1. une première partie est réalisable en "isolation" au sein du domaine Certification
2. une seconde partie nécessite de croiser d'autres domaines de données et est réalisée pour cette raison lors de la deuxième phase de la chaîne de traitements dans le domaine BQSS.

Au cours de la première partie dans le domaine Certification, plusieurs opérations sont effectuées :

- les données sont dédupliquées : certaines démarches de certification ayant eu lieu sur plusieurs dates très proches, nous ne conservons qu'une seule ligne de données concernant ces démarches.
- Les libellés des thématiques de certification sont également dénormalisés à l'intérieur de la table contenant les décisions de certification par thématique.
- La ligne ayant comme identifiant de thématique le numéro 99 est supprimée puisque ce numéro n'est pas référencé dans le fichier des libellés de thématiques.
- Pour terminer cette première étape, le fichier clé-valeur `data/certification_14_20/final/certifications_key_value.csv` est créé, ainsi qu'un fichier CSV de métadonnées documentant les clés (voir [section correspondante](#présentation-du-fichier-csv-de-métadonnées) ci-dessous).

Lors de la seconde étape de traitements des données de certification, la redescente des décisions de certification par thématique a lieu dans le [domaine BQSS](bqss.md) (s'y référer pour plus d'information). Cette étape ne peut avoir lieu dans le domaine Certification car elle nécessite de croiser des informations issues d'autres domaines de données ce qui est proscrit lors de la première phase de la chaîne de traitements (comme expliqué dans la page de [description générale](../donnees/general-description.md)).

## Validation des données

La validation des données de certification est réalisée par la librairie [frictionless](https://framework.frictionlessdata.io/docs/guides/introduction/) et le standard [Table Schema](https://specs.frictionlessdata.io/table-schema/). Cette validation s'appuie sur 2 fichiers de métadonnées (un par table) au format JSON et se trouvant dans le dossier `schemas/certification_14_20` : `certifications.json` et `thematiques.json`.

Cette validation s'appliquant au domaine Certification, elle s'effectue avant la deuxième étape de traitements des données décrite dans la précédente section ci-dessus.

## Présentation du fichier CSV de métadonnées

Le fichier CSV de métadonnées du domaine Certification `data/certification_14_20/final/certification_metadata.csv` a été généré automatiquement grâce au fichier `schemas/certification/certifications.json` décrivant le schéma de données au standard [Table Schema](https://specs.frictionlessdata.io/table-schema/). Il permet de documenter la version clé-valeur des données.

Le fichier de métadonnées contient 4 champs :

- `name`: le nom de la variable,
- `title`: le titre de la variable, explicite la variable en une phrase
- `description`: correspond à une description plus détaillée du champ,
- `type`: type primitif de la variable (chaîne de caractères, entier ou décimal).
