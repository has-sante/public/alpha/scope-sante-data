# Sélection des Finess

Au cours du processus, les données Finess subissent diverses opérations de filtrage/selection.
Ces opérations sont les suivantes:

- Filtrage sur les catégories d'aggrégat
- Filtrage sur les autorisations d'activité et les données de qualité et de sécurité des soins
- Création du filtre `actif_qualiscope`

> 📝 **Note :**
> Les chiffres présentés ci-dessous ont étés extraits le `21/06/2022`.
> Ils évoluent avec le temps et ne seront pas maintenus à jour.
> Le but est d'illustrer les volumes de Finess filtrés par chacun des filtres.

## Base Finess complète

Le Finess historisé compte:

- Tous exports confondus: `244 226` Finess Géographiques uniques.
- Exports depuis 2019: `171 344` Finess Géographiques uniques.
- Dernier export (`2022-05-27`): `94 562` Finess Géographiques uniques.

## Filtrage sur les catégories d'aggrégat

Dans le [Domaine Finess](./finess.md#traitement-des-données), on filtre sur les catégories d’agrégat.

Après filtrage, le finess historisé compte:

- Tous exports confondus: `15 139` Finess Géographiques uniques.
- Exports depuis 2019: `11 637` Finess Géographiques uniques.
- Dernier export (`2022-05-27`): `11 085` Finess Géographiques uniques.

## Filtrage sur les autorisations d'activité et les données de qualité et de sécurité des soins

Dans le [Domaine scope-santé](./bqss.md#exclusion-de-certains-finess), on garde les Finess qui vérifient ces conditions:

- OU
  - Qui ont les bonnes autorisations, c'est à dire:
    - au moins une autorisations d'activité de soin parmi celles concernées
    - et dont l'année de fin est supérieure ou égale à l'année en cours
  - Qui ont des données dans la table valeurs, c'est à dire:
    - au moins une donnée de la SAE
    - ou au moins une donnée d'IQSS (tout type confondu)
    - ou au moins une donnée de certification (tout référentiel confondu)

Après filtrage, le finess historisé compte:

- Tous exports confondus: `11 634` Finess Géographiques uniques.
- Exports depuis 2019: `9 349` Finess Géographiques uniques.
- Dernier export (`2022-05-27`): `8 886` Finess Géographiques uniques.

## Création du filtre `actif_qualiscope`

Dans le [Domaine scope-santé](./bqss.md#colonne-actif_qualiscope), on crée une colonne `actif_qualiscope` sur les Finess.
Cette colonne indique si le Finess doit être affiché sur le site QualiScope ou pas.

Un Finess est affiché sur le site s'il respecte les conditions suivantes:

La règle utilisée actuellement pour ce filtre est la suivante:

- ET
  - Ont été actifs dans les deux dernières années, c'est à dire:
    - Tous les établissements de l'export Finess de l'année en cours sont `actifs`
    - Tous les établissements de l'export Finess qui ont fermé dans l'année N-1 sont `actifs`
  - OU
    - Ont au moins un IQSS récent
    - ET
      - Ont les bonnes autorisation d’activité
      - Ont au moins un résultat de certification

Le filtre ainsi créé sélectionne environ `4 100` Finess.
