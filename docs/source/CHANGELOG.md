# Historique

Toutes les évolutions notables du projet BQSS sont listées ici.

Le code source du projet est accessible sur le dépôt gitlab: [https://gitlab.com/has-sante/public/bqss](https://gitlab.com/has-sante/public/bqss)

NB: Le format est inspiré de [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/)
et ce projet adhère à la [Gestion Sémantique de Version](https://semver.org/lang/fr/spec/v2.0.0.html).

## [1.2.2] - 26/09/2022

### Corrections
- Ajout de l'activité "Diagnostic génétique" à la place d'AMP DPN [#170](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/166)

## [1.2.1] - 29/08/2022

### Changements

- Changement du libelle de l'activité AMP DPN [#169](https://gitlab.com/has-sante/public/bqss/-/issues/169)

## [1.2.0] - 24/08/2022

### Ajouts

- Ajout de la SAE 2020 [#163](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/163)
- Catégorie d'activité à part entière pour les Soins de longue durée [#166](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/166)

### Corrections

- Patch d'un fichier corrompu de la SAE [#164](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/164)
- Suppression de l'activité "Pédiatrie" [#165](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/165)

## [1.1.0] - 27/06/2022

### Ajouts

- Ajout des dates de début et fin effective des autorisations d'activités [#160](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/160)

### Changements

- Ajout des établissements des établissements ayant l'activité de dialyse dans le filtre `actif_qualiscope` [#105](https://gitlab.com/has-sante/public/bqss/-/issues/105#note_1007153424)

### Corrections

- Meilleure gestion des autorisations d'activité [#160](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/160)

## [1.0.0] - 22/06/2022

Le principal changement de cette release est le renommage complet du projet.
On passe de `scope-sante-data` à `BQSS` ce qui engendre des breaking changes.
Le repository du projet a changé de https://gitlab.com/has-sante/public/alpha/scope-sante-data à https://gitlab.com/has-sante/public/bqss.

### Ajouts

- Ajouts des indicateurs "binaires" aux statistiques nationales [#151](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/151)
- Ajouts des données de la certification référentiel 2021 [#125](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/125)
- Ajouts des codes démarche de certification [#152](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/152)

### Changements

- `[BREAKING]` renommage en BQSS [#136](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/136):
  - Renommage de la colonne `actif_scope_sante` dans la table `valeurs.csv` en `actif_qualiscope`
  - Urls de la doc change: https://has-sante.gitlab.io/public/alpha/scope-sante-data devient https://has-sante.gitlab.io/public/bqss
- Changement du filtre `actif_qualiscope [#105](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/105#note_998363597)
- Mise à jour des dépendances du projet
- Utilisation de `pydantic-autodoc` pour documenter la base document
- Amélioration du `type_etablissement` [#155](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/155)

## Corrections

- Correction d'un bug dans le traitement des Finess, qui n'étaient pas filtré sur les catégories d'aggrégats sur l'extract le plus récent.
- Correction des types et des types métiers de certains IQSS [#150](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/150):
  - `MHS_IAS_ICSHA_2018-resultat_icsha`, `PSY_IAS_ICSHA_2018-resultat_icsha` et `mco_eteortho_eteortho_2017-ete_ortho_etbt` changent de type métier: `ratio` à la place de `resultat`
  - Les types de certains IQSS ont été revus et passent de `classe` à `int-string`
- Correction de l'url de téléchargement FINESS géographiques [#153](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/153)

## [0.1.0]

Cette section liste les changements de la version `0.1.0`.

### Ajouts

- Ajout d'un changelog [#147](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/147)
- Ajout des IQSS 2021 [#145](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/145)
- Utilisation de [pandera](https://pandera.readthedocs.io) pour la validation de certains Dataframes. [#146](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/146)

## Corrections

- Correction d'un bug donnant des IQSS satisfaction patient au mauvais finess [#148](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/148)
- Correction d'un bug dans les metadata finess provoquant la perte des activités dans la base document [#149](https://gitlab.com/has-sante/public/alpha/scope-sante-data/-/issues/149)
