"""Sphinx configuration."""
import sys
from typing import List

import importlib_metadata
from dotenv import load_dotenv

# Load user-specific env vars (e.g. secrets) from a `.env` file
load_dotenv()


# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. Note that we are adding an absolute
# path.
# _project_directory = pathlib.Path(__file__).parent.parent.parent
# sys.path.insert(0, str(_project_directory))


# -- Project information -----------------------------------------------------
PACKAGE_NAME = "bqss"
try:
    project_metadata = importlib_metadata.metadata(PACKAGE_NAME)
except importlib_metadata.PackageNotFoundError as err:
    raise RuntimeError(
        f"The package '{PACKAGE_NAME}' must be installed. "
        "Please install the package in editable mode before building docs."
    ) from err


# pylint: disable=invalid-name

# -- Project information -----------------------------------------------------
project = project_metadata["Name"]
author = project_metadata["Author"]
# pylint: disable=redefined-builtin
version = release = project_metadata["Version"]

# -- General configuration ---------------------------------------------------
extensions = [
    "myst_nb",  # To render notebooks as docs (https://myst-nb.readthedocs.io/en/latest/index.html)
    # "myst_parser",  # MyST .md parsing (https://myst-parser.readthedocs.io/en/latest/index.html)
    "sphinx.ext.autodoc",  # Include documentation from docstrings (https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html)
    "sphinx.ext.autosummary",  # Generate autodoc summaries (https://www.sphinx-doc.org/en/master/usage/extensions/autosummary.html)
    "sphinx.ext.intersphinx",  # Link to other projects’ documentation (https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html)
    "sphinx.ext.viewcode",  # Add documentation links to/from source code (https://www.sphinx-doc.org/en/master/usage/extensions/viewcode.html)
    "sphinx.ext.napoleon",  # Support for NumPy and Google style docstrings (https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
    "sphinx_autodoc_typehints",  # Python 3 type annotation extraction (as opposed to manually specifying them in your docstrings) (https://pypi.org/project/sphinx-autodoc-typehints/)
    "sphinx_click",  # Automatic documentation of click based CLI (https://github.com/click-contrib/sphinx-click)
    "sphinxcontrib.mermaid",  # Render mermaid graphs (https://github.com/mgaitan/sphinxcontrib-mermaid)
    "sphinxcontrib.autodoc_pydantic",  # Render pydantic models (https://github.com/mansenfranzen/autodoc_pydantic)
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
# Note: `custom-class-template.rst` & `custom-module-template.rst`
#   for sphinx.ext.autosummary extension `recursive` option
#   see: https://github.com/JamesALeedham/Sphinx-Autosummary-Recursion


# List of patterns, relative to source directory, that match files and
#   directories to ignore when looking for source files.
#   This pattern also affects html_static_path and html_extra_path.
exclude_patterns: List[str] = ["_build", "Thumbs.db", ".DS_Store", ".venv"]

# Sphinx configs
html_theme = "pydata_sphinx_theme"
html_static_path = ["_static"]
html_favicon = "_static/favicon.ico"
html_theme_options = {
    "footer_items": ["sphinx-version"],
    "logo": {
        "image_light": "logo-light.svg",
        "image_dark": "logo-dark.svg",
    },
}
html_context = {
    "default_mode": "light",
}
html_show_sourcelink = (
    False  # Remove 'view source code' from top of page (for html, not python)
)

html_css_files = [
    "css/custom.css",
]

# -- Extension configurations ---------------------------------------------------

# sphinx.ext.autosummary configs
autosummary_generate = True  # Turn on sphinx.ext.autosummary

# sphinx.ext.autodoc configs
autoclass_content = "class"  # Add __init__ doc (ie. params) to class summaries
autodoc_inherit_docstrings = (
    True  # If no class summary, inherit base class summary
)
autodoc_typehints = (
    "description"  # Show typehints as content of function or method
)


# Don't show FQDN as much as possible
add_module_names = False
autodoc_typehints_format = "short"
python_use_unqualified_type_names = True

autodoc_pydantic_model_show_field_summary = False

# myst_parser configs

# tableschemas start at h2 🤷
suppress_warnings = ["myst.header"]
# For section anchors
myst_heading_anchors = 4


# myst_nb config
nb_custom_formats = {
    ".myst": ["jupytext.reads", {"fmt": ".myst"}],
}
nb_execution_timeout = 60
nb_execution_mode = "cache"

# -- External mapping --------------------------------------------------------
python_version = ".".join(map(str, sys.version_info[0:2]))
intersphinx_mapping = {
    "sphinx": ("http://www.sphinx-doc.org/en/stable", None),
    "python": ("https://docs.python.org/" + python_version, None),
}
