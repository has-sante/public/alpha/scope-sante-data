# Description générale des données

Les données produites par ce projets sont centrées autour des établissements de santé.
Elles exposent pour chaque Finess géographique les donnés de qualité et de sécurité des soins qui lui sont associées pour une année donnée.

## Formats de mise à disposition des données

Les données sont mises à disposition sous deux forme:

- Base orientée document
- Base relationnelle

## Version document de la base

Une version document de la base est également mise à disposition au format JSONL, où chaque ligne du fichier correspond à un objet JSON rassemblant les données associées à un établissement (il y a donc autant de lignes que d’établissements).
Ces données d'établissements reprennent l'ensemble des domaines précisés ci-dessus, ainsi que des données d'activité collectées grâce au domaine FINESS (voir [ce schéma](../schemas/finess/activites_et_key_value.md) ou encore [celui-ci](../schemas/finess/activites_et.md)).
Cette version de la base est accompagnée d'un fichier JSON regroupant des statistiques nationales sur les données.

## Base relationnelle (ou clé-valeur)

Les données de la base sont distribuées au format csv, chaque table étant contenue dans un fichier distinct.
Ces fichiers contiennent un "header" sur la première ligne énumérant le nom de chaque colonne constituant les tables.
Le caractère de séparation utilisé est la virgule (",").

Par ailleurs, afin de faciliter les usages, certaines données (la table des métadonnées et la table des nomenclatures) sont également distribuées au format Excel.

### Schéma de la base relationnelle

La base de données finale s'articule autour de 5 tables :

- finess : la table finess servant de référentiel
- autorisations_as : la table des autorisations d'activités de soins
- valeurs : la table contenant toutes les variables au format clé-valeur
- metadata : la table documentant chacune des clés utilisées dans la table valeurs
- nomenclatures : la table documentant la nomenclature des clés de données

Le modèle relationnel de ces 5 tables est décrit sur le diagramme ci-dessous :

```{eval-rst}
.. mermaid::

  erDiagram
    autorisations_as {
        date dateexport
        string num_finess_et
        string num_finess_ej
        int activite
        int modalite
        int forme
        date datemeo
        date datefin
    }
    finess {
        date dateexport
        string num_finess_et
        string num_finess_ej
        string rs
        string commune
        string departement
        boolean ferme_cette_annee
        boolean actif_qualiscope
        boolean dernier_enregistrement
    }
    valeurs {
        int annee
        string finess
        string finess_type
        string key
        boolean value_boolean
        string value_string
        int value_integer
        float value_float
        string missing_value
        date value_date
    }
    metadata {
        string name
        string title
        string description
        string type
        string source
    }
    nomenclatures {
        string key
        string value
        string label
        string source
    }

    autorisations_as ||--o{ finess : "num_finess_et:num_finess_et"
    valeurs ||--o{ finess : "num_finess_{et/ej}:finess"
    metadata ||--o{ valeurs : "key:name"
    metadata ||--o{ valeurs : "key:name"
    nomenclatures ||--o{ metadata : "name:key"
```

Ainsi, dans la table valeurs constituant un tableau associatif, la valeur de chaque variable est associée à une clé documentée dans la table de metadata.
Ce modèle permet une très grande souplesse quant aux évolutions futures.

Chaque valeur est également liée à un numéro FINESS (référencé dans la table finess), qu'il soit géographique ou juridique – ce type étant renseigné dans la colonne `finess_type` de la table valeurs.

Les données d'autorisations et de FINESS sont issues du domaine FINESS décrit dans la section "Description Générale" de la page [Domaine FINESS](../creation/finess.md).
Leurs mutations sont décrites dans la page [Mouvements de Recompostion](../creation/mutation-finess.md).

Les données de la table `valeurs` proviennent de quatre domaines :

- le domaine SAE (voir section [Description Générale](../creation/sae.md) correspondante)
- le domaine Certifications (voir section [Description Générale](../creation/certification.md) correspondante)
- le domaine e-Satis (voir section [Description Générale](../creation/esatis.md) correspondante)
- le domaine IQSS (voir section [Description Générale](../creation/iqss.md) correspondante)

Pour pouvoir être importées dans une base relationnelle, les types primitifs des valeurs doivent pouvoir être explicitement identifiés (booléen, chaîne de caractères, entier et décimal – en anglais boolean, string, integer et float). Pour cela, il existe 4 colonnes qui correspondent à chacun des types de valeurs supportés par la plupart des SGBDR, et dans lesquels se répartissent les valeurs selon qu'il s'agisse de booléen (boolean), de chaîne de caractères (string), d'entier (integer) ou de décimal (float).

La table nomenclatures contient les correspondances valeur-libellés pour chaque couple clé-valeurs concernant une variable catégorielle.
