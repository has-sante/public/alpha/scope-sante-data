# Schémas de données de la base document

## Schémas de données d'un établissement dans la base document

```{eval-rst}
.. autopydantic_model:: bqss.bqss.models.documents.FinessDocumentModel
```

### Schéma détaillé des données des établissements

```{eval-rst}
.. automodule:: bqss.bqss.models.documents
   :members:
   :exclude-members: FinessDocumentModel
   :member-order: bysource
```

## Schémas de données des statistiques nationales

```{eval-rst}
.. autopydantic_model:: bqss.bqss.models.documents_stats.IndicateurStatsModel
```

### Schéma détaillé des statistiques nationales

```{eval-rst}
.. automodule:: bqss.bqss.models.documents_stats
   :members:
   :exclude-members: IndicateurStatsModel
   :member-order: bysource
```
