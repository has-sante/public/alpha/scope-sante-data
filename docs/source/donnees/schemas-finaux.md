# Schémas

Les schémas des tables de la base sont accessibles dans cette section.

```{eval-rst}
.. toctree::
    :maxdepth: 1

    finess <../schemas/finess/finess>
    autorisations_as <../schemas/finess/autorisations_as>
    metadata <../schemas/bqss/metadata>
    valeurs <../schemas/bqss/valeurs>
    nomenclatures <../schemas/bqss/nomenclatures>
    base document <document-db-schemas>
```
