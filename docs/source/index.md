```{include} ../../README.md
:relative-docs: docs/source/
:relative-images:
```

```{toctree}
   :hidden:
   :maxdepth: 1
   :glob:

   Données <donnees/index-donnees>
   Création <creation/index-creation>
   Développement <developpement/index-developpement>
   Historique <CHANGELOG>
```
